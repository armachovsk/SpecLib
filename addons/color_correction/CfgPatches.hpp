class CfgPatches {
    class ADDON {
        name = COMPONENT_NAME;
        units[] = {};
        weapons[] = {};
        requiredVersion = REQUIRED_VERSION;
        requiredAddons[] = {"cba_main", "A3_Modules_F"};
        author = "Reidond";
        authors[] = {"GEORGE FLOROS [GR]"};
        url = "https://gitlab.com/Reidond";
        VERSION_CONFIG;
    };
};
