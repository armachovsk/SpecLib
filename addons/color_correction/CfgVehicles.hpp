class CfgVehicles {
    class Logic;
    class Module_F : Logic {
        class AttributesBase {
            class Default;
            class Edit;           // Default edit box (i.e., text input field)
            class Combo;          // Default combo box (i.e., drop-down menu)
            class Checkbox;       // Default checkbox (returned value is Bool)
            class CheckboxNumber; // Default checkbox (returned value is Number)
            class ModuleDescription; // Module description
            class Units; // Selection of units on which the module is applied
        };
        // Description base classes, for more information see below
        class ModuleDescription {
            class AnyBrain;
        };
    };

    class SpecLib_Module_ColorCorrection : Module_F {
        // Standard object definitions
        scope =
            2; // Editor visibility; 2 will show it in the menu, 1 will hide it.
        displayName = CSTRING(moduleName); // Name displayed in the menu
        // icon =
        // "SpecLib_Module_ColorCorrection\Module\Images\placeholder.paa"; // Map
        // icon. Delete this entry to use the default icon
        category = "SpecLib_Modules";

        // Name of function triggered once conditions are met
        function = QFUNC(moduleColorCorrection);
        // Execution priority, modules with lower number are executed first. 0
        // is used when the attribute is undefined
        functionPriority = 1;
        // 0 for server only execution, 1 for global execution, 2 for persistent
        // global execution
        isGlobal = 1;
        // 1 for module waiting until all synced triggers are activated
        isTriggerActivated = 0;
        // 1 if modules is to be disabled once it's activated (i.e., repeated
        // trigger activation won't work)
        isDisposable = 0;
        // // 1 to run init function in Eden Editor as well
        is3DEN = 0;

        // Menu displayed when the module is placed or double-clicked on by Zeus
        curatorInfoType = "RscDisplayAttributeColorCorrection";

        // Module attributes, uses
        // https://community.bistudio.com/wiki/Eden_Editor:_Configuring_Attributes#Entity_Specific
        class Attributes : AttributesBase {
            // Module specific arguments
            class ModuleColorCorrection_ColorComboBox : Combo {
                // Unique property, use "<moduleClass>_<attributeClass>" format
                // to make sure the name is unique in the world
                property = "SpecLib_ModuleColorCorrection_ColorComboBox";
                displayName = CSTRING(moduleAttrName); // Argument label
                tooltip = "";                          // Tooltip description
                typeName =
                    "NUMBER"; // Value type, can be "NUMBER", "STRING" or "BOOL"
                defaultValue =
                    "1"; // Default attribute value. WARNING: This is an
                         // expression, and its returned value will be used (50
                         // in this case) Because it's an expression, to return
                         // a String one must have a string within a string
                class Values {
                    class disabled {
                        name = "Disabled";
                        value = 0;
                    }; // Listbox item
                    class realistic {
                        name = "Realistic Color Correction theme";
                        value = 1;
                    };
                    class post_apocalyptic {
                        name = "Post Apocalyptic theme";
                        value = 2;
                    };
                    class nightstalkers {
                        name = "Nightstalkers theme";
                        value = 3;
                    };
                    class ofp_gamma {
                        name = "OFP Gamma theme";
                        value = 4;
                    };
                    class golden_autumn {
                        name = "Golden autumn theme";
                        value = 5;
                    };
                    class africa {
                        name = "Africa theme";
                        value = 6;
                    };
                    class afghan {
                        name = "Afghan theme";
                        value = 7;
                    };
                    class middle_east {
                        name = "Middle East theme";
                        value = 8;
                    };
                    class real_is_brown {
                        name = "Real Is Brown theme";
                        value = 9;
                    };
                    class gray_tone {
                        name = "Gray Tone theme";
                        value = 10;
                    };
                    class cold_tone {
                        name = "Cold Tone theme";
                        value = 11;
                    };
                    class winter_blue {
                        name = "Winter Blue theme";
                        value = 12;
                    };
                    class winter_white {
                        name = "Winter White theme";
                        value = 13;
                    };
                    class mediterranean {
                        name = "Mediterranean theme";
                        value = 14;
                    };
                };
            };
            // class ModuleDescription: ModuleDescription{}; // Module
            // description should be shown last
        };
    };
    class SpecLib_ModuleColorCorrection : SpecLib_Module_ColorCorrection {
        scope = 1;
    };
};
