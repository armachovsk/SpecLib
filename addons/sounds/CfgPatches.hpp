class CfgPatches {
    class ADDON {
        name = COMPONENT_NAME;
        units[] = {};
        weapons[] = {};
        requiredVersion = REQUIRED_VERSION;
        requiredAddons[] = {"cba_main", "rhs_sounds", "rhs_c_weapons"};
        author = "Reidond";
        authors[] = {""};
        url = "https://gitlab.com/Reidond";
        VERSION_CONFIG;
    };
};
