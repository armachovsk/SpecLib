class RHS_UAZ_Base : Offroad_01_base_F {
    class Turrets : Turrets {};
};
class RHS_UAZ_DShKM_Base : RHS_UAZ_Base {
    class CargoTurret;
    class Turrets : Turrets {
        class MainTurret : MainTurret {};
        class CargoTurret_01 : CargoTurret {};
    };
};
class rhsgref_BRDM2 : Wheeled_APC_F {};
class rhsgref_BRDM2_ATGM : rhsgref_BRDM2 {};
class rhsgref_BRDM2UM : rhsgref_BRDM2 {};
class rhsgref_BRDM2_HQ : rhsgref_BRDM2UM {};
class rhsgref_nat_2b14;
class rhs_btr60_msv;
class rhsgref_nat_uaz;
class rhsgref_nat_uaz_open;
class RHS_UAZ_AGS30_Base : RHS_UAZ_DShKM_Base {};
class RHS_UAZ_SPG9_Base : RHS_UAZ_DShKM_Base {};
class rhs_uh1h_base : Helicopter_Base_H {
    class Components;
};
class rhs_uh1h_hidf : rhs_uh1h_base {};
class rhs_bmp1tank_base : Tank_F {};
class rhs_bmp_base : rhs_bmp1tank_base {};
class rhs_bmp1_vdv : rhs_bmp_base {};
class rhs_bmp1_msv : rhs_bmp1_vdv {};
class RHS_Ural_BaseTurret : Truck_F {
    class Turrets;
    class AnimationSources;
};
class RHS_Ural_Zu23_Base : RHS_Ural_BaseTurret {
    class Turrets : Turrets {
        class MainTurret;
        class CargoTurret_01;
        class CargoTurret_02;
        class CargoTurret_03;
    };
};
class rhsgref_nat_ural_Zu23 : RHS_Ural_Zu23_Base {};
class rhs_truck : Truck_F {};
class rhs_gaz66_vmf : rhs_truck {};
class rhs_gaz66_zu23_base : rhs_gaz66_vmf {
    class Turrets : Turrets {
        class MainTurret;
        class CargoTurret_01;
        class CargoTurret_02;
        class CargoTurret_03;
    };
};
class rhs_gaz66_zu23_msv : rhs_gaz66_zu23_base {};
class RHS_Ural_Base : RHS_Ural_BaseTurret {};
class RHS_Ural_MSV_Base : RHS_Ural_Base {};
class RHS_Ural_Support_MSV_Base_01 : RHS_Ural_MSV_Base {};
class RHS_Ural_Fuel_MSV_01 : RHS_Ural_Support_MSV_Base_01 {};
class rhs_gaz66_repair_base : rhs_gaz66_vmf {};
class rhs_gaz66_repair_msv : rhs_gaz66_repair_base {};
class rhs_gaz66_ammo_base : rhs_gaz66_vmf {};
class rhs_gaz66_ammo_msv : rhs_gaz66_ammo_base {};
class RHS_Ural_Civ_Base : RHS_Ural_Base {};
class rhsgref_nat_ural : RHS_Ural_Civ_Base {};
class RHS_ZU23_base;
class rhsgref_nat_ZU23 : RHS_ZU23_base {};
class rhs_SPG9_base;
class rhsgref_nat_SPG9 : rhs_SPG9_base {};
class rhs_DSHKM_base;
class rhsgref_nat_DSHKM : rhs_DSHKM_base {};
class rhs_DSHkM_Mini_TriPod_base;
class rhsgref_nat_DSHKM_Mini_TriPod : rhs_DSHkM_Mini_TriPod_base {};
class RHS_AGS30_TriPod_base;
class rhsgref_nat_AGS30_TriPod : RHS_AGS30_TriPod_base {};
class RHS_AN2_Base : Plane_Base_F {};
class RHS_AN2 : RHS_AN2_Base {};
class rhsgref_nat_ural_work : RHS_Ural_Civ_Base {};
class rhsgref_nat_ural_work_open : RHS_Ural_Civ_Base {};
class rhs_medic_bag;
class rhs_rpg_empty;
