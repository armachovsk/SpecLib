#define COMPONENT units_takistani_civilians
#define COMPONENT_BEAUTIFIED Units - Takistani Civilians
#include "\z\speclib\addons\main\script_mod.hpp"

// #define DEBUG_MODE_FULL
// #define DISABLE_COMPILE_CACHE

#ifdef DEBUG_ENABLED_TEST
#define DEBUG_MODE_FULL
#endif
#ifdef DEBUG_SETTINGS_TEST
#define DEBUG_SETTINGS DEBUG_SETTINGS_TEST
#endif

#include "\z\speclib\addons\main\script_macros.hpp"

#define PREVIEW(CLASSNAME)                                                     \
    editorPreview = \z\speclib\addons\COMPONENT\preview\##CLASSNAME##.jpg
