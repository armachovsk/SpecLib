class CfgVehicles {
#include "include\a3.hpp"
#include "include\cup.hpp"
#include "include\rds.hpp"
#include "include\rhs.hpp"

    class SpecLib_tt650_Taki_Civ_01 : RDS_tt650_Civ_01 {
        PREVIEW(SpecLib_tt650_Taki_Civ_01);
        scope = 2;
        author = "RDS (BIS A2 Port)";
        dlc = "RDS_CIV_PACK";
        crew = "SpecLib_c_tk_civ_black";
        faction = "SpecLib_Faction_Taki_Civilians";
        typicalCargo[] = {};
        editorSubcategory = "SpecLib_EdSubcat_Bikes";
    };

    class SpecLib_JAWA353_Taki_Civ_01 : RDS_JAWA353_Civ_01 {
        PREVIEW(SpecLib_JAWA353_Taki_Civ_01);
        scope = 2;
        author = "RDS (BIS A2 Port)";
        dlc = "RDS_CIV_PACK";
        crew = "SpecLib_c_tk_civ_blue";
        faction = "SpecLib_Faction_Taki_Civilians";
        typicalCargo[] = {};
        editorSubcategory = "SpecLib_EdSubcat_Bikes";
    };

    class SpecLib_Old_bike_Taki_Civ_01 : RDS_Old_bike_Civ_01 {
        PREVIEW(SpecLib_Old_bike_Taki_Civ_01);
        scope = 2;
        author = "RDS (BIS A2 Port)";
        dlc = "RDS_CIV_PACK";
        crew = "SpecLib_c_tk_civ_brown_brown_jacket";
        faction = "SpecLib_Faction_Taki_Civilians";
        typicalCargo[] = {};
        editorSubcategory = "SpecLib_EdSubcat_Bikes";
    };

    class SpecLib_Gaz24_Taki_Civ_01 : RDS_Gaz24_Civ_01 {
        PREVIEW(SpecLib_Gaz24_Taki_Civ_01);
        scope = 2;
        author = "RDS (BIS A2 Port)";
        dlc = "RDS_CIV_PACK";
        displayName = "GAZ-24 (Blue)";
        crew = "SpecLib_c_tk_civ_white_grey_Waistcoat";
        faction = "SpecLib_Faction_Taki_Civilians";
        typicalCargo[] = {};
        editorSubcategory = "SpecLib_EdSubcat_Cars";
    };

    class SpecLib_Gaz24_Taki_Civ_02 : RDS_Gaz24_Civ_02 {
        PREVIEW(SpecLib_Gaz24_Taki_Civ_02);
        author = "RDS (BIS A2 Port)";
        dlc = "RDS_CIV_PACK";
        displayName = "GAZ-24 (Grey)";
        hiddenselectionstextures[] = {
            "rds_a2port_civ\volha\Data\Volha_Gray_ECIV_CO.paa"};
        crew = "SpecLib_c_tk_civ_blue_brown_coat";
        faction = "SpecLib_Faction_Taki_Civilians";
        typicalCargo[] = {};
        editorSubcategory = "SpecLib_EdSubcat_Cars";
    };

    class SpecLib_Gaz24_Taki_Civ_03 : RDS_Gaz24_Civ_03 {
        PREVIEW(SpecLib_Gaz24_Taki_Civ_03);
        author = "RDS (BIS A2 Port)";
        dlc = "RDS_CIV_PACK";
        displayName = "GAZ-24 (Black)";
        hiddenselectionstextures[] = {
            "rds_a2port_civ\volha\Data\Volha_Black_ECIV_CO.paa"};
        crew = "SpecLib_c_tk_civ_black_brown_jacket";
        faction = "SpecLib_Faction_Taki_Civilians";
        typicalCargo[] = {};
        editorSubcategory = "SpecLib_EdSubcat_Cars";
    };

    class SpecLib_Lada_Taki_Civ_03 : RDS_Lada_Civ_03 {
        PREVIEW(SpecLib_Lada_Taki_Civ_03);
        author = "RDS (BIS A2 Port)";
        dlc = "RDS_CIV_PACK";
        displayName = "VAZ-2103 (Green)";
        hiddenselectionstextures[] = {
            "rds_a2port_civ\Lada\Data\lada_eciv1_co.paa",
            "rds_a2port_civ\Lada\Data\Lada_glass_ECIV1_CA.paa"};
        crew = "SpecLib_c_tk_civ_black_brown_jacket";
        faction = "SpecLib_Faction_Taki_Civilians";
        typicalCargo[] = {};
        editorSubcategory = "SpecLib_EdSubcat_Cars";
    };

    class SpecLib_S1203_Taki_Civ_01 : RDS_S1203_Civ_01 {
        PREVIEW(SpecLib_S1203_Taki_Civ_01);
        scope = 2;
        author = "RDS (BIS A2 Port)";
        dlc = "RDS_CIV_PACK";
        crew = "SpecLib_c_tk_civ_tan_light_brown_jacket";
        faction = "SpecLib_Faction_Taki_Civilians";
        typicalCargo[] = {};
        editorSubcategory = "SpecLib_EdSubcat_Cars";
    };

    class SpecLib_Taki_Civ_UAZ : rhsgref_nat_uaz {
        PREVIEW(SpecLib_Taki_Civ_UAZ);
        scope = 2;
        author = "RHS (A2 port)";
        side = 3;
        crew = "SpecLib_c_tk_civ_white_grey_jacket";
        faction = "SpecLib_Faction_Taki_Civilians";
        typicalCargo[] = {};
        editorSubcategory = "SpecLib_EdSubcat_Cars";
        accuracy = 0.5;
        rhs_decalParameters[] = {};
        class TransportItems {
            class _xx_rds_car_FirstAidKit {
                name = "rds_car_FirstAidKit";
                count = 1;
            };
        };
        hiddenSelections[] = {"camo1", "camo2g", "n1", "n2", "n3",
                              "n4",    "i1",     "i2", "i3", "i4"};
        hiddenSelectionsTextures[] = {
            "rhsafrf\addons\rhs_a2port_car\uaz\data\uaz_main_civil_co.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa"};
        class textureSources {
            class Camo3 {
                displayName = "Civil";
                author = "$STR_RHS_AUTHOR_FULL";
                textures[] = {
                    "rhsafrf\addons\rhs_a2port_car\uaz\data\uaz_main_civil_co.paa"};
                factions[] = {"SpecLib_Faction_Taki_Civilians"};
            };
        };
        textureList[] = {};
        class Attributes {
            class ObjectTexture {
                control = "ObjectTexture";
                data = "ObjectTexture";
                displayName = "Skin";
                tooltip = "Texture and material set applied on the object.";
            };
            class rhs_decalNumber_type {
                displayName = "Define font type of plate number";
                tooltip = "Define kind of font that will be drawn on vehicle.";
                property = "rhs_decalNumber_type";
                control = "Combo";
                expression =
                    "_this setVariable ['%s', _value];[_this,[['Number', cDecals4CarsNumberPlaces, _value]]] call rhs_fnc_decalsInit";
                defaultValue = 0;
                typeName = "STRING";
                class values {
                    class Default {
                        name = "Default";
                        value = "Default";
                        defaultValue = "Default";
                    };
                    class DefaultRed {
                        name = "Default (Red)";
                        value = "DefaultRed";
                    };
                    class BoldRed {
                        name = "Bold Red";
                        value = "BoldRed";
                    };
                    class CDF {
                        name = "CDF";
                        value = "CDF";
                    };
                    class Handpaint {
                        name = "Handpaint";
                        value = "Handpaint";
                    };
                    class HandpaintBlack {
                        name = "Handpaint Black";
                        value = "HandpaintBlack";
                    };
                    class Iraqi {
                        name = "Iraqi";
                        value = "Iraqi";
                    };
                    class LicensePlate {
                        name = "License Plate";
                        value = "LicensePlate";
                    };
                };
            };
            class rhs_decalNumber {
                collapsed = 1;
                displayName = "Set plate number";
                tooltip =
                    "Set plate number. 4 numbers are required. If 0, random number will be generated";
                property = "rhs_decalNumber";
                control = "Edit";
                validate = "Number";
                typeName = "Number";
                defaultValue = "-1";
                expression =
                    "if(_value >= 0)then{[_this,[['Number', cDecals4CarsNumberPlaces, _this getVariable ['rhs_decalNumber_type','LicensePlate'], _value]]] call rhs_fnc_decalsInit};";
            };
            class rhs_decalArmy_type {
                displayName = "Define large door roundel type";
                tooltip = "Decal type";
                property = "rhs_decalArmy_type";
                control = "Combo";
                expression = "_this setVariable ['%s', _value];";
                defaultValue = "0";
                typeName = "STRING";
                class values {
                    class Army {
                        name = "Army";
                        value = "Army";
                        defaultValue = "Army";
                    };
                    class Honor {
                        name = "Honor";
                        value = "Honor";
                    };
                    class HonorGDR {
                        name = "Honor GDR";
                        value = "HonorGDR";
                    };
                    class Platoon {
                        name = "Platoon";
                        value = "Platoon";
                    };
                    class PlatoonGDR {
                        name = "Platoon GDR";
                        value = "PlatoonGDR";
                    };
                    class PlatoonVDV {
                        name = "Platoon VDV";
                        value = "PlatoonVDV";
                    };
                };
            };
            class rhs_decalArmy {
                displayName = "Set large door roundel symbol";
                tooltip =
                    "Set large door roundel located on both sides. Usually used for army symbols. -1 leaves current symbol & 0 clears decal.";
                property = "rhs_decalArmy";
                control = "Edit";
                validate = "none";
                defaultValue = "-1";
                expression =
                    "if(parseNumber _value >= 0)then{ [_this, [ [ 'Label', cDecalsCarsRightArmyPlaces,  _this getVariable ['rhs_decalArmy_type','Army'],call compile _value] ] ] call rhs_fnc_decalsInit};";
            };
            class rhs_decalPlatoon_type : rhs_decalArmy_type {
                displayName = "Define small door roundel type";
                property = "rhs_decalPlatoon_type";
                expression = "_this setVariable ['%s', _value];";
                defaultValue = "0";
            };
            class rhs_decalPlatoon : rhs_decalArmy {
                displayName = "Set small door roundel symbol";
                tooltip =
                    "Define small door roundel located on both sides. Usually used for platoon symbols. -1 leaves current symbol & 0 clears decal.";
                property = "rhs_decalPlatoon";
                expression =
                    "if(parseNumber _value >= 0)then{ [_this, [ [ 'Label', cDecalsCarsRightPlatoonPlaces,  _this getVariable ['rhs_decalPlatoon_type','Army'],call compile _value] ] ] call rhs_fnc_decalsInit};";
            };
            class rhs_hideLightCover {
                displayName = "Hide light covers";
                property = "rhs_hideLightCover";
                control = "CheckboxNumber";
                expression = "_this animate ['light_hide',_value,true]";
                defaultValue = "0";
            };
            class rhs_hidespare : rhs_hideLightCover {
                displayName = "Remove spare wheel";
                property = "rhs_hidespare";
                expression = "_this animate ['spare_hide',_value,true]";
            };
            class Door_LF : rhs_hidespare {
                displayName = "Open front left door";
                property = "Door_LF";
                expression = "_this animateDoor ['%s',_value,true]";
            };
            class Door_RF : Door_LF {
                displayName = "Open front right door";
                property = "Door_RF";
            };
            class Door_LB : Door_LF {
                displayName = "Open left back door";
                property = "Door_LB";
            };
            class Door_RB : Door_LF {
                displayName = "Open right back door";
                property = "Door_RB";
            };
        };
    };
    class SpecLib_Taki_Civ_UAZ_Open : rhsgref_nat_uaz_open {
        PREVIEW(SpecLib_Taki_Civ_UAZ_Open);
        scope = 2;
        author = "RHS (A2 port)";
        side = 3;
        crew = "SpecLib_c_tk_civ_black_brown_waistcoat";
        faction = "SpecLib_Faction_Taki_Civilians";
        typicalCargo[] = {};
        editorSubcategory = "SpecLib_EdSubcat_Cars";
        accuracy = 0.5;
        rhs_decalParameters[] = {};
        class TransportItems {
            class _xx_rds_car_FirstAidKit {
                name = "rds_car_FirstAidKit";
                count = 1;
            };
        };
        hiddenSelections[] = {"camo1", "camo2g", "n1", "n2", "n3",
                              "n4",    "i1",     "i2", "i3", "i4"};
        hiddenSelectionsTextures[] = {
            "rhsafrf\addons\rhs_a2port_car\uaz\data\uaz_main_civil_co.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa"};
        class textureSources {
            class Camo3 {
                displayName = "Civil";
                author = "$STR_RHS_AUTHOR_FULL";
                textures[] = {
                    "rhsafrf\addons\rhs_a2port_car\uaz\data\uaz_main_civil_co.paa"};
                factions[] = {"SpecLib_Faction_Taki_Civilians"};
            };
        };
        textureList[] = {};
        class Attributes {
            class ObjectTexture {
                control = "ObjectTexture";
                data = "ObjectTexture";
                displayName = "Skin";
                tooltip = "Texture and material set applied on the object.";
            };
            class rhs_decalNumber_type {
                displayName = "Define font type of plate number";
                tooltip = "Define kind of font that will be drawn on vehicle.";
                property = "rhs_decalNumber_type";
                control = "Combo";
                expression =
                    "_this setVariable ['%s', _value];[_this,[['Number', cDecals4CarsNumberPlaces, _value]]] call rhs_fnc_decalsInit";
                defaultValue = 0;
                typeName = "STRING";
                class values {
                    class Default {
                        name = "Default";
                        value = "Default";
                        defaultValue = "Default";
                    };
                    class DefaultRed {
                        name = "Default (Red)";
                        value = "DefaultRed";
                    };
                    class BoldRed {
                        name = "Bold Red";
                        value = "BoldRed";
                    };
                    class CDF {
                        name = "CDF";
                        value = "CDF";
                    };
                    class Handpaint {
                        name = "Handpaint";
                        value = "Handpaint";
                    };
                    class HandpaintBlack {
                        name = "Handpaint Black";
                        value = "HandpaintBlack";
                    };
                    class Iraqi {
                        name = "Iraqi";
                        value = "Iraqi";
                    };
                    class LicensePlate {
                        name = "License Plate";
                        value = "LicensePlate";
                    };
                };
            };
            class rhs_decalNumber {
                collapsed = 1;
                displayName = "Set plate number";
                tooltip =
                    "Set plate number. 4 numbers are required. If 0, random number will be generated";
                property = "rhs_decalNumber";
                control = "Edit";
                validate = "Number";
                typeName = "Number";
                defaultValue = "-1";
                expression =
                    "if(_value >= 0)then{[_this,[['Number', cDecals4CarsNumberPlaces, _this getVariable ['rhs_decalNumber_type','LicensePlate'], _value]]] call rhs_fnc_decalsInit};";
            };
            class rhs_decalArmy_type {
                displayName = "Define large door roundel type";
                tooltip = "Decal type";
                property = "rhs_decalArmy_type";
                control = "Combo";
                expression = "_this setVariable ['%s', _value];";
                defaultValue = "0";
                typeName = "STRING";
                class values {
                    class Army {
                        name = "Army";
                        value = "Army";
                        defaultValue = "Army";
                    };
                    class Honor {
                        name = "Honor";
                        value = "Honor";
                    };
                    class HonorGDR {
                        name = "Honor GDR";
                        value = "HonorGDR";
                    };
                    class Platoon {
                        name = "Platoon";
                        value = "Platoon";
                    };
                    class PlatoonGDR {
                        name = "Platoon GDR";
                        value = "PlatoonGDR";
                    };
                    class PlatoonVDV {
                        name = "Platoon VDV";
                        value = "PlatoonVDV";
                    };
                };
            };
            class rhs_decalArmy {
                displayName = "Set large door roundel symbol";
                tooltip =
                    "Set large door roundel located on both sides. Usually used for army symbols. -1 leaves current symbol & 0 clears decal.";
                property = "rhs_decalArmy";
                control = "Edit";
                validate = "none";
                defaultValue = "-1";
                expression =
                    "if(parseNumber _value >= 0)then{ [_this, [ [ 'Label', cDecalsCarsRightArmyPlaces,  _this getVariable ['rhs_decalArmy_type','Army'],call compile _value] ] ] call rhs_fnc_decalsInit};";
            };
            class rhs_decalPlatoon_type : rhs_decalArmy_type {
                displayName = "Define small door roundel type";
                property = "rhs_decalPlatoon_type";
                expression = "_this setVariable ['%s', _value];";
                defaultValue = "0";
            };
            class rhs_decalPlatoon : rhs_decalArmy {
                displayName = "Set small door roundel symbol";
                tooltip =
                    "Define small door roundel located on both sides. Usually used for platoon symbols. -1 leaves current symbol & 0 clears decal.";
                property = "rhs_decalPlatoon";
                expression =
                    "if(parseNumber _value >= 0)then{ [_this, [ [ 'Label', cDecalsCarsRightPlatoonPlaces,  _this getVariable ['rhs_decalPlatoon_type','Army'],call compile _value] ] ] call rhs_fnc_decalsInit};";
            };
            class rhs_hideLightCover {
                displayName = "Hide light covers";
                property = "rhs_hideLightCover";
                control = "CheckboxNumber";
                expression = "_this animate ['light_hide',_value,true]";
                defaultValue = "0";
            };
            class rhs_hidespare : rhs_hideLightCover {
                displayName = "Remove spare wheel";
                property = "rhs_hidespare";
                expression = "_this animate ['spare_hide',_value,true]";
            };
            class Door_LF : rhs_hidespare {
                displayName = "Open front left door";
                property = "Door_LF";
                expression = "_this animateDoor ['%s',_value,true]";
            };
            class Door_RF : Door_LF {
                displayName = "Open front right door";
                property = "Door_RF";
            };
            class Door_LB : Door_LF {
                displayName = "Open left back door";
                property = "Door_LB";
            };
            class Door_RB : Door_LF {
                displayName = "Open right back door";
                property = "Door_RB";
            };
        };
    };

    class SpecLib_AN2 : RHS_AN2 {
        PREVIEW(SpecLib_AN2);
        accuracy = 0.5;
        author = "$STR_RHS_AUTHOR_FULL";
        scope = 2;
        side = 3;
        crew = "SpecLib_c_tk_civ_black_brown_jacket";
        faction = "SpecLib_Faction_Taki_Civilians";
        typicalCargo[] = {"SpecLib_c_tk_civ_cream_tricolor_waistcoat"};
        editorSubcategory = "SpecLib_EdSubcat_Planes";
        displayName = "$STR_RHSGREF_AIR_AN2_NAME";
        rhs_decalParameters[] = {"['Number',[3,4],'DefaultRed']"};
        class TransportItems {
            class _xx_FirstAidKit {
                name = "FirstAidKit";
                count = 10;
            };
        };
        class TransportBackpacks {
            class _xx_rhs_d6_Parachute_backpack {
                backpack = "rhs_d6_Parachute_backpack";
                count = 16;
            };
        };
        hiddenSelections[] = {"Camo1", "Camo2", "Camo3", "n1",
                              "n2",    "i1",    "i2"};
        hiddenSelectionsTextures[] = {
            "rhsgref\addons\rhsgref_air\AN2\data\an2_1_B_CO",
            "rhsgref\addons\rhsgref_air\AN2\data\an2_2_B_CO",
            "rhsgref\addons\rhsgref_air\AN2\data\an2_wings_B_CO"};
        class textureSources {
            class airtak {
                displayName = "AirTak";
                author = "$STR_A3_Bohemia_Interactive";
                textures[] = {
                    "rhsgref\addons\rhsgref_air\AN2\data\an2_1_B_CO",
                    "rhsgref\addons\rhsgref_air\AN2\data\an2_2_B_CO",
                    "rhsgref\addons\rhsgref_air\AN2\data\an2_wings_B_CO"};
                factions[] = {"SpecLib_Faction_Taki_Civilians"};
            };
        };
        textureList[] = {};
        class Attributes {
            class ObjectTexture {
                control = "ObjectTexture";
                data = "ObjectTexture";
                displayName = "Skin";
                tooltip = "Texture and material set applied on the object.";
            };
            class rhs_decalNumber_type {
                displayName = "Define font type of side number";
                tooltip = "Define kind of font that will be drawn on vehicle";
                property = "rhs_decalNumber_type";
                control = "Combo";
                expression =
                    "if(_value != 'NoChange')then{ _this setVariable ['%s', _value];[_this,[['Number', [3,4], _value]]] call rhs_fnc_decalsInit}";
                defaultValue = "DefaultRed";
                typeName = "STRING";
                class values {
                    class DefaultRed {
                        name = "Default (Red)";
                        value = "DefaultRed";
                        defaultValue = "DefaultRed";
                    };
                };
            };
            class rhs_decalNumber {
                displayName = "Set side number";
                tooltip =
                    "Set side number. 2 numbers are required. Type 0 to hide numbers completly";
                property = "rhs_decalNumber";
                control = "Edit";
                validate = "Number";
                defaultValue = "-1";
                expression =
                    "if(parseNumber _value >= 0)then{if(parseNumber _value == 0)then{{[_this setobjectTexture [_x,'a3\data_f\clear_empty.paa']]}foreach [3,4]}else{[_this, [['Number', [3,4], _this getVariable ['rhs_decalNumber_type','AviaCDF'],parseNumber _value] ] ] call rhs_fnc_decalsInit}};";
            };
            class rhs_decalTail {
                displayName = "Define tail decal";
                tooltip = "Define tail decal that will be drawn on vehicle";
                property = "rhs_decalTail";
                control = "Combo";
                expression =
                    "[_this,[['Label', [5], 'Aviation',_value]]] call rhs_fnc_decalsInit";
                defaultValue = 0;
                typeName = "Number";
                class values {
                    class Empty {
                        name = "Empty";
                        value = 0;
                    };
                };
            };
            class door {
                displayName = "Open Door";
                property = "door";
                control = "slider";
                expression = "_this animate ['door',_value];";
                defaultValue = "0";
            };
        };
    };

    class SpecLib_Taki_Civ_Ural_Work : rhsgref_nat_ural_work {
        PREVIEW(SpecLib_Taki_Civ_Ural_Work);
        scope = 2;
        displayName = "$STR_RHS_URAL4320WORKER_NAME";
        author = "RHS (A2 port)";
        side = 3;
        crew = "SpecLib_c_tk_civ_blue_grey_waistcoat";
        faction = "SpecLib_Faction_Taki_Civilians";
        typicalCargo[] = {};
        editorSubcategory = "SpecLib_EdSubcat_Trucks";
        rhs_decalParameters[] = {};
        class AnimationSources : AnimationSources {
            class light_hide {
                source = "user";
                mass = 1;
                initPhase = 1;
                animPeriod = 1e-011;
                displayName = "hide light covers";
            };
        };
        hiddenSelectionsTextures[] = {
            "rhsafrf\addons\rhs_a2port_car\ural\data\ural_kabina_civ1_co.paa",
            "rhsafrf\addons\rhs_a2port_car\ural\data\ural_plachta_civ1_co.paa"};
        class textureSources {
            class Camo4 {
                displayName = "Civil Yellow";
                author = "$STR_RHS_AUTHOR_FULL";
                textures[] = {
                    "rhsafrf\addons\rhs_a2port_car\ural\data\ural_kabina_civ1_co.paa",
                    "rhsafrf\addons\rhs_a2port_car\ural\data\ural_plachta_civ1_co.paa"};
            };
        };
        textureList[] = {};
        class Attributes {
            class ObjectTexture {
                control = "ObjectTexture";
                data = "ObjectTexture";
                displayName = "Skin";
                tooltip = "Texture and material set applied on the object.";
            };
            class rhs_decalNumber_type {
                displayName = "Define font type of plate number";
                tooltip = "Define kind of font that will be drawn on vehicle.";
                property = "rhs_decalNumber_type";
                control = "Combo";
                expression =
                    "_this setVariable ['%s', _value];[_this,[['Number', cDecals4CarsNumberPlaces, _value]]] call rhs_fnc_decalsInit";
                defaultValue = 0;
                typeName = "STRING";
                class values {
                    class Default {
                        name = "Default";
                        value = "Default";
                        defaultValue = "Default";
                    };
                    class DefaultRed {
                        name = "Default (Red)";
                        value = "DefaultRed";
                    };
                    class BoldRed {
                        name = "Bold Red";
                        value = "BoldRed";
                    };
                    class CDF {
                        name = "CDF";
                        value = "CDF";
                    };
                    class Handpaint {
                        name = "Handpaint";
                        value = "Handpaint";
                    };
                    class HandpaintBlack {
                        name = "Handpaint Black";
                        value = "HandpaintBlack";
                    };
                    class Iraqi {
                        name = "Iraqi";
                        value = "Iraqi";
                    };
                    class LicensePlate {
                        name = "License Plate";
                        value = "LicensePlate";
                    };
                };
            };
            class rhs_decalNumber {
                collapsed = 1;
                displayName = "Set plate number";
                tooltip =
                    "Set plate number. 4 numbers are required. If 0, random number will be generated";
                property = "rhs_decalNumber";
                control = "Edit";
                validate = "Number";
                typeName = "Number";
                defaultValue = "-1";
                expression =
                    "if( _value >= 0)then{[_this,[['Number', cDecals4CarsNumberPlaces, _this getVariable ['rhs_decalNumber_type','Default'], _value]]] call rhs_fnc_decalsInit};";
            };
            class rhs_decalArmy_type {
                displayName = "Define large door roundel type";
                tooltip = "Decal type";
                property = "rhs_decalArmy_type";
                control = "Combo";
                expression = "_this setVariable ['%s', _value];";
                defaultValue = "0";
                typeName = "STRING";
                class values {
                    class Army {
                        name = "Army";
                        value = "Army";
                        defaultValue = "Army";
                    };
                    class Honor {
                        name = "Honor";
                        value = "Honor";
                    };
                    class HonorGDR {
                        name = "Honor GDR";
                        value = "HonorGDR";
                    };
                    class Platoon {
                        name = "Platoon";
                        value = "Platoon";
                    };
                    class PlatoonGDR {
                        name = "Platoon GDR";
                        value = "PlatoonGDR";
                    };
                    class PlatoonVDV {
                        name = "Platoon VDV";
                        value = "PlatoonVDV";
                    };
                };
            };
            class rhs_decalArmy {
                displayName = "Set large door roundel symbol";
                tooltip =
                    "Set large door roundel located on both sides. Usually used for army symbols. -1 leaves current symbol & 0 clears decal.";
                property = "rhs_decalArmy";
                control = "Edit";
                validate = "none";
                defaultValue = "-1";
                expression =
                    "if(parseNumber _value >= 0)then{ [_this, [ [ 'Label', cDecalsCarsRightArmyPlaces,  _this getVariable ['rhs_decalArmy_type','Army'],call compile _value] ] ] call rhs_fnc_decalsInit};";
            };
            class rhs_decalPlatoon_type : rhs_decalArmy_type {
                displayName = "Define small door roundel type";
                property = "rhs_decalPlatoon_type";
                expression = "_this setVariable ['%s', _value];";
                defaultValue = "0";
            };
            class rhs_decalPlatoon : rhs_decalArmy {
                displayName = "Set small door roundel symbol";
                tooltip =
                    "Define small door roundel located on both sides. Usually used for platoon symbols. -1 leaves current symbol & 0 clears decal.";
                property = "rhs_decalPlatoon";
                expression =
                    "if(parseNumber _value >= 0)then{ [_this, [ [ 'Label', cDecalsCarsRightPlatoonPlaces,  _this getVariable ['rhs_decalPlatoon_type','Army'],call compile _value] ] ] call rhs_fnc_decalsInit};";
            };
            class rhs_hideLightCover {
                displayName = "Hide light covers";
                property = "rhs_hideLightCover";
                control = "CheckboxNumber";
                expression = "_this animate ['light_hide',_value,true]";
                defaultValue = "0";
            };
            class rhs_hidespare : rhs_hideLightCover {
                displayName = "Remove spare wheel";
                property = "rhs_hidespare";
                expression = "_this animate ['spare_hide',_value,true]";
            };
            class Door_LF : rhs_hidespare {
                displayName = "Open front left door";
                property = "Door_LF";
                expression = "_this animateDoor ['%s',_value,true]";
            };
            class Door_RF : Door_LF {
                displayName = "Open front right door";
                property = "Door_RF";
            };
        };
        class TransportItems {
            class _xx_rds_car_FirstAidKit {
                name = "rds_car_FirstAidKit";
                count = 1;
            };
        };
    };

    class SpecLib_Taki_Civ_Ural_Work_Open : rhsgref_nat_ural_work_open {
        PREVIEW(SpecLib_Taki_Civ_Ural_Work_Open);
        scope = 2;
        displayName = "$STR_RHS_URAL4320WORKEROPEN_NAME";
        author = "RHS (A2 port)";
        side = 3;
        crew = "SpecLib_c_tk_civ_brown";
        faction = "SpecLib_Faction_Taki_Civilians";
        typicalCargo[] = {};
        editorSubcategory = "SpecLib_EdSubcat_Trucks";
        rhs_decalParameters[] = {};
        class textureSources {
            class Camo3 {
                displayName = "Civil Blue";
                author = "$STR_RHS_AUTHOR_FULL";
                textures[] = {
                    "rhsafrf\addons\rhs_a2port_car\ural\data\ural_kabina_civil_co.paa",
                    "rhsafrf\addons\rhs_a2port_car\ural\data\ural_plachta_civil_co.paa"};
            };
        };
        textureList[] = {};
        class Attributes {
            class ObjectTexture {
                control = "ObjectTexture";
                data = "ObjectTexture";
                displayName = "Skin";
                tooltip = "Texture and material set applied on the object.";
            };
            class rhs_decalNumber_type {
                displayName = "Define font type of plate number";
                tooltip = "Define kind of font that will be drawn on vehicle.";
                property = "rhs_decalNumber_type";
                control = "Combo";
                expression =
                    "_this setVariable ['%s', _value];[_this,[['Number', cDecals4CarsNumberPlaces, _value]]] call rhs_fnc_decalsInit";
                defaultValue = 0;
                typeName = "STRING";
                class values {
                    class Default {
                        name = "Default";
                        value = "Default";
                        defaultValue = "Default";
                    };
                    class DefaultRed {
                        name = "Default (Red)";
                        value = "DefaultRed";
                    };
                    class BoldRed {
                        name = "Bold Red";
                        value = "BoldRed";
                    };
                    class CDF {
                        name = "CDF";
                        value = "CDF";
                    };
                    class Handpaint {
                        name = "Handpaint";
                        value = "Handpaint";
                    };
                    class HandpaintBlack {
                        name = "Handpaint Black";
                        value = "HandpaintBlack";
                    };
                    class Iraqi {
                        name = "Iraqi";
                        value = "Iraqi";
                    };
                    class LicensePlate {
                        name = "License Plate";
                        value = "LicensePlate";
                    };
                };
            };
            class rhs_decalNumber {
                collapsed = 1;
                displayName = "Set plate number";
                tooltip =
                    "Set plate number. 4 numbers are required. If 0, random number will be generated";
                property = "rhs_decalNumber";
                control = "Edit";
                validate = "Number";
                typeName = "Number";
                defaultValue = "-1";
                expression =
                    "if( _value >= 0)then{[_this,[['Number', cDecals4CarsNumberPlaces, _this getVariable ['rhs_decalNumber_type','Default'], _value]]] call rhs_fnc_decalsInit};";
            };
            class rhs_decalArmy_type {
                displayName = "Define large door roundel type";
                tooltip = "Decal type";
                property = "rhs_decalArmy_type";
                control = "Combo";
                expression = "_this setVariable ['%s', _value];";
                defaultValue = "0";
                typeName = "STRING";
                class values {
                    class Army {
                        name = "Army";
                        value = "Army";
                        defaultValue = "Army";
                    };
                    class Honor {
                        name = "Honor";
                        value = "Honor";
                    };
                    class HonorGDR {
                        name = "Honor GDR";
                        value = "HonorGDR";
                    };
                    class Platoon {
                        name = "Platoon";
                        value = "Platoon";
                    };
                    class PlatoonGDR {
                        name = "Platoon GDR";
                        value = "PlatoonGDR";
                    };
                    class PlatoonVDV {
                        name = "Platoon VDV";
                        value = "PlatoonVDV";
                    };
                };
            };
            class rhs_decalArmy {
                displayName = "Set large door roundel symbol";
                tooltip =
                    "Set large door roundel located on both sides. Usually used for army symbols. -1 leaves current symbol & 0 clears decal.";
                property = "rhs_decalArmy";
                control = "Edit";
                validate = "none";
                defaultValue = "-1";
                expression =
                    "if(parseNumber _value >= 0)then{ [_this, [ [ 'Label', cDecalsCarsRightArmyPlaces,  _this getVariable ['rhs_decalArmy_type','Army'],call compile _value] ] ] call rhs_fnc_decalsInit};";
            };
            class rhs_decalPlatoon_type : rhs_decalArmy_type {
                displayName = "Define small door roundel type";
                property = "rhs_decalPlatoon_type";
                expression = "_this setVariable ['%s', _value];";
                defaultValue = "0";
            };
            class rhs_decalPlatoon : rhs_decalArmy {
                displayName = "Set small door roundel symbol";
                tooltip =
                    "Define small door roundel located on both sides. Usually used for platoon symbols. -1 leaves current symbol & 0 clears decal.";
                property = "rhs_decalPlatoon";
                expression =
                    "if(parseNumber _value >= 0)then{ [_this, [ [ 'Label', cDecalsCarsRightPlatoonPlaces,  _this getVariable ['rhs_decalPlatoon_type','Army'],call compile _value] ] ] call rhs_fnc_decalsInit};";
            };
            class rhs_hideLightCover {
                displayName = "Hide light covers";
                property = "rhs_hideLightCover";
                control = "CheckboxNumber";
                expression = "_this animate ['light_hide',_value,true]";
                defaultValue = "0";
            };
            class rhs_hidespare : rhs_hideLightCover {
                displayName = "Remove spare wheel";
                property = "rhs_hidespare";
                expression = "_this animate ['spare_hide',_value,true]";
            };
            class Door_LF : rhs_hidespare {
                displayName = "Open front left door";
                property = "Door_LF";
                expression = "_this animateDoor ['%s',_value,true]";
            };
            class Door_RF : Door_LF {
                displayName = "Open front right door";
                property = "Door_RF";
            };
        };
        class AnimationSources : AnimationSources {
            class light_hide {
                source = "user";
                mass = 1;
                initPhase = 1;
                animPeriod = 1e-011;
                displayName = "hide light covers";
            };
        };
        model = "\rhsafrf\addons\rhs_a2port_car\Ural\Ural_open.p3d";
        hiddenSelectionsTextures[] = {
            "rhsafrf\addons\rhs_a2port_car\ural\data\ural_kabina_civil_co.paa",
            "rhsafrf\addons\rhs_a2port_car\ural\data\ural_plachta_civil_co.paa"};
        class TransportItems {
            class _xx_rds_car_FirstAidKit {
                name = "rds_car_FirstAidKit";
                count = 1;
            };
        };
    };
};
