class CfgPatches {
    class ADDON {
        name = COMPONENT_NAME;
        units[] = {};
        weapons[] = {};
        requiredVersion = REQUIRED_VERSION;
        requiredAddons[] = {"A3_Anims_F_Config_Sdr", "A3_Functions_F",
                            "ace_main"};
        author = "Reidond";
        authors[] = {""};
        url = "https://gitlab.com/Reidond";
        VERSION_CONFIG;
    };
};
