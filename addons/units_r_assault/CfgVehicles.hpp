class CfgVehicles {
    class YuE_6sh92rOl;
    class O_R_recon_TL_F;
    class O_R_recon_AR_F;
    class O_R_recon_medic_F;
    class O_R_recon_M_F;
    class O_R_Soldier_recon_base;

    class YuE_6sh92rOl_Medic : YuE_6sh92rOl {
        scope = 1;
        class TransportMagazines {
            mag_xx(VTN_RDGM, 4);
        };
        class TransportItems {
            item_xx(ACE_fieldDressing, 14);
            item_xx(ACE_elasticBandage, 14);
            item_xx(ACE_quikclot, 14);
            item_xx(ACE_packingBandage, 14);
            item_xx(ACE_tourniquet, 4);
            item_xx(ACE_morphine, 6);
            item_xx(ACE_epinephrine, 6);
            item_xx(ACE_adenosine, 6);
            item_xx(ACE_salineIV, 2);
            item_xx(ACE_salineIV_500, 4);
            item_xx(ACE_surgicalKit, 1);
            item_xx(ACE_splint, 6);
        };
    };

    class SpecLib_O_R_Assault_TL_01_F : O_R_recon_TL_F {
        UNIT_ENTRIES;
        PREVIEW(SpecLib_O_R_Assault_TL_01_F);

        uniformClass = "Spec_Gorka_p_nkl_EAST_Uniform";

        displayName = "Командир отделения";
        weapons[] = {"CUP_arifle_AK74M_GL_ekp1",
                     "VTN_GSH18",
                     DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"CUP_arifle_AK74M_GL_ekp1",
                            "VTN_GSH18",
                            DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_7("rhs_30Rnd_545x39_7N22_AK"),
                       mag_3("VTN_GSH18_18s_PBP"),
                       mag_2("rhs_30Rnd_545x39_AK_plum_green"),
                       GRENADES,
                       GRENADE_LAUNCHER_ROUNDS};
        respawnMagazines[] = {mag_7("rhs_30Rnd_545x39_7N22_AK"),
                              mag_3("VTN_GSH18_18s_PBP"),
                              mag_2("rhs_30Rnd_545x39_AK_plum_green"),
                              GRENADES,
                              GRENADE_LAUNCHER_ROUNDS};
        Items[] = {ITEMS};
        RespawnItems[] = {ITEMS};
        linkedItems[] = {"KoraKulon_SAKVOGd_FOs",
                         "ZSH1_2_F_ON",
                         "YuEBalaklava4bl",
                         LINKED_ITEMS_TL};
        respawnLinkedItems[] = {"KoraKulon_SAKVOGd_FOs",
                                "ZSH1_2_F_ON",
                                "YuEBalaklava4bl",
                                LINKED_ITEMS_TL};
    };
    class SpecLib_O_R_Assault_TL_02_F : O_R_recon_TL_F {
        UNIT_ENTRIES;
        PREVIEW(SpecLib_O_R_Assault_TL_02_F);

        uniformClass = "Spec_Gorka_p_nkl_EAST_Uniform";

        displayName = "Командир отделения ПБС";
        weapons[] = {"gs_9a91_pbs_okp7", "VTN_GSH18", DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"gs_9a91_pbs_okp7",
                            "VTN_GSH18",
                            DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_9("gs_20r_9x39mm_BP_VSK"),
                       mag_3("VTN_GSH18_18s_PBP"),
                       GRENADES};
        respawnMagazines[] = {mag_9("gs_20r_9x39mm_BP_VSK"),
                              mag_3("VTN_GSH18_18s_PBP"),
                              GRENADES};
        Items[] = {"VTN_1PN74",
                         ITEMS};
        RespawnItems[] = {"VTN_1PN74",
                         ITEMS};
        linkedItems[] = {"KoraKulon_SAKd_FOs",
                         "ZSH1_2_F",
                         "YuEBalaklava4bl",
                         LINKED_ITEMS};
        respawnLinkedItems[] = {"KoraKulon_SAKd_FOs",
                                "ZSH1_2_F",
                                "YuEBalaklava4bl",
                                LINKED_ITEMS};
    };
    class SpecLib_O_R_Assault_MachineGunner_F : O_R_recon_AR_F {
        UNIT_ENTRIES;
        PREVIEW(SpecLib_O_R_Assault_MachineGunner_F);

        role = "MachineGunner";
        icon = "iconManMG";

        uniformClass = "Spec_Gorka_p_nkl_nlk_EAST_Uniform";

        displayName = "Пулемётчик";
        weapons[] = {"VTN_PKP", "VTN_GSH18", DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"VTN_PKP", "VTN_GSH18", DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_4("rhs_100Rnd_762x54mmR_7BZ3"),
                       mag_3("VTN_GSH18_18s_PBP"),
                       GRENADES};
        respawnMagazines[] = {mag_4("rhs_100Rnd_762x54mmR_7BZ3"),
                              mag_3("VTN_GSH18_18s_PBP"),
                              GRENADES};
        Items[] = {ITEMS};
        RespawnItems[] = {ITEMS};
        linkedItems[] = {"KoraKulon_SPKd_FOs",
                         "ZSH1_2_F_ON",
                         "YuEBalaklava4bl",
                         LINKED_ITEMS};
        respawnLinkedItems[] = {"KoraKulon_SPKd_FOs",
                                "ZSH1_2_F_ON",
                                "YuEBalaklava4bl",
                                LINKED_ITEMS};
    };
    class SpecLib_O_R_Assault_medic_01_F : O_R_recon_medic_F {
        UNIT_ENTRIES;
        PREVIEW(SpecLib_O_R_Assault_medic_01_F);

        backpack = "YuE_6sh92rOl_Medic";

        uniformClass = "Spec_Gorka_p_nkl_EAST_Uniform";

        displayName = "Санитар";
        weapons[] = {"CUP_arifle_AK74M_ekp1",
                     "VTN_GSH18",
                     DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"CUP_arifle_AK74M_ekp1",
                            "VTN_GSH18",
                            DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_9("rhs_30Rnd_545x39_7N22_AK"),
                       mag_3("VTN_GSH18_18s_PBP"),
                       GRENADES};
        respawnMagazines[] = {mag_9("rhs_30Rnd_545x39_7N22_AK"),
                              mag_3("VTN_GSH18_18s_PBP"),
                              GRENADES};
        Items[] = {ITEMS};
        RespawnItems[] = {ITEMS};
        linkedItems[] = {"KoraKulon_SAK_FOs",
                         "ZSH1_2_F_ON",
                         "YuEBalaklava4bl",
                         LINKED_ITEMS};
        respawnLinkedItems[] = {"KoraKulon_SAK_FOs",
                                "ZSH1_2_F_ON",
                                "YuEBalaklava4bl",
                                LINKED_ITEMS};
    };
    class SpecLib_O_R_Assault_medic_02_F : O_R_recon_medic_F {
        UNIT_ENTRIES;
        PREVIEW(SpecLib_O_R_Assault_medic_02_F);

        backpack = "YuE_6sh92rOl_Medic";

        uniformClass = "Spec_Gorka_p_nkl_EAST_Uniform";

        displayName = "Санитар ПБС";
        weapons[] = {"gs_9a91_pbs_okp7", "VTN_GSH18", DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"gs_9a91_pbs_okp7",
                            "VTN_GSH18",
                            DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_9("gs_20r_9x39mm_BP_VSK"),
                       mag_3("VTN_GSH18_18s_PBP"),
                       GRENADES};
        respawnMagazines[] = {mag_9("gs_20r_9x39mm_BP_VSK"),
                              mag_3("VTN_GSH18_18s_PBP"),
                              GRENADES};
        Items[] = {"VTN_1PN74",
                         ITEMS};
        RespawnItems[] = {"VTN_1PN74",
                         ITEMS};
        linkedItems[] = {"KoraKulon_SAKd_FOs",
                         "ZSH1_2_F",
                         "YuEBalaklava4bl",
                         LINKED_ITEMS};
        respawnLinkedItems[] = {"KoraKulon_SAKd_FOs",
                                "ZSH1_2_F",
                                "YuEBalaklava4bl",
                                LINKED_ITEMS};
    };
    class SpecLib_O_R_Assault_M_01_F : O_R_recon_M_F {
        UNIT_ENTRIES;
        PREVIEW(SpecLib_O_R_Assault_M_01_F);

        uniformClass = "Spec_Gorka_p_nkl_EAST_Uniform";

        displayName = "Снайпер";
        weapons[] = {"VTN_SVD_PSO1M2", "VTN_GSH18", DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"VTN_SVD_PSO1M2",
                            "VTN_GSH18",
                            DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_9("VTN_SVD_10s_AP"),
                       mag_3("VTN_GSH18_18s_PBP"),
                       GRENADES};
        respawnMagazines[] = {mag_9("VTN_SVD_10s_AP"),
                              mag_3("VTN_GSH18_18s_PBP"),
                              GRENADES};
        Items[] = {ITEMS};
        RespawnItems[] = {ITEMS};
        linkedItems[] = {"KoraKulon_SSVDd_FOs",
                         "ZSH1_2_F_OV",
                         "YuEBalaklava4bl",
                         LINKED_ITEMS};
        respawnLinkedItems[] = {"KoraKulon_SSVDd_FOs",
                                "ZSH1_2_F_OV",
                                "YuEBalaklava4bl",
                                LINKED_ITEMS};
    };
    class SpecLib_O_R_Assault_M_02_F : O_R_recon_M_F {
        UNIT_ENTRIES;
        PREVIEW(SpecLib_O_R_Assault_M_02_F);

        uniformClass = "Spec_Gorka_p_nkl_EAST_Uniform";

        displayName = "Снайпер ПБС";
        weapons[] = {"gs_vsk94_pbs_pso1_1",
                     "VTN_GSH18",
                     DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"gs_vsk94_pbs_pso1_1",
                            "VTN_GSH18",
                            DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_9("gs_20r_9x39mm_BP_VSK"),
                       mag_3("VTN_GSH18_18s_PBP"),
                       GRENADES};
        Items[] = {"VTN_1PN74",
                         ITEMS};
        RespawnItems[] = {"VTN_1PN74",
                         ITEMS};
        respawnMagazines[] = {mag_9("gs_20r_9x39mm_BP_VSK"),
                              mag_3("VTN_GSH18_18s_PBP"),
                              GRENADES};
        Items[] = {ITEMS};
        RespawnItems[] = {ITEMS};
        linkedItems[] = {"KoraKulon_SAKd_FOs",
                         "ZSH1_2_F",
                         "YuEBalaklava4bl",
                         LINKED_ITEMS};
        respawnLinkedItems[] = {"KoraKulon_SAKd_FOs",
                                "ZSH1_2_F",
                                "YuEBalaklava4bl",
                                LINKED_ITEMS};
    };
    class SpecLib_O_R_Assault_01_F : O_R_Soldier_recon_base {
        UNIT_ENTRIES;
        PREVIEW(SpecLib_O_R_Assault_01_F);

        uniformClass = "Spec_Gorka_p_nkl_nlk_EAST_Uniform";

        displayName = "Стрелок";
        weapons[] = {"CUP_arifle_AK74M_ekp1",
                     "VTN_GSH18",
                     DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"CUP_arifle_AK74M_ekp1",
                            "VTN_GSH18",
                            DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_9("rhs_30Rnd_545x39_7N22_AK"),
                       mag_3("VTN_GSH18_18s_PBP"),
                       GRENADES};
        respawnMagazines[] = {mag_9("rhs_30Rnd_545x39_7N22_AK"),
                              mag_3("VTN_GSH18_18s_PBP"),
                              GRENADES};
        Items[] = {ITEMS};
        RespawnItems[] = {ITEMS};
        linkedItems[] = {"KoraKulon_SAKd_FOs",
                         "ZSH1_2_F_ZN",
                         "YuEBalaklava4bl",
                         LINKED_ITEMS};
        respawnLinkedItems[] = {"KoraKulon_SAKd_FOs",
                                "ZSH1_2_F_ZN",
                                "YuEBalaklava4bl",
                                LINKED_ITEMS};
    };
    class SpecLib_O_R_Assault_02_F : SpecLib_O_R_Assault_01_F {
        PREVIEW(SpecLib_O_R_Assault_02_F);

        displayName = "Стрелок ПБС";
        weapons[] = {"gs_9a91_pbs_okp7", "VTN_GSH18", DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"gs_9a91_pbs_okp7",
                            "VTN_GSH18",
                            DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_9("gs_20r_9x39mm_BP_VSK"),
                       mag_3("VTN_GSH18_18s_PBP"),
                       GRENADES};
        respawnMagazines[] = {mag_9("gs_20r_9x39mm_BP_VSK"),
                              mag_3("VTN_GSH18_18s_PBP"),
                              GRENADES};
        Items[] = {"VTN_1PN74",
                         ITEMS};
        RespawnItems[] = {"VTN_1PN74",
                         ITEMS};
        linkedItems[] = {"KoraKulon_SAKd_FOs",
                         "ZSH1_2_F",
                         "YuEBalaklava4bl",
                         LINKED_ITEMS};
        respawnLinkedItems[] = {"KoraKulon_SAKd_FOs",
                                "ZSH1_2_F",
                                "YuEBalaklava4bl",
                                LINKED_ITEMS};
    };
    class SpecLib_O_R_Assault_GL_01_F : SpecLib_O_R_Assault_01_F {
        PREVIEW(SpecLib_O_R_Assault_GL_01_F);

        displayName = "Стрелок ГП";
        weapons[] = {"CUP_arifle_AK74M_GL_ekp1",
                     "VTN_GSH18",
                     DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"CUP_arifle_AK74M_GL_ekp1",
                            "VTN_GSH18",
                            DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_9("rhs_30Rnd_545x39_7N22_AK"),
                       GRENADES,
                       GRENADE_LAUNCHER_ROUNDS};
        respawnMagazines[] = {mag_9("rhs_30Rnd_545x39_7N22_AK"),
                              mag_3("VTN_GSH18_18s_PBP"),
                              GRENADES,
                              GRENADE_LAUNCHER_ROUNDS};
        Items[] = {ITEMS};
        RespawnItems[] = {ITEMS};
        linkedItems[] = {"KoraKulon_SAKVOGd_FOs",
                         "ZSH1_2_F_ZN",
                         "YuEBalaklava4bl",
                         LINKED_ITEMS};
        respawnLinkedItems[] = {"KoraKulon_SAKVOGd_FOs",
                                "ZSH1_2_F_ZN",
                                "YuEBalaklava4bl",
                                LINKED_ITEMS};
    };
    class SpecLib_O_R_Assault_GL_02_F : SpecLib_O_R_Assault_GL_01_F {
        PREVIEW(SpecLib_O_R_Assault_GL_02_F);

        displayName = "Стрелок ГП ПБС";
        weapons[] = {"CUP_arifle_AK74M_GL_gp25_tgpa_ekp1",
                     "VTN_GSH18",
                     DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"CUP_arifle_AK74M_GL_gp25_tgpa_ekp1",
                            "VTN_GSH18",
                            DEFAULT_UNIT_WEAPONS};
        Items[] = {"VTN_1PN74",
                         ITEMS};
        RespawnItems[] = {"VTN_1PN74",
                         ITEMS};
        linkedItems[] = {"KoraKulon_SAKVOGd_FOs",
                         "ZSH1_2_F",
                         "YuEBalaklava4bl",
                         LINKED_ITEMS};
        respawnLinkedItems[] = {"KoraKulon_SAKVOGd_FOs",
                                "ZSH1_2_F",
                                "YuEBalaklava4bl",
                                LINKED_ITEMS};
    };
    class SpecLib_O_R_Assault_AT_01_F : SpecLib_O_R_Assault_01_F {
        PREVIEW(SpecLib_O_R_Assault_AT_01_F);

        icon = "iconManAT";
        role = "MissileSpecialist";

        displayName = "Стрелок (РПГ-26)";
        weapons[] = {"CUP_arifle_AK74M_ekp1",
                     "rhs_weap_rpg26",
                     "VTN_GSH18",
                     DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"CUP_arifle_AK74M_ekp1",
                            "rhs_weap_rpg26",
                            "VTN_GSH18",
                            DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_9("rhs_30Rnd_545x39_7N22_AK"),
                       "rhs_rpg26_mag",
                       mag_3("VTN_GSH18_18s_PBP"),
                       GRENADES};
        respawnMagazines[] = {mag_9("rhs_30Rnd_545x39_7N22_AK"),
                              "rhs_rpg26_mag",
                              mag_3("VTN_GSH18_18s_PBP"),
                              GRENADES};
        linkedItems[] = {"KoraKulon_SAKd_FOs",
                         "ZSH1_2_F_ON",
                         "YuEBalaklava4bl",
                         LINKED_ITEMS};
        respawnLinkedItems[] = {"KoraKulon_SAKd_FOs",
                                "ZSH1_2_F_ON",
                                "YuEBalaklava4bl",
                                LINKED_ITEMS};
    };
    class SpecLib_O_R_Assault_AT_02_F : SpecLib_O_R_Assault_01_F {
        PREVIEW(SpecLib_O_R_Assault_AT_02_F);

        icon = "iconManAT";
        role = "MissileSpecialist";

        displayName = "Стрелок (РПГ-26) ПБС";
        weapons[] = {"gs_9a91_pbs_okp7",
                     "rhs_weap_rpg26",
                     "VTN_GSH18",
                     DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"gs_9a91_pbs_okp7",
                            "rhs_weap_rpg26",
                            "VTN_GSH18",
                            DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_9("gs_20r_9x39mm_BP_VSK"),
                       "rhs_rpg26_mag",
                       mag_3("VTN_GSH18_18s_PBP"),
                       GRENADES};
        respawnMagazines[] = {mag_9("gs_20r_9x39mm_BP_VSK"),
                              "rhs_rpg26_mag",
                              mag_3("VTN_GSH18_18s_PBP"),
                              GRENADES};
        Items[] = {"VTN_1PN74",
                         ITEMS};
        RespawnItems[] = {"VTN_1PN74",
                         ITEMS};
        linkedItems[] = {"KoraKulon_SAKd_FOs",
                         "ZSH1_2_F",
                         "YuEBalaklava4bl",
                         LINKED_ITEMS};
        respawnLinkedItems[] = {"KoraKulon_SAKd_FOs",
                                "ZSH1_2_F",
                                "YuEBalaklava4bl",
                                LINKED_ITEMS};
    };
    class SpecLib_O_R_Assault_AT_03_F : SpecLib_O_R_Assault_01_F {
        PREVIEW(SpecLib_O_R_Assault_AT_03_F);

        icon = "iconManAT";
        role = "MissileSpecialist";

        displayName = "Стрелок (РШГ-2)";
        weapons[] = {"CUP_arifle_AK74M_ekp1",
                     "rhs_weap_rshg2",
                     "VTN_GSH18",
                     DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"CUP_arifle_AK74M_ekp1",
                            "rhs_weap_rshg2",
                            "VTN_GSH18",
                            DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_9("rhs_30Rnd_545x39_7N22_AK"),
                       "rhs_rshg2_mag",
                       mag_3("VTN_GSH18_18s_PBP"),
                       GRENADES};
        respawnMagazines[] = {mag_9("rhs_30Rnd_545x39_7N22_AK"),
                              "rhs_rshg2_mag",
                              mag_3("VTN_GSH18_18s_PBP"),
                              GRENADES};
        linkedItems[] = {"KoraKulon_SAKd_FOs",
                         "ZSH1_2_F_ON",
                         "YuEBalaklava4bl",
                         LINKED_ITEMS};
        respawnLinkedItems[] = {"KoraKulon_SAKd_FOs",
                                "ZSH1_2_F_ON",
                                "YuEBalaklava4bl",
                                LINKED_ITEMS};
    };
    class SpecLib_O_R_Assault_AT_04_F : SpecLib_O_R_Assault_01_F {
        PREVIEW(SpecLib_O_R_Assault_AT_04_F);

        icon = "iconManAT";
        role = "MissileSpecialist";

        displayName = "Стрелок (РШГ-2) ПБС";
        weapons[] = {"gs_9a91_pbs_okp7",
                     "rhs_weap_rshg2",
                     "VTN_GSH18",
                     DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"gs_9a91_pbs_okp7",
                            "rhs_weap_rshg2",
                            "VTN_GSH18",
                            DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_9("gs_20r_9x39mm_BP_VSK"),
                       "rhs_rshg2_mag",
                       mag_3("VTN_GSH18_18s_PBP"),
                       GRENADES};
        respawnMagazines[] = {mag_9("gs_20r_9x39mm_BP_VSK"),
                              "rhs_rshg2_mag",
                              mag_3("VTN_GSH18_18s_PBP"),
                              GRENADES};
        Items[] = {"VTN_1PN74",
                         ITEMS};
        RespawnItems[] = {"VTN_1PN74",
                         ITEMS};
        linkedItems[] = {"KoraKulon_SAKd_FOs",
                         "ZSH1_2_F",
                         "YuEBalaklava4bl",
                         LINKED_ITEMS};
        respawnLinkedItems[] = {"KoraKulon_SAKd_FOs",
                                "ZSH1_2_F",
                                "YuEBalaklava4bl",
                                LINKED_ITEMS};
    };
};
