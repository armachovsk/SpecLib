#define COMPONENT units_r_assault
#include "\z\speclib\addons\main\script_mod.hpp"

// #define DEBUG_MODE_FULL
// #define DISABLE_COMPILE_CACHE

#ifdef DEBUG_ENABLED_UNITS_R_ASSAULT
#define DEBUG_MODE_FULL
#endif
#ifdef DEBUG_SETTINGS_UNITS_R_ASSAULT
#define DEBUG_SETTINGS DEBUG_SETTINGS_UNITS_R_ASSAULT
#endif

#include "\z\speclib\addons\main\script_macros.hpp"

#define GRENADE_LAUNCHER_ROUNDS                                                \
    mag_7("rhs_VOG25"), mag_2("rhs_VG40MD"), mag_2("rhs_VG40TB")

#define GRENADES mag_2("rhs_mag_rgn"), mag_2("rhs_mag_rgo"), "VTN_RDGM"

#define ACE_ITEMS                                                              \
    "ACE_morphine", "ACE_tourniquet", "ACE_fieldDressing",                     \
        "ACE_fieldDressing", "ACE_packingBandage", "ACE_packingBandage",       \
        "ACE_elasticBandage", "ACE_elasticBandage", "ACE_quikclot",            \
        "ACE_quikclot", "ACE_EarPlugs"

#define ITEMS ACE_ITEMS

#define LINKED_ITEMS_TL                                                        \
    "ItemMap", "ItemCompass", "ItemWatch", "ItemRadio"

#define LINKED_ITEMS "ItemMap", "ItemCompass", "ItemWatch", "ItemRadio"

#define DEFAULT_UNIT_WEAPONS "Throw", "Put"

#define UNIT_ENTRIES                                                           \
    author = CSTRING(SpecLibTeam);                                             \
    dlc = "speclib";                                                           \
    scope = 2;                                                                 \
    scopeCurator = 2;                                                          \
    faction = "rhs_faction_vv";                                                \
    editorSubcategory = "SpecLib_EdSubcat_R_Assault";                          \
    nameSound = "veh_infantry_SF_s";                                           \
    textPlural = "specops";                                                    \
    textSingular = "specop";                                                   \
    selectionClan = "";                                                        \
    class EventHandlers {                                                      \
        class CBA_Extended_EventHandlers : CBA_Extended_EventHandlers {};      \
    };                                                                         \
    headgearList[] = {}
