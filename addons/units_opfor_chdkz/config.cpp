#include "script_component.hpp"
class CfgPatches {
    class ADDON {
        name = COMPONENT;
        units[] = {
            "SpecLib_O_ChDKZ_Soldier_SL_F",
            "SpecLib_O_ChDKZ_Crew_SL_F",
            "SpecLib_O_ChDKZ_Crew_F",
            "SpecLib_O_ChDKZ_officer_F",
            "SpecLib_O_ChDKZ_Soldier_AR_F",
            "SpecLib_O_ChDKZ_Soldier_MG_F",
            "SpecLib_O_ChDKZ_Soldier_AMG_F",
            "SpecLib_O_ChDKZ_Soldier_AT_F",
            "SpecLib_O_ChDKZ_Soldier_AAT_F",
            "SpecLib_O_ChDKZ_medic_F",
            "SpecLib_O_ChDKZ_engineer_F",
            "SpecLib_O_ChDKZ_Soldier_01_F",
            "SpecLib_O_ChDKZ_Soldier_02_F",
            "SpecLib_O_ChDKZ_Soldier_GL_F",
            "SpecLib_O_ChDKZ_Soldier_LAT_F",
            "SpecLib_O_ChDKZ_Soldier_AA_F",
            "SpecLib_O_ChDKZ_Soldier_M_F",
            "SpecLib_O_ChDKZ_soldier_exp_F",
            "SpecLib_O_ChDKZ_soldier_mine_F",
            "SpecLib_O_ChDKZ_Pilot_F"
        };
        weapons[] = {};
        requiredVersion = REQUIRED_VERSION;
        requiredAddons[] = {"cba_main",
                            "ace_main",
                            "speclib_units_chdkz_base",
                            "speclib_units_ind_chdkz"};
        author = "Reidond";
        VERSION_CONFIG;
    };
};

#include "CfgEditorSubcategories.hpp"
#include "CfgEventHandlers.hpp"
#include "CfgVehicles.hpp"
