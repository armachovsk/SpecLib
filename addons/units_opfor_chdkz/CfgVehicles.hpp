class CfgVehicles {
    class O_Soldier_SL_F;
    class O_Soldier_F;
    class O_officer_F;
    class O_Soldier_AR_F;
    class O_Soldier_LAT_F;
    class O_medic_F;
    class O_engineer_F;
    class O_Soldier_GL_F;
    class O_Soldier_M_F;
    class O_soldier_exp_F;
    class O_soldier_mine_F;
    class O_Pilot_F;

    class SpecLib_O_ChDKZ_Soldier_SL_F : O_Soldier_SL_F {
        UNIT_ENTRIES;
        PREVIEW(SpecLib_O_ChDKZ_Soldier_SL_F);
        uniformClass = "T_CHDKZ_chedak3_uniform";

        displayName = "Командир отделения";
        weapons[] = {"SpecLib_arifle_AKS74_PSO1",
                     "VTN_B8",
                     DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"SpecLib_arifle_AKS74_PSO1",
                            "VTN_B8",
                            DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_9("SPEC_30Rnd_545x39_7N6M_AK_M"), DEFAULT_GRENADES};
        respawnMagazines[] = {mag_9("SPEC_30Rnd_545x39_7N6M_AK_M"),
                              DEFAULT_GRENADES};
        Items[] = {MED_ITEMS, REGULAR_ITEMS, SMERSH_ITEMS};
        RespawnItems[] = {MED_ITEMS, REGULAR_ITEMS, SMERSH_ITEMS};
        linkedItems[] = {"RPS_Smersh13",
                         "rhsgref_patrolcap_specter",
                         "rhsusf_shemagh_tan",
                         LINKED_ITEMS_FULL};
        respawnLinkedItems[] = {"RPS_Smersh13",
                                "rhsgref_patrolcap_specter",
                                "rhsusf_shemagh_tan",
                                LINKED_ITEMS_FULL};
    };

    class SpecLib_O_ChDKZ_Crew_SL_F : O_Soldier_SL_F {
        UNIT_ENTRIES;
        PREVIEW(SpecLib_O_ChDKZ_Crew_SL_F);
        uniformClass = "T_CHDKZ_chedak3_uniform";

        displayName = "Командир экипажа";
        weapons[] = {"CUP_arifle_AKS74U", "VTN_B8", DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"CUP_arifle_AKS74U",
                            "VTN_B8",
                            DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_4("SPEC_30Rnd_545x39_7N6M_AK_M")};
        respawnMagazines[] = {mag_4("SPEC_30Rnd_545x39_7N6M_AK_M")};
        Items[] = {MED_ITEMS};
        RespawnItems[] = {MED_ITEMS};
        linkedItems[] = {"rhs_6sh46", "rhs_tsh4", LINKED_ITEMS_FULL};
        respawnLinkedItems[] = {"rhs_6sh46", "rhs_tsh4", LINKED_ITEMS_FULL};
    };

    class SpecLib_O_ChDKZ_Crew_F : O_Soldier_F {
        UNIT_ENTRIES;
        PREVIEW(SpecLib_O_ChDKZ_Crew_F);
        uniformClass = "T_CHDKZ_chedak5_uniform";

        displayName = "Член экипажа";
        weapons[] = {"CUP_arifle_AKS74U", DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"CUP_arifle_AKS74U", DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_4("SPEC_30Rnd_545x39_7N6M_AK_M")};
        respawnMagazines[] = {mag_4("SPEC_30Rnd_545x39_7N6M_AK_M")};
        Items[] = {MED_ITEMS};
        RespawnItems[] = {MED_ITEMS};
        linkedItems[] = {"rhs_6sh46", "rhs_tsh4", LINKED_ITEMS_FULL};
        respawnLinkedItems[] = {"rhs_6sh46", "rhs_tsh4", LINKED_ITEMS_FULL};
    };

    class SpecLib_O_ChDKZ_officer_F : O_officer_F {
        UNIT_ENTRIES;
        PREVIEW(SpecLib_O_ChDKZ_officer_F);
        uniformClass = "T_CHDKZ_chedak2_uniform";

        displayName = "Полевой командир";
        weapons[] = {"rhs_weap_makarov_pm", "VTN_B8", DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"rhs_weap_makarov_pm",
                            "VTN_B8",
                            DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_4("rhs_mag_9x18_8_57N181S")};
        respawnMagazines[] = {mag_4("rhs_mag_9x18_8_57N181S")};
        Items[] = {MED_ITEMS};
        RespawnItems[] = {MED_ITEMS};
        linkedItems[] = {"rhs_vest_pistol_holster",
                         "T_ChDKZ_Beret",
                         LINKED_ITEMS_FULL};
        respawnLinkedItems[] = {"rhs_vest_pistol_holster",
                                "T_ChDKZ_Beret",
                                LINKED_ITEMS_FULL};
    };

    class SpecLib_O_ChDKZ_Soldier_AR_F : O_Soldier_AR_F {
        UNIT_ENTRIES;
        PREVIEW(SpecLib_O_ChDKZ_Soldier_AR_F);
        uniformClass = "T_CHDKZ_chedak2_uniform";
        backpack = "YuE_RD54_Soldier_AR_F";

        displayName = "Пулемётчик (РПК-74)";
        weapons[] = {"CUP_arifle_RPK74_45", DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"CUP_arifle_RPK74_45", DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_9("SPEC_45Rnd_TE4_LRT4_7N6M_545x39_RPK_M"),
                       DEFAULT_GRENADES};
        respawnMagazines[] = {mag_9("SPEC_45Rnd_TE4_LRT4_7N6M_545x39_RPK_M"),
                              DEFAULT_GRENADES};
        Items[] = {MED_ITEMS, REGULAR_ITEMS, SMERSH_ITEMS};
        RespawnItems[] = {MED_ITEMS, REGULAR_ITEMS, SMERSH_ITEMS};
        linkedItems[] = {"RPS_Smersh13", "YuEShapka1Camo", LINKED_ITEMS};
        respawnLinkedItems[] = {"RPS_Smersh13", "YuEShapka1Camo", LINKED_ITEMS};
    };

    class SpecLib_O_ChDKZ_Soldier_MG_F : O_Soldier_AR_F {
        UNIT_ENTRIES;
        PREVIEW(SpecLib_O_ChDKZ_Soldier_MG_F);
        uniformClass = "T_CHDKZ_chedak6_uniform";
        backpack = "YuE_RD54_Soldier_MG_F";

        displayName = "Пулемётчик";
        weapons[] = {"VTN_PKM_1974", DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"VTN_PKM_1974", DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_4("rhs_100Rnd_762x54mmR"),
                       DEFAULT_GRENADES};
        respawnMagazines[] = {mag_4("rhs_100Rnd_762x54mmR"),
                              DEFAULT_GRENADES};
        Items[] = {MED_ITEMS, REGULAR_ITEMS, SMERSH_ITEMS};
        RespawnItems[] = {MED_ITEMS, REGULAR_ITEMS, SMERSH_ITEMS};
        linkedItems[] = {"RPS_Smersh3", "YuEBalaklavaW2o", LINKED_ITEMS};
        respawnLinkedItems[] = {"RPS_Smersh3", "YuEBalaklavaW2o", LINKED_ITEMS};
    };

    class SpecLib_O_ChDKZ_Soldier_AMG_F : O_Soldier_F {
        UNIT_ENTRIES;
        PREVIEW(SpecLib_O_ChDKZ_Soldier_AMG_F);
        uniformClass = "T_CHDKZ_chedak6_uniform";
        backpack = "YuE_RD54_Soldier_AMG_F";

        displayName = "Помощник пулемётчика";
        weapons[] = {"CUP_arifle_AKM_Early", DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"CUP_arifle_AKM_Early", DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_4("SPEC_30Rnd_762x39_AK47_M"), DEFAULT_GRENADES};
        respawnMagazines[] = {mag_4("SPEC_30Rnd_762x39_AK47_M"),
                              DEFAULT_GRENADES};
        Items[] = {MED_ITEMS, REGULAR_ITEMS};
        RespawnItems[] = {MED_ITEMS, REGULAR_ITEMS};
        linkedItems[] = {"rhsgref_chicom_m88", "YuEShapka1Camo", LINKED_ITEMS};
        respawnLinkedItems[] = {"rhsgref_chicom_m88",
                                "YuEShapka1Camo",
                                LINKED_ITEMS};
    };

    class SpecLib_O_ChDKZ_Soldier_AT_F : O_Soldier_LAT_F {
        UNIT_ENTRIES;
        PREVIEW(SpecLib_O_ChDKZ_Soldier_AT_F);
        uniformClass = "T_CHDKZ_chedak4_uniform";
        backpack = "rhs_rpg_empty_Soldier_AT_F";

        displayName = "Гранатомётчик";
        weapons[] = {"CUP_arifle_AKMS_Early",
                     "rhs_weap_rpg7",
                     DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"CUP_arifle_AKMS_Early",
                            "rhs_weap_rpg7",
                            DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_4("SPEC_30Rnd_762x39_AK47_M"),
                       "rhs_rpg7_PG7VL_mag",
                       DEFAULT_GRENADES};
        respawnMagazines[] = {mag_4("SPEC_30Rnd_762x39_AK47_M"),
                              "rhs_rpg7_PG7VL_mag",
                              DEFAULT_GRENADES};
        Items[] = {MED_ITEMS, REGULAR_ITEMS};
        RespawnItems[] = {MED_ITEMS, REGULAR_ITEMS};
        linkedItems[] = {"rhsgref_chicom_m88", "YuEShapka1Camo", LINKED_ITEMS};
        respawnLinkedItems[] = {"rhsgref_chicom_m88",
                                "YuEShapka1Camo",
                                LINKED_ITEMS};
    };

    class SpecLib_O_ChDKZ_Soldier_AAT_F : O_Soldier_F {
        UNIT_ENTRIES;
        PREVIEW(SpecLib_O_ChDKZ_Soldier_AAT_F);
        uniformClass = "T_CHDKZ_chedak5_uniform";
        backpack = "rhs_rpg_empty_Soldier_AT_F";

        displayName = "Помощник гранатомётчика";
        weapons[] = {"CUP_arifle_AKM_Early", DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"CUP_arifle_AKM_Early", DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_4("SPEC_30Rnd_762x39_AK47_M"), DEFAULT_GRENADES};
        respawnMagazines[] = {mag_4("SPEC_30Rnd_762x39_AK47_M"),
                              DEFAULT_GRENADES};
        Items[] = {MED_ITEMS, REGULAR_ITEMS};
        RespawnItems[] = {MED_ITEMS, REGULAR_ITEMS};
        linkedItems[] = {"rhsgref_chicom_m88", "YuEBalaklavaW2o", LINKED_ITEMS};
        respawnLinkedItems[] = {"rhsgref_chicom_m88",
                                "YuEBalaklavaW2o",
                                LINKED_ITEMS};
    };

    class SpecLib_O_ChDKZ_medic_F : O_medic_F {
        UNIT_ENTRIES;
        PREVIEW(SpecLib_O_ChDKZ_medic_F);
        uniformClass = "T_CHDKZ_chedak1_uniform";
        backpack = "YuE_RD54_medic_F";

        displayName = "Санитар";
        weapons[] = {"CUP_arifle_AKMS_Early", DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"CUP_arifle_AKMS_Early", DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_9("SPEC_30Rnd_762x39_AK47_M"), DEFAULT_GRENADES};
        respawnMagazines[] = {mag_9("SPEC_30Rnd_762x39_AK47_M"),
                              DEFAULT_GRENADES};
        Items[] = {MED_ITEMS, REGULAR_ITEMS, SMERSH_ITEMS};
        RespawnItems[] = {MED_ITEMS, REGULAR_ITEMS, SMERSH_ITEMS};
        linkedItems[] = {"RPS_Smersh13",
                         "rhsgref_patrolcap_specter",
                         "rhsusf_shemagh_tan",
                         LINKED_ITEMS};
        respawnLinkedItems[] = {"RPS_Smersh13",
                                "rhsgref_patrolcap_specter",
                                "rhsusf_shemagh_tan",
                                LINKED_ITEMS};
    };

    class SpecLib_O_ChDKZ_engineer_F : O_engineer_F {
        UNIT_ENTRIES;
        PREVIEW(SpecLib_O_ChDKZ_engineer_F);
        uniformClass = "T_CHDKZ_chedak4_uniform";
        backpack = "YuE_RD54Full_engineer_F";

        displayName = "Инженер";
        weapons[] = {"CUP_arifle_AK74_Early", DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"CUP_arifle_AK74_Early", DEFAULT_UNIT_WEAPONS};
        magazines[] = {"SPEC_30Rnd_545x39_7N6M_AK_M"};
        respawnMagazines[] = {"SPEC_30Rnd_545x39_7N6M_AK_M"};
        Items[] = {MED_ITEMS, REGULAR_ITEMS};
        RespawnItems[] = {MED_ITEMS, REGULAR_ITEMS};
        linkedItems[] = {"YuEShapka1Camo", LINKED_ITEMS};
        respawnLinkedItems[] = {"YuEShapka1Camo", LINKED_ITEMS};
    };

    class SpecLib_O_ChDKZ_Soldier_01_F : O_Soldier_F {
        UNIT_ENTRIES;
        PREVIEW(SpecLib_O_ChDKZ_Soldier_01_F);
        uniformClass = "T_CHDKZ_chedak4_uniform";

        displayName = "Стрелок";
        weapons[] = {"CUP_arifle_AKM_Early", DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"CUP_arifle_AKM_Early", DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_4("SPEC_30Rnd_762x39_AK47_M"), DEFAULT_GRENADES};
        respawnMagazines[] = {mag_4("SPEC_30Rnd_762x39_AK47_M"),
                              DEFAULT_GRENADES};
        Items[] = {MED_ITEMS, REGULAR_ITEMS};
        RespawnItems[] = {MED_ITEMS, REGULAR_ITEMS};
        linkedItems[] = {"rhsgref_chicom_m88", "YuEShapka1Camo", LINKED_ITEMS};
        respawnLinkedItems[] = {"rhsgref_chicom_m88",
                                "YuEShapka1Camo",
                                LINKED_ITEMS};
    };

    class SpecLib_O_ChDKZ_Soldier_02_F : O_Soldier_F {
        UNIT_ENTRIES;
        PREVIEW(SpecLib_O_ChDKZ_Soldier_02_F);
        uniformClass = "T_CHDKZ_chedak4_uniform";

        displayName = "Стрелок (АК-74)";
        weapons[] = {"CUP_arifle_AK74_Early", DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"CUP_arifle_AK74_Early", DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_4("SPEC_30Rnd_545x39_7N6M_AK_M"), DEFAULT_GRENADES};
        respawnMagazines[] = {mag_4("SPEC_30Rnd_545x39_7N6M_AK_M"),
                              DEFAULT_GRENADES};
        Items[] = {MED_ITEMS, REGULAR_ITEMS};
        RespawnItems[] = {MED_ITEMS, REGULAR_ITEMS};
        linkedItems[] = {"rhsgref_chicom_m88", "YuEShapka1Camo", LINKED_ITEMS};
        respawnLinkedItems[] = {"rhsgref_chicom_m88",
                                "YuEShapka1Camo",
                                LINKED_ITEMS};
    };

    class SpecLib_O_ChDKZ_Soldier_GL_F : O_Soldier_GL_F {
        UNIT_ENTRIES;
        PREVIEW(SpecLib_O_ChDKZ_Soldier_GL_F);
        uniformClass = "T_CHDKZ_chedak6_uniform";

        displayName = "Стрелок ГП";
        weapons[] = {"CUP_arifle_AK74_GL_Early", DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"CUP_arifle_AK74_GL_Early", DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_9("SPEC_30Rnd_545x39_7N6M_AK_M"),
                       mag_11("rhs_VOG25"),
                       DEFAULT_GRENADES};
        respawnMagazines[] = {mag_9("SPEC_30Rnd_545x39_7N6M_AK_M"),
                              mag_11("rhs_VOG25"),
                              DEFAULT_GRENADES};
        Items[] = {MED_ITEMS, REGULAR_ITEMS, SMERSH_ITEMS};
        RespawnItems[] = {MED_ITEMS, REGULAR_ITEMS, SMERSH_ITEMS};
        linkedItems[] = {"RPS_Smersh2", "YuEBalaklavaW2o", LINKED_ITEMS};
        respawnLinkedItems[] = {"RPS_Smersh2", "YuEBalaklavaW2o", LINKED_ITEMS};
    };

    class SpecLib_O_ChDKZ_Soldier_LAT_F : O_Soldier_LAT_F {
        UNIT_ENTRIES;
        PREVIEW(SpecLib_O_ChDKZ_Soldier_LAT_F);
        uniformClass = "T_CHDKZ_chedak4_uniform";
        backpack = "";

        displayName = "Стрелок (РПГ-18)";
        weapons[] = {"CUP_arifle_AKM_Early",
                     "CUP_launch_RPG18",
                     DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"CUP_arifle_AKM_Early",
                            "CUP_launch_RPG18",
                            DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_4("SPEC_30Rnd_762x39_AK47_M"), DEFAULT_GRENADES};
        respawnMagazines[] = {mag_4("SPEC_30Rnd_762x39_AK47_M"),
                              DEFAULT_GRENADES};
        Items[] = {MED_ITEMS, REGULAR_ITEMS};
        RespawnItems[] = {MED_ITEMS, REGULAR_ITEMS};
        linkedItems[] = {"rhsgref_chicom_m88", "YuEShapka1Camo", LINKED_ITEMS};
        respawnLinkedItems[] = {"rhsgref_chicom_m88",
                                "YuEShapka1Camo",
                                LINKED_ITEMS};
    };

    class SpecLib_O_ChDKZ_Soldier_AA_F : O_Soldier_LAT_F {
        UNIT_ENTRIES;
        PREVIEW(SpecLib_O_ChDKZ_Soldier_AA_F);
        uniformClass = "T_CHDKZ_chedak2_uniform";
        backpack = "";

        displayName = "Оператор ПЗРК";
        weapons[] = {"CUP_arifle_AKMS_Early",
                     "rhs_weap_igla",
                     DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"CUP_arifle_AKMS_Early",
                            "rhs_weap_igla",
                            DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_4("SPEC_30Rnd_762x39_AK47_M"),
                       "rhs_mag_9k38_rocket",
                       DEFAULT_GRENADES};
        respawnMagazines[] = {mag_4("SPEC_30Rnd_762x39_AK47_M"),
                              "rhs_mag_9k38_rocket",
                              DEFAULT_GRENADES};
        Items[] = {MED_ITEMS, REGULAR_ITEMS};
        RespawnItems[] = {MED_ITEMS, REGULAR_ITEMS};
        linkedItems[] = {"rhsgref_chicom_m88", "YuEShapka1Camo", LINKED_ITEMS};
        respawnLinkedItems[] = {"rhsgref_chicom_m88",
                                "YuEShapka1Camo",
                                LINKED_ITEMS};
    };

    class SpecLib_O_ChDKZ_Soldier_M_F : O_Soldier_M_F {
        UNIT_ENTRIES;
        PREVIEW(SpecLib_O_ChDKZ_Soldier_M_F);
        uniformClass = "T_CHDKZ_chedak6_uniform";

        displayName = "Снайпер";
        weapons[] = {"VTN_SVD_1963_PSO1", DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"VTN_SVD_1963_PSO1", DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_9("VTN_SVD_10s_SC"), DEFAULT_GRENADES};
        respawnMagazines[] = {mag_9("VTN_SVD_10s_SC"), DEFAULT_GRENADES};
        Items[] = {MED_ITEMS, REGULAR_ITEMS, SMERSH_ITEMS};
        RespawnItems[] = {MED_ITEMS, REGULAR_ITEMS, SMERSH_ITEMS};
        linkedItems[] = {"RPS_Smersh15", "YuEBalaklavaW2o", LINKED_ITEMS};
        respawnLinkedItems[] = {"RPS_Smersh15",
                                "YuEBalaklavaW2o",
                                LINKED_ITEMS};
    };

    class SpecLib_O_ChDKZ_soldier_exp_F : O_soldier_exp_F {
        UNIT_ENTRIES;
        PREVIEW(SpecLib_O_ChDKZ_soldier_exp_F);
        uniformClass = "T_CHDKZ_chedak6_uniform";
        backpack = "YuE_RD54_soldier_exp_F";

        displayName = "Диверсант";
        weapons[] = {"CUP_arifle_AKS74U_PBS4", "VTN_B8", DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"CUP_arifle_AKS74U_PBS4",
                            "VTN_B8",
                            DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_9("SPEC_30Rnd_545x39_7N6M_AK_M"), DEFAULT_GRENADES};
        respawnMagazines[] = {mag_9("SPEC_30Rnd_545x39_7N6M_AK_M"),
                              DEFAULT_GRENADES};
        Items[] = {MED_ITEMS, REGULAR_ITEMS, SMERSH_ITEMS};
        RespawnItems[] = {MED_ITEMS, REGULAR_ITEMS, SMERSH_ITEMS};
        linkedItems[] = {"RPS_Smersh13", "YuEBalaklavaW2o", LINKED_ITEMS_FULL};
        respawnLinkedItems[] = {"RPS_Smersh13",
                                "YuEBalaklavaW2o",
                                LINKED_ITEMS_FULL};
    };

    class SpecLib_O_ChDKZ_soldier_mine_F : O_soldier_mine_F {
        UNIT_ENTRIES;
        PREVIEW(SpecLib_O_ChDKZ_soldier_mine_F);
        uniformClass = "T_CHDKZ_chedak4_uniform";
        backpack = "YuE_RD54Full_soldier_mine_F";

        displayName = "Сапёр";
        weapons[] = {"CUP_arifle_AKMS_Early", DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"CUP_arifle_AKMS_Early", DEFAULT_UNIT_WEAPONS};
        magazines[] = {"SPEC_30Rnd_762x39_AK47_M"};
        respawnMagazines[] = {"SPEC_30Rnd_762x39_AK47_M"};
        Items[] = {MED_ITEMS, REGULAR_ITEMS};
        RespawnItems[] = {MED_ITEMS, REGULAR_ITEMS};
        linkedItems[] = {"YuEShapka1Camo", LINKED_ITEMS};
        respawnLinkedItems[] = {"YuEShapka1Camo", LINKED_ITEMS};
    };

    class SpecLib_O_ChDKZ_Pilot_F : O_Pilot_F {
        UNIT_ENTRIES;
        PREVIEW(SpecLib_O_ChDKZ_Pilot_F);
        uniformClass = "T_CHDKZ_chedak1_uniform";
        backpack = "";

        displayName = "Пилот";
        weapons[] = {"CUP_arifle_AKS74U", DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"CUP_arifle_AKS74U", DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_4("SPEC_30Rnd_545x39_7N6M_AK_M")};
        respawnMagazines[] = {mag_4("SPEC_30Rnd_545x39_7N6M_AK_M")};
        Items[] = {MED_ITEMS};
        RespawnItems[] = {MED_ITEMS};
        linkedItems[] = {"rhs_6sh46", "rhs_gssh18", LINKED_ITEMS_FULL};
        respawnLinkedItems[] = {"rhs_6sh46", "rhs_gssh18", LINKED_ITEMS_FULL};
    };
};
