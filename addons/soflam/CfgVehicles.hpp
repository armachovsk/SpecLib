class CfgVehicles {
    class Item_Base_F;
    class Item_UK3CB_BAF_Soflam_Laserdesignator : Item_Base_F {
        scope = 2;
        scopeCurator = 2;
        author = "www.3commandobrigade.com";
        DLC = "speclib";
        displayName = "Soflam Laser Designator [BAF]";
        vehicleClass = "Items";
        model = "\z\speclib\addons\soflam\models\Soflam.p3d";
        class TransportItems {
            class UK3CB_BAF_Soflam_Laserdesignator {
                name = "UK3CB_BAF_Soflam_Laserdesignator";
                count = 1;
            };
        };
    };
};
