﻿#include "\a3\editor_f\Data\Scripts\dikCodes.h"
#include "script_component.hpp"

private ["_currentVeh","_veh"];

if (isDedicated) exitWith {};

if (!canSuspend) exitWith {
	[] spawn FUNC(lowGearInit);
};

waitUntil {
	uiSleep 0.25;
	time > 0;
};

RIPLOWGEARON = false;
RIP_LOWGEARACTON = nil;
RIP_LOWGEARACTOFF = nil;
_currentVeh = vehicle player;

[
	"speclib",
	"toggle_lowgear",
	"Переключить пониженную передачу",
	{
		if (!isNil "RIP_LOWGEARACTON") exitWith {
			[] spawn FUNC(lowGearOn);
		};
		if (!isNil "RIP_LOWGEARACTOFF") exitWith {
			[] spawn FUNC(lowGearOff);
		};
	},
	"",
	[DIK_C, [true, false, false]]
] call CBA_fnc_addKeybind;

while {true} do {
	uiSleep 0.25;
	_veh = vehicle player;
	if ((player == (driver _veh)) && (canMove _veh)) then {
		if (_veh isKindOf "Tank" || _veh isKindOf "BTR90_Base" || _veh isKindOf "car") then {
			if (!RIPLOWGEARON && (isNil "RIP_LOWGEARACTON")) then {
				RIP_LOWGEARACTON = _veh addAction [
					'<t color="#baa71c">Включить пониженную передачу</t>',
					FUNC(lowGearOn),
					"",
					30,
					false,
					false,
					"",
					"driver  _target == _this"
				];
				_currentVeh = _veh;
			};
		};
	};
	if ((player != (driver _currentVeh)) && !(isNil "RIP_LOWGEARACTON")) then {
		_currentVeh removeAction RIP_LOWGEARACTON;
		RIP_LOWGEARACTON = nil;
	};
};
