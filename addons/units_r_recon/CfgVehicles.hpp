class CfgVehicles {
    class YuE_Ataka2Green;
    class O_R_recon_TL_F;
    class O_R_recon_AR_F;
    class O_R_recon_medic_F;
    class O_R_recon_M_F;
    class O_R_recon_exp_F;
    class O_R_recon_LAT_F;
    class O_R_Soldier_recon_base;

    class YuE_Ataka2Green_01_F : YuE_Ataka2Green {
        scope = 1;
        class TransportWeapons {
            weap_xx(rhs_weap_rsp30_white, 1);
            weap_xx(rhs_weap_rsp30_green, 1);
            weap_xx(rhs_weap_rsp30_red, 1);
        };
        class TransportMagazines {
            mag_xx(SPEC_30Rnd_545x39_7N22_AK74M_camo_M, 4);
            mag_xx(rhs_mag_nspd, 1);
            mag_xx(rhs_mag_rdg2_white, 2);
            mag_xx(rhs_mag_f1, 2);
        };
        class TransportItems {
            item_xx(ACE_Canteen, 1);
            item_xx(VTN_1PN74, 1);
            item_xx(ACE_Flashlight_KSF1, 1);
        };
    };

    class YuE_Ataka2Green_02_F : YuE_Ataka2Green {
        scope = 1;
        class TransportWeapons {
            weap_xx(rhs_weap_rsp30_white, 1);
            weap_xx(rhs_weap_rsp30_green, 1);
            weap_xx(rhs_weap_rsp30_red, 1);
        };
        class TransportMagazines {
            mag_xx(gs_20rC_9x39mm_BP, 4);
            mag_xx(rhs_mag_nspd, 1);
            mag_xx(rhs_mag_rdg2_white, 2);
            mag_xx(rhs_mag_f1, 2);
        };
        class TransportItems {
            item_xx(ACE_Canteen, 1);
            item_xx(VTN_1PN74, 1);
            item_xx(ACE_Flashlight_KSF1, 1);
        };
    };

    class YuE_Ataka2Green_03_F : YuE_Ataka2Green {
        scope = 1;
        class TransportWeapons {
            weap_xx(rhs_weap_rsp30_white, 1);
            weap_xx(rhs_weap_rsp30_green, 1);
            weap_xx(rhs_weap_rsp30_red, 1);
        };
        class TransportMagazines {
            mag_xx(rhs_mag_nspd, 1);
            mag_xx(rhs_100Rnd_762x54mmR_7BZ3, 2);
            mag_xx(rhs_mag_rdg2_white, 2);
        };
        class TransportItems {
            item_xx(ACE_Canteen, 1);
            item_xx(VTN_1PN74, 1);
            item_xx(ACE_Flashlight_KSF1, 1);
        };
    };

    class YuE_Ataka2Green_04_F : YuE_Ataka2Green {
        scope = 1;
        class TransportWeapons {
            weap_xx(rhs_weap_rsp30_white, 1);
            weap_xx(rhs_weap_rsp30_green, 1);
            weap_xx(rhs_weap_rsp30_red, 1);
        };
        class TransportMagazines {
            mag_xx(rhs_mag_nspd, 1);
            mag_xx(rhs_mag_rdg2_white, 4);
        };
        class TransportItems {
            item_xx(VTN_1PN74, 1);
            item_xx(ACE_Canteen, 1);
            item_xx(ACE_Flashlight_KSF1, 1);
            item_xx(ACE_fieldDressing, 14);
            item_xx(ACE_elasticBandage, 14);
            item_xx(ACE_quikclot, 14);
            item_xx(ACE_packingBandage, 14);
            item_xx(ACE_tourniquet, 4);
            item_xx(ACE_morphine, 6);
            item_xx(ACE_epinephrine, 6);
            item_xx(ACE_adenosine, 6);
            item_xx(ACE_salineIV, 2);
            item_xx(ACE_salineIV_500, 4);
            item_xx(ACE_surgicalKit, 1);
            item_xx(ACE_splint, 6);
        };
    };

    class YuE_Ataka2Green_05_F : YuE_Ataka2Green {
        scope = 1;
        class TransportWeapons {
            weap_xx(rhs_weap_rsp30_white, 1);
            weap_xx(rhs_weap_rsp30_green, 1);
            weap_xx(rhs_weap_rsp30_red, 1);
        };
        class TransportMagazines {
            mag_xx(rhs_ec200_mag, 2);
            mag_xx(rhs_mine_ozm72_a_mag, 2);           
        };
        class TransportItems {
            item_xx(VTN_1PN74, 1);
            item_xx(ACE_Canteen, 1);
            item_xx(ACE_Flashlight_KSF1, 1);
            item_xx(ACE_DefusalKit, 1);
            item_xx(ACE_Clacker, 1);
            item_xx(MineDetector, 1);
        };
    };

    class YuE_Ataka2Green_06_F : YuE_Ataka2Green {
        scope = 1;
        class TransportWeapons {
            weap_xx(rhs_weap_rsp30_white, 1);
            weap_xx(rhs_weap_rsp30_green, 1);
            weap_xx(rhs_weap_rsp30_red, 1);
        };
        class TransportMagazines {
            mag_xx(rhs_mag_nspd, 1);
            mag_xx(rhs_mag_rdg2_white, 2);
            mag_xx(rhs_mag_f1, 2);
            mag_xx(VTN_SVDC_10s_AP, 8);
        };
        class TransportItems {
            item_xx(VTN_1PN74, 1);
            item_xx(ACE_Canteen, 1);
            item_xx(ACE_Flashlight_KSF1, 1);
        };
    };

    class YuE_Ataka2Green_07_F : YuE_Ataka2Green {
        scope = 1;
        class TransportWeapons {
            weap_xx(rhs_weap_rsp30_white, 1);
            weap_xx(rhs_weap_rsp30_green, 1);
            weap_xx(rhs_weap_rsp30_red, 1);
        };
        class TransportMagazines {
            mag_xx(rhs_mag_nspd, 1);
            mag_xx(rhs_mag_rdg2_white, 2);
            mag_xx(rhs_mag_f1, 2);
            mag_xx(gs_20rC_9x39mm_BP, 4);
        };
        class TransportItems {
            item_xx(VTN_1PN74, 1);
            item_xx(ACE_Canteen, 1);
            item_xx(ACE_Flashlight_KSF1, 1);
        };
    };

    class SpecLib_O_R_recon_TL_01_F : O_R_recon_TL_F {
        UNIT_ENTRIES;
        PREVIEW(SpecLib_O_R_recon_TL_01_F);

        uniformClass = "Spec_p_berezka2_EAST_Uniform";

        backpack = "YuE_Ataka2Green_01_F";

        displayName = "Командир отделения";
        weapons[] = {"CUP_arifle_AK74M_GL_camo", 
                    "CUP_hgun_PB6P9_muzzle", 
                    DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"CUP_arifle_AK74M_GL_camo",
                            "CUP_hgun_PB6P9_muzzle",
                            "VTN_LPR2",
                            DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_9("SPEC_30Rnd_545x39_7N22_AK74M_camo_M"),
                       mag_3("rhs_mag_9x18_8_57N181S"),
                       GRENADES,
                       GRENADE_LAUNCHER_ROUNDS};
        respawnMagazines[] = {mag_9("SPEC_30Rnd_545x39_7N22_AK74M_camo_M"),
                              mag_3("rhs_mag_9x18_8_57N181S"),
                              GRENADES,
                              GRENADE_LAUNCHER_ROUNDS};
        Items[] = {ITEMS};
        RespawnItems[] = {ITEMS};
        linkedItems[] = {"RPS_Smersh8",
                         "YuEPanama2bb2_GBSHP2",
                         "YuEBandanaO_n",
                         LINKED_ITEMS_TL};
        respawnLinkedItems[] = {"RPS_Smersh8",
                                "YuEPanama2bb2_GBSHP2",
                                "YuEBandanaO_n",
                                LINKED_ITEMS_TL};
    };
    class SpecLib_O_R_recon_TL_02_F : O_R_recon_TL_F {
        UNIT_ENTRIES;
        PREVIEW(SpecLib_O_R_recon_TL_02_F);

        uniformClass = "Spec_p_klp_frog_EAST_Uniform";

        backpack = "YuE_Ataka2Green_02_F";

        displayName = "Командир отделения ПБС";
        weapons[] = {"gs_asval_C",
                     "CUP_hgun_PB6P9_muzzle",
                     "VTN_LPR2",
                     DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"gs_asval_C",
                            "CUP_hgun_PB6P9_muzzle",
                            "VTN_LPR2",
                            DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_9("gs_20rC_9x39mm_BP"),
                       mag_3("rhs_mag_9x18_8_57N181S"),
                       GRENADES};
        respawnMagazines[] = {mag_9("gs_20rC_9x39mm_BP"),
                              mag_3("rhs_mag_9x18_8_57N181S"),
                              GRENADES};
        Items[] = {ITEMS};
        RespawnItems[] = {ITEMS};
        linkedItems[] = {"RPS_Smersh12",
                         "YuEBandanaO_n_GBSHP2",
                         LINKED_ITEMS_TL};
        respawnLinkedItems[] = {"RPS_Smersh12",
                                "YuEBandanaO_n_GBSHP2",
                                LINKED_ITEMS_TL};
    };
    class SpecLib_O_R_recon_MachineGunner_F : O_R_recon_AR_F {
        UNIT_ENTRIES;
        PREVIEW(SpecLib_O_R_recon_MachineGunner_F);

        role = "MachineGunner";
        icon = "iconManMG";

        uniformClass = "Spec_p_frog_EAST_Uniform";

        backpack = "YuE_Ataka2Green_03_F";

        displayName = "Пулемётчик";
        weapons[] = {"VTN_PKM", "CUP_hgun_PB6P9_muzzle", DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"VTN_PKM", "CUP_hgun_PB6P9_muzzle", DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_3("rhs_100Rnd_762x54mmR_7BZ3"),
                       mag_3("rhs_mag_9x18_8_57N181S"),
                       GRENADES};
        respawnMagazines[] = {mag_3("rhs_100Rnd_762x54mmR_7BZ3"),
                              mag_3("rhs_mag_9x18_8_57N181S"),
                              GRENADES};
        Items[] = {ITEMS};
        RespawnItems[] = {ITEMS};
        linkedItems[] = {"RPS_Smersh6",
                         "YuEPanama1bfr_GBSHP2",
                         "YuEBandanaO_n",
                         LINKED_ITEMS};
        respawnLinkedItems[] = {"RPS_Smersh6",
                                "YuEPanama1bfr_GBSHP2",
                                "YuEBandanaO_n",
                                LINKED_ITEMS};
    };
    class SpecLib_O_R_recon_medic_01_F : O_R_recon_medic_F {
        UNIT_ENTRIES;
        PREVIEW(SpecLib_O_R_recon_medic_01_F);

        backpack = "YuE_Ataka2Green_04_F";

        uniformClass = "Spec_p_frog_EAST_Uniform";

        displayName = "Санитар";
        weapons[] = {"CUP_arifle_AK74M_camo", "CUP_hgun_PB6P9_muzzle", DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"CUP_arifle_AK74M_camo",
                            "CUP_hgun_PB6P9_muzzle",
                            DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_13("SPEC_30Rnd_545x39_7N22_AK74M_camo_M"),
                       mag_3("rhs_mag_9x18_8_57N181S"),
                       GRENADES};
        respawnMagazines[] = {mag_13("SPEC_30Rnd_545x39_7N22_AK74M_camo_M"),
                              mag_3("rhs_mag_9x18_8_57N181S"),
                              GRENADES};
        Items[] = {ITEMS};
        RespawnItems[] = {ITEMS};
        linkedItems[] = {"RPS_Smersh12",
                         "YuEPanama2bfr_GBSHP2",
                         "YuEBandanaO_n",
                         LINKED_ITEMS};
        respawnLinkedItems[] = {"RPS_Smersh12",
                                "YuEPanama2bfr_GBSHP2",
                                "YuEBandanaO_n",
                                LINKED_ITEMS};
    };
    class SpecLib_O_R_recon_medic_02_F : O_R_recon_medic_F {
        UNIT_ENTRIES;
        PREVIEW(SpecLib_O_R_recon_medic_02_F);

        backpack = "YuE_Ataka2Green_04_F";

        uniformClass = "Spec_p_klp_frog_EAST_Uniform";

        displayName = "Санитар ПБС";
        weapons[] = {"gs_asval_C", "CUP_hgun_PB6P9_muzzle", DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"gs_asval_C",
                            "CUP_hgun_PB6P9_muzzle",
                            DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_9("gs_20rC_9x39mm_BP"),
                       mag_3("rhs_mag_9x18_8_57N181S"),
                       GRENADES};
        respawnMagazines[] = {mag_9("gs_20rC_9x39mm_BP"),
                              mag_3("rhs_mag_9x18_8_57N181S"),
                              GRENADES};
        Items[] = {ITEMS};
        RespawnItems[] = {ITEMS};
        linkedItems[] = {"RPS_Smersh12",
                         "YuEBandanaO_n_GBSHP2",
                         LINKED_ITEMS};
        respawnLinkedItems[] = {"RPS_Smersh12",
                                "YuEBandanaO_n_GBSHP2",
                                LINKED_ITEMS};
    };
    class SpecLib_O_R_recon_exp_01_F : O_R_recon_exp_F {
        UNIT_ENTRIES;
        PREVIEW(SpecLib_O_R_recon_exp_01_F);

        backpack = "YuE_Ataka2Green_05_F";

        uniformClass = "Spec_p_berezka2_EAST_Uniform";

        displayName = "Подрывник";
        weapons[] = {"CUP_arifle_AK74M_camo", "CUP_hgun_PB6P9_muzzle", DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"CUP_arifle_AK74M_camo",
                            "CUP_hgun_PB6P9_muzzle",
                            DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_13("SPEC_30Rnd_545x39_7N22_AK74M_camo_M"),
                       mag_3("rhs_mag_9x18_8_57N181S"),
                       GRENADES};
        respawnMagazines[] = {mag_13("SPEC_30Rnd_545x39_7N22_AK74M_camo_M"),
                              mag_3("rhs_mag_9x18_8_57N181S"),
                              GRENADES};
        Items[] = {ITEMS};
        RespawnItems[] = {ITEMS};
        linkedItems[] = {"RPS_Smersh12",
                         "YuEBandana_O_GBSHP2",
                         LINKED_ITEMS};
        respawnLinkedItems[] = {"RPS_Smersh12",
                                "YuEBandana_O_GBSHP2",
                                LINKED_ITEMS};
    };
    class SpecLib_O_R_recon_exp_02_F : O_R_recon_exp_F {
        UNIT_ENTRIES;
        PREVIEW(SpecLib_O_R_recon_exp_02_F);

        backpack = "YuE_Ataka2Green_05_F";

        uniformClass = "Spec_p_klp_frog_EAST_Uniform";

        displayName = "Подрывник ПБС";
        weapons[] = {"gs_asval_C", "CUP_hgun_PB6P9_muzzle", DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"gs_asval_C",
                            "CUP_hgun_PB6P9_muzzle",
                            DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_9("gs_20rC_9x39mm_BP"),
                       mag_3("rhs_mag_9x18_8_57N181S"),
                       GRENADES};
        respawnMagazines[] = {mag_9("gs_20rC_9x39mm_BP"),
                              mag_3("rhs_mag_9x18_8_57N181S"),
                              GRENADES};
        Items[] = {ITEMS};
        RespawnItems[] = {ITEMS};
        linkedItems[] = {"RPS_Smersh12",
                         "YuEBandanaO_n_GBSHP2",
                         LINKED_ITEMS};
        respawnLinkedItems[] = {"RPS_Smersh12",
                                "YuEBandanaO_n_GBSHP2",
                                LINKED_ITEMS};
    };
    class SpecLib_O_R_recon_M_01_F : O_R_recon_M_F {
        UNIT_ENTRIES;
        PREVIEW(SpecLib_O_R_recon_M_01_F);

        uniformClass = "Spec_p_frog_EAST_Uniform";

        backpack = "YuE_Ataka2Green_06_F";

        displayName = "Снайпер";
        weapons[] = {"VTN_SVD_CAMO_PART_PSO1M2",
                     "CUP_hgun_PB6P9_muzzle",
                     "VTN_LPR2",
                     DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"VTN_SVD_CAMO_PART_PSO1M2",
                            "CUP_hgun_PB6P9_muzzle",
                            "VTN_LPR2",
                            DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_9("VTN_SVDC_10s_AP"),
                       mag_3("rhs_mag_9x18_8_57N181S"),
                       GRENADES};
        respawnMagazines[] = {mag_9("VTN_SVDC_10s_AP"),
                              mag_3("rhs_mag_9x18_8_57N181S"),
                              GRENADES};
        Items[] = {ITEMS};
        RespawnItems[] = {ITEMS};
        linkedItems[] = {"RPS_Smersh17",
                         "YuEPanama2bfr_GBSHP2",
                         "YuEBandanaO_n",
                         LINKED_ITEMS};
        respawnLinkedItems[] = {"RPS_Smersh17",
                                "YuEPanama2bfr_GBSHP2",
                                "YuEBandanaO_n",
                                LINKED_ITEMS};
    };
    class SpecLib_O_R_recon_M_02_F : O_R_recon_M_F {
        UNIT_ENTRIES;
        PREVIEW(SpecLib_O_R_recon_M_02_F);

        uniformClass = "Spec_p_klp_berezka2_EAST_Uniform";

        backpack = "YuE_Ataka2Green_07_F";

        displayName = "Снайпер ПБС";
        weapons[] = {"gs_vss_camo_pso", 
                     "CUP_hgun_PB6P9_muzzle", 
                     "VTN_LPR2",    
                     DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"gs_vss_camo_pso",
                            "CUP_hgun_PB6P9_muzzle",
                            "VTN_LPR2",
                            DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_9("gs_10rC_9x39mm_BP"),
                       mag_3("rhs_mag_9x18_8_57N181S"),
                       GRENADES};
        respawnMagazines[] = {mag_9("gs_10rC_9x39mm_BP"),
                              mag_3("rhs_mag_9x18_8_57N181S"),
                              GRENADES};
        Items[] = {ITEMS};
        RespawnItems[] = {ITEMS};
        linkedItems[] = {"RPS_Smersh17",
                         "YuEBandanaO_n_GBSHP2",
                         LINKED_ITEMS};
        respawnLinkedItems[] = {"RPS_Smersh17",
                                "YuEBandanaO_n_GBSHP2",
                                LINKED_ITEMS};
    };
    class SpecLib_O_R_recon_01_F : O_R_Soldier_recon_base {
        UNIT_ENTRIES;
        PREVIEW(SpecLib_O_R_recon_01_F);

        uniformClass = "Spec_p_berezka2_EAST_Uniform";

        backpack = "YuE_Ataka2Green_01_F";

        displayName = "Стрелок";
        weapons[] = {"CUP_arifle_AK74M_camo", "CUP_hgun_PB6P9_muzzle", DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"CUP_arifle_AK74M_camo",
                            "CUP_hgun_PB6P9_muzzle",
                            DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_13("SPEC_30Rnd_545x39_7N22_AK74M_camo_M"),
                       mag_3("rhs_mag_9x18_8_57N181S"),
                       GRENADES};
        respawnMagazines[] = {mag_13("SPEC_30Rnd_545x39_7N22_AK74M_camo_M"),
                              mag_3("rhs_mag_9x18_8_57N181S"),
                              GRENADES};
        Items[] = {ITEMS};
        RespawnItems[] = {ITEMS};
        linkedItems[] = {"RPS_Smersh12",
                         "YuEPanama1bb2_GBSHP2",
                         "YuEBandanaO_n",
                         LINKED_ITEMS};
        respawnLinkedItems[] = {"RPS_Smersh12",
                                "YuEPanama1bb2_GBSHP2",
                                "YuEBandanaO_n",
                                LINKED_ITEMS};
    };
    class SpecLib_O_R_recon_02_F : SpecLib_O_R_recon_01_F {
        PREVIEW(SpecLib_O_R_recon_02_F);
        backpack = "YuE_Ataka2Green_02_F";

        uniformClass = "Spec_p_klp_st_berezka2_EAST_Uniform";

        displayName = "Стрелок ПБС";
        weapons[] = {"gs_asval_C", "CUP_hgun_PB6P9_muzzle", DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"gs_asval_C",
                            "CUP_hgun_PB6P9_muzzle",
                            DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_9("gs_20rC_9x39mm_BP"),
                       mag_3("rhs_mag_9x18_8_57N181S"),
                       GRENADES};
        respawnMagazines[] = {mag_9("gs_20rC_9x39mm_BP"),
                              mag_3("rhs_mag_9x18_8_57N181S"),
                              GRENADES};
        linkedItems[] = {"RPS_Smersh12",
                         "YuEBandanaO_n_GBSHP2",
                         LINKED_ITEMS};
        respawnLinkedItems[] = {"RPS_Smersh12",
                                "YuEBandanaO_n_GBSHP2",
                                LINKED_ITEMS};
    };
    class SpecLib_O_R_recon_GL_01_F : SpecLib_O_R_recon_01_F {
        PREVIEW(SpecLib_O_R_recon_GL_01_F);
        displayName = "Стрелок ГП";
        weapons[] = {"CUP_arifle_AK74M_GL_camo", "CUP_hgun_PB6P9_muzzle", DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"CUP_arifle_AK74M_GL_camo",
                            "CUP_hgun_PB6P9_muzzle",
                            DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_13("SPEC_30Rnd_545x39_7N22_AK74M_camo_M"),
                       mag_3("rhs_mag_9x18_8_57N181S"),
                       GRENADES,
                       GRENADE_LAUNCHER_ROUNDS};
        respawnMagazines[] = {mag_13("SPEC_30Rnd_545x39_7N22_AK74M_camo_M"),
                              mag_3("rhs_mag_9x18_8_57N181S"),
                              GRENADES,
                              GRENADE_LAUNCHER_ROUNDS};
        linkedItems[] = {"RPS_Smersh8",
                         "YuEBandana_O_GBSHP2",
                         LINKED_ITEMS};
        respawnLinkedItems[] = {"RPS_Smersh8",
                                "YuEBandana_O_GBSHP2",
                                LINKED_ITEMS};
    };
    class SpecLib_O_R_recon_GL_02_F : SpecLib_O_R_recon_GL_01_F {
        PREVIEW(SpecLib_O_R_recon_GL_02_F);

        uniformClass = "Spec_p_klp_st_frog_EAST_Uniform";

        displayName = "Стрелок ГП ПБС";
        weapons[] = {"CUP_arifle_AK74M_GL_camo_tgpa",
                     "CUP_hgun_PB6P9_muzzle",
                     DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"CUP_arifle_AK74M_GL_camo_tgpa",
                            "CUP_hgun_PB6P9_muzzle",
                            DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_13("SPEC_30Rnd_545x39_7N22_AK74M_camo_M"),
                       mag_3("rhs_mag_9x18_8_57N181S"),
                       GRENADES,
                       GRENADE_LAUNCHER_ROUNDS};
        respawnMagazines[] = {mag_13("SPEC_30Rnd_545x39_7N22_AK74M_camo_M"),
                              mag_3("rhs_mag_9x18_8_57N181S"),
                              GRENADES,
                              GRENADE_LAUNCHER_ROUNDS};
        linkedItems[] = {"RPS_Smersh8",
                         "YuEBandanaO_n_GBSHP2",
                         LINKED_ITEMS};
        respawnLinkedItems[] = {"RPS_Smersh8",
                                "YuEBandanaO_n_GBSHP2",
                                LINKED_ITEMS};
    };
    class SpecLib_O_R_recon_AT_01_F : SpecLib_O_R_recon_01_F {
        PREVIEW(SpecLib_O_R_recon_AT_01_F);

       uniformClass = "Spec_p_frog_EAST_Uniform";
        icon = "iconManAT";
        role = "MissileSpecialist";

        displayName = "Стрелок (РПГ-26)";
        weapons[] = {"CUP_arifle_AK74M_camo",
                     "rhs_weap_rpg26",
                     "CUP_hgun_PB6P9_muzzle",
                     DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"CUP_arifle_AK74M_camo",
                            "rhs_weap_rpg26",
                            "CUP_hgun_PB6P9_muzzle",
                            DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_12("SPEC_30Rnd_545x39_7N22_AK74M_camo_M"),
                       "rhs_rpg26_mag",
                       mag_3("rhs_mag_9x18_8_57N181S"),
                       GRENADES};
        respawnMagazines[] = {mag_12("SPEC_30Rnd_545x39_7N22_AK74M_camo_M"),
                              "rhs_rpg26_mag",
                              mag_3("rhs_mag_9x18_8_57N181S"),
                              GRENADES};
        linkedItems[] = {"RPS_Smersh12",
                         "YuEBandana_O_GBSHP2",
                         LINKED_ITEMS};
        respawnLinkedItems[] = {"RPS_Smersh12",
                                "YuEBandana_O_GBSHP2",
                                LINKED_ITEMS};
    };
    class SpecLib_O_R_recon_AT_02_F : SpecLib_O_R_recon_02_F {
        PREVIEW(SpecLib_O_R_recon_AT_02_F);

       uniformClass = "Spec_p_klp_berezka2_EAST_Uniform";
        icon = "iconManAT";
        role = "MissileSpecialist";

        displayName = "Стрелок (РПГ-26) ПБС";
        weapons[] = {"gs_asval_C",
                     "rhs_weap_rpg26",
                     "CUP_hgun_PB6P9_muzzle",
                     DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"gs_asval_C",
                            "rhs_weap_rpg26",
                            "CUP_hgun_PB6P9_muzzle",
                            DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_9("gs_20rC_9x39mm_BP"),
                       "rhs_rpg26_mag",
                       mag_3("rhs_mag_9x18_8_57N181S"),
                       GRENADES};
        respawnMagazines[] = {mag_9("gs_20rC_9x39mm_BP"),
                              "rhs_rpg26_mag",
                              mag_3("rhs_mag_9x18_8_57N181S"),
                              GRENADES};
    };
    class SpecLib_O_R_recon_AT_03_F : SpecLib_O_R_recon_01_F {
        PREVIEW(SpecLib_O_R_recon_AT_03_F);
        icon = "iconManAT";
        role = "MissileSpecialist";

        displayName = "Стрелок (РШГ-2)";
        weapons[] = {"CUP_arifle_AK74M_camo",
                     "rhs_weap_rshg2",
                     "CUP_hgun_PB6P9_muzzle",
                     DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"CUP_arifle_AK74M_camo",
                            "rhs_weap_rshg2",
                            "CUP_hgun_PB6P9_muzzle",
                            DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_12("SPEC_30Rnd_545x39_7N22_AK74M_camo_M"),
                       "rhs_rshg2_mag",
                       mag_3("rhs_mag_9x18_8_57N181S"),
                       GRENADES};
        respawnMagazines[] = {mag_12("SPEC_30Rnd_545x39_7N22_AK74M_camo_M"),
                              "rhs_rshg2_mag",
                              mag_3("rhs_mag_9x18_8_57N181S"),
                              GRENADES};
    };
    class SpecLib_O_R_recon_AT_04_F : SpecLib_O_R_recon_02_F {
        PREVIEW(SpecLib_O_R_recon_AT_04_F);
        
       uniformClass = "Spec_p_klp_st_frog_EAST_Uniform";
        icon = "iconManAT";
        role = "MissileSpecialist";

        displayName = "Стрелок (РШГ-2) ПБС";
        weapons[] = {"gs_asval_C",
                     "rhs_weap_rshg2",
                     "CUP_hgun_PB6P9_muzzle",
                     DEFAULT_UNIT_WEAPONS};
        respawnWeapons[] = {"gs_asval_C",
                            "rhs_weap_rshg2",
                            "CUP_hgun_PB6P9_muzzle",
                            DEFAULT_UNIT_WEAPONS};
        magazines[] = {mag_9("gs_20rC_9x39mm_BP"),
                       "rhs_rshg2_mag",
                       mag_3("rhs_mag_9x18_8_57N181S"),
                       GRENADES};
        respawnMagazines[] = {mag_9("gs_20rC_9x39mm_BP"),
                              "rhs_rshg2_mag",
                              mag_3("rhs_mag_9x18_8_57N181S"),
                              GRENADES};
    };
};
