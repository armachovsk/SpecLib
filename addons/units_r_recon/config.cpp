#include "script_component.hpp"
class CfgPatches {
    class ADDON {
        name = COMPONENT;
        units[] = {
            "SpecLib_O_R_recon_TL_01_F",
            "SpecLib_O_R_recon_TL_02_F",
            "SpecLib_O_R_recon_MachineGunner_F",
            "SpecLib_O_R_recon_medic_01_F",
            "SpecLib_O_R_recon_medic_02_F",
            "SpecLib_O_R_recon_exp_01_F",
            "SpecLib_O_R_recon_exp_02_F",
            "SpecLib_O_R_recon_M_01_F",
            "SpecLib_O_R_recon_M_02_F",
            "SpecLib_O_R_recon_01_F",
            "SpecLib_O_R_recon_02_F",
            "SpecLib_O_R_recon_GL_01_F",
            "SpecLib_O_R_recon_GL_02_F",
            "SpecLib_O_R_recon_AT_01_F",
            "SpecLib_O_R_recon_AT_02_F",
            "SpecLib_O_R_recon_AT_03_F",
            "SpecLib_O_R_recon_AT_04_F"
        };
        weapons[] = {};
        requiredVersion = REQUIRED_VERSION;
        requiredAddons[] = {"cba_main", "ace_main"};
        author = "Reidond";
        VERSION_CONFIG;
    };
};

class CBA_Extended_EventHandlers;

#include "CfgEventHandlers.hpp"
#include "CfgEditorSubcategories.hpp"
#include "CfgVehicles.hpp"
