class CfgMagazineWells {
    class VSK_9x39 {
        GS_Magazines[] = {"gs_20r_9x39mm_SP5_VSK", "gs_20r_9x39mm_SP6_VSK",
                          "gs_20r_9x39mm_SPP_VSK", "gs_20r_9x39mm_BP_VSK"};
    };
    class VSS_VAL_9x39 {
        GS_Magazines[] = {
            "gs_10r_9x39mm_SP5",  "gs_10r_9x39mm_SP6",  "gs_10r_9x39mm_SPP",
            "gs_10r_9x39mm_BP",   "gs_20r_9x39mm_SP5",  "gs_20r_9x39mm_SP6",
            "gs_20r_9x39mm_SPP",  "gs_20r_9x39mm_BP",   "gs_20rS_9x39mm_SP5",
            "gs_20rS_9x39mm_SP6", "gs_20rS_9x39mm_SPP", "gs_20rS_9x39mm_BP",
            "gs_20rD_9x39mm_SP5", "gs_20rD_9x39mm_SP6", "gs_20rD_9x39mm_SPP",
            "gs_20rD_9x39mm_BP",  "gs_30r_9x39mm_SP5",  "gs_30r_9x39mm_SP6",
            "gs_30r_9x39mm_SPP",  "gs_30r_9x39mm_BP"};
        RHS_Magazines[] = {};
    };
    class CBA_762x39_AK
	{
		SPEC_mags[] = {"SPEC_30Rnd_762x39_AK47_M","SPEC_30Rnd_762x39_89_AK47_M","SPEC_30Rnd_762x39_T_AK47_M","SPEC_30Rnd_762x39_U_AK47_M","SPEC_30Rnd_762x39_AK47_bakelite_M","SPEC_30Rnd_762x39_89_AK47_bakelite_M","SPEC_30Rnd_762x39_T_AK47_bakelite_M","SPEC_30Rnd_762x39_U_AK47_bakelite_M","SPEC_30Rnd_762x39_AKM_bakelite_desert_M","SPEC_30Rnd_762x39_89_AKM_bakelite_desert_M","SPEC_30Rnd_762x39_T_AKM_bakelite_desert_M","SPEC_30Rnd_762x39_U_AKM_bakelite_desert_M"};
	};
	class CBA_762x39_RPK
	{
		SPEC_mags[] = {"SPEC_75Rnd_TE4_LRT4_762x39_RPK_M","SPEC_75Rnd_TE4_LRT4_762x39_89_RPK_M","SPEC_75Rnd_TE4_LRT4_762x39_T_RPK_M","SPEC_75Rnd_TE4_LRT4_762x39_U_RPK_M","SPEC_40Rnd_TE4_LRT4_762x39_RPK_M","SPEC_40Rnd_TE4_LRT4_762x39_89_RPK_M","SPEC_40Rnd_TE4_LRT4_762x39_T_RPK_M","SPEC_40Rnd_TE4_LRT4_762x39_U_RPK_M"};
	};
	class CBA_545x39_AK
	{
		SPEC_mags[] = {"SPEC_30Rnd_545x39_7N10_AK74M_camo_M","SPEC_30Rnd_545x39_7U1_AK74M_camo_M","SPEC_30Rnd_545x39_green_AK74M_camo_M","SPEC_30Rnd_545x39_7N22_AK74M_camo_M","SPEC_30Rnd_545x39_7N10_AK74M_desert_M","SPEC_30Rnd_545x39_7U1_AK74M_desert_M","SPEC_30Rnd_545x39_green_AK74M_desert_M","SPEC_30Rnd_545x39_7N22_AK74M_desert_M","SPEC_30Rnd_545x39_7N6_AK_M","SPEC_30Rnd_545x39_7N6M_AK_M","SPEC_30Rnd_545x39_7N10_AK_M","SPEC_30Rnd_545x39_7U1_AK_M","SPEC_30Rnd_545x39_7T3M_AK_M","SPEC_45Rnd_545x39_7N10_RPK74M","SPEC_45Rnd_545x39_7U1_RPK74M","SPEC_45Rnd_545x39_green_RPK74M","SPEC_45Rnd_545x39_7N22_RPK74M"};
	};
	class CBA_545x39_RPK
	{
		SPEC_mags[] = {"SPEC_45Rnd_TE4_LRT4_7N6_545x39_RPK_M","SPEC_45Rnd_TE4_LRT4_7N6M_545x39_RPK_M","SPEC_45Rnd_TE4_LRT4_7N10_545x39_RPK_M","SPEC_45Rnd_TE4_LRT4_7U1_545x39_RPK_M","SPEC_45Rnd_TE4_LRT4_7T3M_545x39_RPK_M","SPEC_45Rnd_545x39_7N10_RPK74M","SPEC_45Rnd_545x39_7U1_RPK74M","SPEC_45Rnd_545x39_green_RPK74M","SPEC_45Rnd_545x39_7N22_RPK74M"};
	};
};
