class CfgMagazines {
    class rhs_30Rnd_762x39mm;
    class rhs_30Rnd_762x39mm_89;
    class rhs_30Rnd_762x39mm_tracer;
    class rhs_30Rnd_762x39mm_U;
    class rhs_30Rnd_545x39_7N10_AK;
    class rhs_30Rnd_545x39_7U1_AK;
    class rhs_30Rnd_545x39_AK_green;
    class rhs_30Rnd_545x39_7N22_AK;
    class rhs_30Rnd_545x39_7N6_AK;
    class rhs_30Rnd_545x39_7N6M_AK;
    class rhs_45Rnd_545X39_7N6_AK;
    class rhs_45Rnd_545X39_7N10_AK;
    class rhs_45Rnd_545X39_7U1_AK;
    class rhs_45Rnd_545X39_AK_Green;
    class rhs_10rnd_9x39mm_SP5;
    class rhs_10rnd_9x39mm_SP6;
    class gs_20r_9x39mm_SP5;
    class gs_20r_9x39mm_SP6;
    class gs_20r_9x39mm_SPP;
    class gs_20r_9x39mm_BP;

    class SPEC_30Rnd_762x39_AK47_M: rhs_30Rnd_762x39mm
	{
		author = "$STR_CUP_AUTHOR_STRING";
		scope = 2;
        mass = 13.53;
		displayName = "30Птр. АКМ 57-Н-231";
		picture = "\CUP\Weapons\CUP_Weapons_Ammunition\data\ui\m_ak_ca.paa";
		descriptionShort = "$STR_CUP_dss_ak47_30rnd_M";
		model = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines\CUP_mag_30Rnd_AKM.p3d";
		modelSpecial = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines_proxy\CUP_mag_30Rnd_AKM.p3d";
		modelSpecialIsProxy = 1;
		hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"\cup\weapons\cup_weapons_ak\data\akm_magazine_co.paa"};
	};

    class SPEC_30Rnd_762x39_89_AK47_M: rhs_30Rnd_762x39mm_89
	{
		author = "$STR_CUP_AUTHOR_STRING";
		scope = 2;
        mass = 13.53;
		displayName = "30Птр. АКМ 57-Н-231(89)";
		picture = "\CUP\Weapons\CUP_Weapons_Ammunition\data\ui\m_ak_ca.paa";
		descriptionShort = "$STR_CUP_dss_ak47_30rnd_M";
		model = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines\CUP_mag_30Rnd_AKM.p3d";
		modelSpecial = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines_proxy\CUP_mag_30Rnd_AKM.p3d";
		modelSpecialIsProxy = 1;
		hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"\cup\weapons\cup_weapons_ak\data\akm_magazine_co.paa"};
	};

    class SPEC_30Rnd_762x39_T_AK47_M: rhs_30Rnd_762x39mm_tracer
	{
		author = "$STR_CUP_AUTHOR_STRING";
		scope = 2;
        mass = 13.53;
		displayName = "30Птр. АКМ 57-Н-231П (Трассер)";
		picture = "\CUP\Weapons\CUP_Weapons_Ammunition\data\ui\m_ak_ca.paa";
		descriptionShort = "$STR_CUP_dss_ak47_30rnd_M";
		model = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines\CUP_mag_30Rnd_AKM.p3d";
		modelSpecial = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines_proxy\CUP_mag_30Rnd_AKM.p3d";
		modelSpecialIsProxy = 1;
		hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"\cup\weapons\cup_weapons_ak\data\akm_magazine_co.paa"};
	};

    class SPEC_30Rnd_762x39_U_AK47_M: rhs_30Rnd_762x39mm_U
	{
		author = "$STR_CUP_AUTHOR_STRING";
		scope = 2;
        mass = 13.53;
		displayName = "30Птр. АКМ 57-Н-231У (дозвуковой)";
		picture = "\CUP\Weapons\CUP_Weapons_Ammunition\data\ui\m_ak_ca.paa";
		descriptionShort = "$STR_CUP_dss_ak47_30rnd_M";
		model = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines\CUP_mag_30Rnd_AKM.p3d";
		modelSpecial = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines_proxy\CUP_mag_30Rnd_AKM.p3d";
		modelSpecialIsProxy = 1;
		hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"\cup\weapons\cup_weapons_ak\data\akm_magazine_co.paa"};
	};

    class SPEC_30Rnd_762x39_AK47_bakelite_M: SPEC_30Rnd_762x39_AK47_M
	{
		author = "$STR_CUP_AUTHOR_STRING";
		scope = 2;
		displayName = "30Птр. АКМ (бакелит) 57-Н-231";
		picture = "\CUP\Weapons\CUP_Weapons_Ammunition\data\ui\m_akm_bakelite_ca.paa";
		mass = 13.53;
		model = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines\CUP_mag_30Rnd_AKM_bakelite.p3d";
		modelSpecial = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines_proxy\CUP_mag_30Rnd_AKM_bakelite.p3d";
		modelSpecialIsProxy = 1;
		hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"\cup\weapons\cup_weapons_ak\data\akm_magazine_co.paa"};
	};

    class SPEC_30Rnd_762x39_T_AK47_bakelite_M: SPEC_30Rnd_762x39_T_AK47_M
	{
		author = "$STR_CUP_AUTHOR_STRING";
		scope = 2;
		displayName = "30Птр. АКМ (бакелит) 57-Н-231П (Трассер)";
		picture = "\CUP\Weapons\CUP_Weapons_Ammunition\data\ui\m_akm_bakelite_ca.paa";
		mass = 13.53;
		model = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines\CUP_mag_30Rnd_AKM_bakelite.p3d";
		modelSpecial = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines_proxy\CUP_mag_30Rnd_AKM_bakelite.p3d";
		modelSpecialIsProxy = 1;
		hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"\cup\weapons\cup_weapons_ak\data\akm_magazine_co.paa"};
	};

    class SPEC_30Rnd_762x39_89_AK47_bakelite_M: SPEC_30Rnd_762x39_89_AK47_M
	{
		author = "$STR_CUP_AUTHOR_STRING";
		scope = 2;
		displayName = "30Птр. АКМ (бакелит) 57-Н-231(89)";
		picture = "\CUP\Weapons\CUP_Weapons_Ammunition\data\ui\m_akm_bakelite_ca.paa";
		mass = 13.53;
		model = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines\CUP_mag_30Rnd_AKM_bakelite.p3d";
		modelSpecial = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines_proxy\CUP_mag_30Rnd_AKM_bakelite.p3d";
		modelSpecialIsProxy = 1;
		hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"\cup\weapons\cup_weapons_ak\data\akm_magazine_co.paa"};
	};

    class SPEC_30Rnd_762x39_U_AK47_bakelite_M: SPEC_30Rnd_762x39_U_AK47_M
	{
		author = "$STR_CUP_AUTHOR_STRING";
		scope = 2;
		displayName = "30Птр. АКМ (бакелит) 57-Н-231У (дозвуковой)";
		picture = "\CUP\Weapons\CUP_Weapons_Ammunition\data\ui\m_akm_bakelite_ca.paa";
		mass = 13.53;
		model = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines\CUP_mag_30Rnd_AKM_bakelite.p3d";
		modelSpecial = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines_proxy\CUP_mag_30Rnd_AKM_bakelite.p3d";
		modelSpecialIsProxy = 1;
		hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"\cup\weapons\cup_weapons_ak\data\akm_magazine_co.paa"};
	};

    class SPEC_30Rnd_762x39_AKM_bakelite_desert_M: SPEC_30Rnd_762x39_AK47_M
	{
		author = "$STR_CUP_AUTHOR_STRING";
		scope = 2;
		displayName = "30Птр. АКМ (пустынный) 57-Н-231";
		picture = "\CUP\Weapons\CUP_Weapons_Ammunition\data\ui\m_akm_desert_ca.paa";
		mass = 13.53;
		model = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines\CUP_mag_30Rnd_AKM_bakelite_desert.p3d";
		modelSpecial = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines_proxy\CUP_mag_30Rnd_AKM_bakelite.p3d";
		modelSpecialIsProxy = 1;
		hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"\cup\weapons\cup_weapons_ammunition\magazines\data\akm_magazine_desert_co.paa"};
	};

    class SPEC_30Rnd_762x39_89_AKM_bakelite_desert_M: SPEC_30Rnd_762x39_89_AK47_M
	{
		author = "$STR_CUP_AUTHOR_STRING";
		scope = 2;
		displayName = "30Птр. АКМ (пустынный) 57-Н-231(89)";
		picture = "\CUP\Weapons\CUP_Weapons_Ammunition\data\ui\m_akm_desert_ca.paa";
		mass = 13.53;
		model = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines\CUP_mag_30Rnd_AKM_bakelite_desert.p3d";
		modelSpecial = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines_proxy\CUP_mag_30Rnd_AKM_bakelite.p3d";
		modelSpecialIsProxy = 1;
		hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"\cup\weapons\cup_weapons_ammunition\magazines\data\akm_magazine_desert_co.paa"};
	};

    class SPEC_30Rnd_762x39_T_AKM_bakelite_desert_M: SPEC_30Rnd_762x39_T_AK47_M
	{
		author = "$STR_CUP_AUTHOR_STRING";
		scope = 2;
		displayName = "30Птр. АКМ (пустынный) 57-Н-231П (Трассер)";
		picture = "\CUP\Weapons\CUP_Weapons_Ammunition\data\ui\m_akm_desert_ca.paa";
		mass = 13.53;
		model = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines\CUP_mag_30Rnd_AKM_bakelite_desert.p3d";
		modelSpecial = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines_proxy\CUP_mag_30Rnd_AKM_bakelite.p3d";
		modelSpecialIsProxy = 1;
		hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"\cup\weapons\cup_weapons_ammunition\magazines\data\akm_magazine_desert_co.paa"};
	};

    class SPEC_30Rnd_762x39_U_AKM_bakelite_desert_M: SPEC_30Rnd_762x39_U_AK47_M
	{
		author = "$STR_CUP_AUTHOR_STRING";
		scope = 2;
		displayName = "30Птр. АКМ (пустынный) 57-Н-231У (дозвуковой)";
		picture = "\CUP\Weapons\CUP_Weapons_Ammunition\data\ui\m_akm_desert_ca.paa";
		mass = 13.53;
		model = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines\CUP_mag_30Rnd_AKM_bakelite_desert.p3d";
		modelSpecial = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines_proxy\CUP_mag_30Rnd_AKM_bakelite.p3d";
		modelSpecialIsProxy = 1;
		hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"\cup\weapons\cup_weapons_ammunition\magazines\data\akm_magazine_desert_co.paa"};
	};

    class SPEC_75Rnd_TE4_LRT4_762x39_RPK_M: SPEC_30Rnd_762x39_AK47_M
	{
		author = "$STR_CUP_AUTHOR_STRING";
		scope = 2;
        count = 75;
		displayName = "75Птр. РПК 57-Н-231";
		picture = "\CUP\Weapons\CUP_Weapons_Ammunition\data\ui\M_RPK_74_CA.paa";
		descriptionShort = "$STR_CUP_dss_rpk762_75rnd_M";
		mass = 33.68;
		ACE_isBelt = 0;
		model = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines\CUP_mag_75Rnd_RPK.p3d";
		modelSpecial = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines_proxy\CUP_mag_75Rnd_RPK.p3d";
		modelSpecialIsProxy = 1;
		hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"\cup\weapons\cup_weapons_ak\data\rpk_magazine_drum_co.paa"};
	};

    class SPEC_75Rnd_TE4_LRT4_762x39_89_RPK_M: SPEC_30Rnd_762x39_89_AK47_M
	{
		author = "$STR_CUP_AUTHOR_STRING";
		scope = 2;
        count = 75;
		displayName = "75Птр. РПК 57-Н-231(89)";
		picture = "\CUP\Weapons\CUP_Weapons_Ammunition\data\ui\M_RPK_74_CA.paa";
		descriptionShort = "$STR_CUP_dss_rpk762_75rnd_M";
		mass = 33.68;
		ACE_isBelt = 0;
		model = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines\CUP_mag_75Rnd_RPK.p3d";
		modelSpecial = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines_proxy\CUP_mag_75Rnd_RPK.p3d";
		modelSpecialIsProxy = 1;
		hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"\cup\weapons\cup_weapons_ak\data\rpk_magazine_drum_co.paa"};
	};

    class SPEC_75Rnd_TE4_LRT4_762x39_T_RPK_M: SPEC_30Rnd_762x39_T_AK47_M
	{
		author = "$STR_CUP_AUTHOR_STRING";
		scope = 2;
        count = 75;
		displayName = "75Птр. РПК 57-Н-231П (Трассер)";
		picture = "\CUP\Weapons\CUP_Weapons_Ammunition\data\ui\M_RPK_74_CA.paa";
		descriptionShort = "$STR_CUP_dss_rpk762_75rnd_M";
		mass = 33.68;
		ACE_isBelt = 0;
		model = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines\CUP_mag_75Rnd_RPK.p3d";
		modelSpecial = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines_proxy\CUP_mag_75Rnd_RPK.p3d";
		modelSpecialIsProxy = 1;
		hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"\cup\weapons\cup_weapons_ak\data\rpk_magazine_drum_co.paa"};
	};

    class SPEC_75Rnd_TE4_LRT4_762x39_U_RPK_M: SPEC_30Rnd_762x39_U_AK47_M
	{
		author = "$STR_CUP_AUTHOR_STRING";
		scope = 2;
        count = 75;
		displayName = "75Птр. РПК 57-Н-231У (дозвуковой)";
		picture = "\CUP\Weapons\CUP_Weapons_Ammunition\data\ui\M_RPK_74_CA.paa";
		descriptionShort = "$STR_CUP_dss_rpk762_75rnd_M";
		mass = 33.68;
		ACE_isBelt = 0;
		model = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines\CUP_mag_75Rnd_RPK.p3d";
		modelSpecial = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines_proxy\CUP_mag_75Rnd_RPK.p3d";
		modelSpecialIsProxy = 1;
		hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"\cup\weapons\cup_weapons_ak\data\rpk_magazine_drum_co.paa"};
	};

	class SPEC_40Rnd_TE4_LRT4_762x39_RPK_M: SPEC_30Rnd_762x39_AK47_M
	{
		author = "$STR_CUP_AUTHOR_STRING";
		scope = 2;
        count = 40;
		displayName = "40Птр. РПК 57-Н-231";
		picture = "\CUP\Weapons\CUP_Weapons_Ammunition\data\ui\m_rpk_40_ca.paa";
		descriptionShort = "$STR_CUP_dss_rpk_40rnd_M";
		mass = 19.06;
		ACE_isBelt = 0;
		model = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines\CUP_mag_40Rnd_RPK.p3d";
		modelSpecial = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines_proxy\CUP_mag_40Rnd_RPK.p3d";
		modelSpecialIsProxy = 1;
		hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"\cup\weapons\cup_weapons_ak\data\rpk_magazine_co.paa"};
	};

    class SPEC_40Rnd_TE4_LRT4_762x39_89_RPK_M: SPEC_30Rnd_762x39_89_AK47_M
	{
		author = "$STR_CUP_AUTHOR_STRING";
		scope = 2;
        count = 40;
		displayName = "40Птр. РПК 57-Н-231(89)";
		picture = "\CUP\Weapons\CUP_Weapons_Ammunition\data\ui\m_rpk_40_ca.paa";
		descriptionShort = "$STR_CUP_dss_rpk_40rnd_M";
		mass = 19.06;
		ACE_isBelt = 0;
		model = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines\CUP_mag_40Rnd_RPK.p3d";
		modelSpecial = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines_proxy\CUP_mag_40Rnd_RPK.p3d";
		modelSpecialIsProxy = 1;
		hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"\cup\weapons\cup_weapons_ak\data\rpk_magazine_co.paa"};
	};

    class SPEC_40Rnd_TE4_LRT4_762x39_T_RPK_M: SPEC_30Rnd_762x39_T_AK47_M
	{
		author = "$STR_CUP_AUTHOR_STRING";
		scope = 2;
        count = 40;
		displayName = "40Птр. РПК 57-Н-231П (Трассер)";
		picture = "\CUP\Weapons\CUP_Weapons_Ammunition\data\ui\m_rpk_40_ca.paa";
		descriptionShort = "$STR_CUP_dss_rpk_40rnd_M";
		mass = 19.06;
		ACE_isBelt = 0;
		model = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines\CUP_mag_40Rnd_RPK.p3d";
		modelSpecial = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines_proxy\CUP_mag_40Rnd_RPK.p3d";
		modelSpecialIsProxy = 1;
		hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"\cup\weapons\cup_weapons_ak\data\rpk_magazine_co.paa"};
	};

    class SPEC_40Rnd_TE4_LRT4_762x39_U_RPK_M: SPEC_30Rnd_762x39_U_AK47_M
	{
		author = "$STR_CUP_AUTHOR_STRING";
		scope = 2;
        count = 40;
		displayName = "40Птр. РПК 57-Н-231У (дозвуковой)";
		picture = "\CUP\Weapons\CUP_Weapons_Ammunition\data\ui\m_rpk_40_ca.paa";
		descriptionShort = "$STR_CUP_dss_rpk_40rnd_M";
		mass = 19.06;
		ACE_isBelt = 0;
		model = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines\CUP_mag_40Rnd_RPK.p3d";
		modelSpecial = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines_proxy\CUP_mag_40Rnd_RPK.p3d";
		modelSpecialIsProxy = 1;
		hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"\cup\weapons\cup_weapons_ak\data\rpk_magazine_co.paa"};
	};

    class SPEC_30Rnd_545x39_7N10_AK74M_camo_M: rhs_30Rnd_545x39_7N10_AK
	{
		author = "$STR_CUP_AUTHOR_STRING";
		displayName = "30Птр. АК-74 (лесной) 7Н10";
		picture = "\CUP\Weapons\CUP_Weapons_Ammunition\data\ui\m_ak74m_camo_ca.paa";
		model = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines\CUP_mag_30Rnd_AK74M_camo.p3d";
		modelSpecial = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines_proxy\CUP_mag_30Rnd_AK74M.p3d";
        hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"\CUP\Weapons\CUP_Weapons_Ammunition\magazines\data\ak74m_magazine_camo_co.paa"};
	};

    class SPEC_30Rnd_545x39_7U1_AK74M_camo_M: rhs_30Rnd_545x39_7U1_AK
	{
		author = "$STR_CUP_AUTHOR_STRING";
		displayName = "30Птр. АК-74 (лесной) 7У1 (дозвуковой)";
		picture = "\CUP\Weapons\CUP_Weapons_Ammunition\data\ui\m_ak74m_camo_ca.paa";
		model = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines\CUP_mag_30Rnd_AK74M_camo.p3d";
		modelSpecial = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines_proxy\CUP_mag_30Rnd_AK74M.p3d";
        hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"\CUP\Weapons\CUP_Weapons_Ammunition\magazines\data\ak74m_magazine_camo_co.paa"};
	};

    class SPEC_30Rnd_545x39_green_AK74M_camo_M: rhs_30Rnd_545x39_AK_green
	{
		author = "$STR_CUP_AUTHOR_STRING";
		displayName = "30Птр. АК-74 (лесной) 7Т3М (Трассер)";
		picture = "\CUP\Weapons\CUP_Weapons_Ammunition\data\ui\m_ak74m_camo_ca.paa";
		model = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines\CUP_mag_30Rnd_AK74M_camo.p3d";
		modelSpecial = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines_proxy\CUP_mag_30Rnd_AK74M.p3d";
        hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"\CUP\Weapons\CUP_Weapons_Ammunition\magazines\data\ak74m_magazine_camo_co.paa"};
	};

    class SPEC_30Rnd_545x39_7N22_AK74M_camo_M: rhs_30Rnd_545x39_7N22_AK
	{
		author = "$STR_CUP_AUTHOR_STRING";
		displayName = "30Птр. АК-74 (лесной) 7Н22";
		picture = "\CUP\Weapons\CUP_Weapons_Ammunition\data\ui\m_ak74m_camo_ca.paa";
		model = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines\CUP_mag_30Rnd_AK74M_camo.p3d";
		modelSpecial = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines_proxy\CUP_mag_30Rnd_AK74M.p3d";
        hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"\CUP\Weapons\CUP_Weapons_Ammunition\magazines\data\ak74m_magazine_camo_co.paa"};
	};

    class SPEC_30Rnd_545x39_7N10_AK74M_desert_M: rhs_30Rnd_545x39_7N10_AK
	{
		author = "$STR_CUP_AUTHOR_STRING";
		displayName = "30Птр. АК-74 (пустынный) 7Н10";
		picture = "\CUP\Weapons\CUP_Weapons_Ammunition\data\ui\m_ak74m_desert_ca.paa";
		model = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines\CUP_mag_30Rnd_AK74M_desert.p3d";
		modelSpecial = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines_proxy\CUP_mag_30Rnd_AK74M.p3d";
        hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"\CUP\Weapons\CUP_Weapons_Ammunition\magazines\data\ak74m_magazine_desert_co.paa"};
	};

    class SPEC_30Rnd_545x39_7U1_AK74M_desert_M: rhs_30Rnd_545x39_7U1_AK
	{
		author = "$STR_CUP_AUTHOR_STRING";
		displayName = "30Птр. АК-74 (пустынный) 7У1 (дозвуковой)";
		picture = "\CUP\Weapons\CUP_Weapons_Ammunition\data\ui\m_ak74m_desert_ca.paa";
		model = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines\CUP_mag_30Rnd_AK74M_desert.p3d";
		modelSpecial = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines_proxy\CUP_mag_30Rnd_AK74M.p3d";
        hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"\CUP\Weapons\CUP_Weapons_Ammunition\magazines\data\ak74m_magazine_desert_co.paa"};
	};

    class SPEC_30Rnd_545x39_green_AK74M_desert_M: rhs_30Rnd_545x39_AK_green
	{
		author = "$STR_CUP_AUTHOR_STRING";
		displayName = "30Птр. АК-74 (пустынный) 7Т3М (Трассер)";
		picture = "\CUP\Weapons\CUP_Weapons_Ammunition\data\ui\m_ak74m_desert_ca.paa";
		model = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines\CUP_mag_30Rnd_AK74M_desert.p3d";
		modelSpecial = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines_proxy\CUP_mag_30Rnd_AK74M.p3d";
        hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"\CUP\Weapons\CUP_Weapons_Ammunition\magazines\data\ak74m_magazine_desert_co.paa"};
	};

    class SPEC_30Rnd_545x39_7N22_AK74M_desert_M: rhs_30Rnd_545x39_7N22_AK
	{
		author = "$STR_CUP_AUTHOR_STRING";
		displayName = "30Птр. АК-74 (пустынный) 7Н22";
		picture = "\CUP\Weapons\CUP_Weapons_Ammunition\data\ui\m_ak74m_desert_ca.paa";
		model = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines\CUP_mag_30Rnd_AK74M_desert.p3d";
		modelSpecial = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines_proxy\CUP_mag_30Rnd_AK74M.p3d";
        hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"\CUP\Weapons\CUP_Weapons_Ammunition\magazines\data\ak74m_magazine_desert_co.paa"};
	};

    class SPEC_30Rnd_545x39_7N6_AK_M: rhs_30Rnd_545x39_7N6_AK
	{
		author = "$STR_CUP_AUTHOR_STRING";
		scope = 2;
		displayName = "30Птр. АК-74 (бакелит) 7Н6";
		picture = "\CUP\Weapons\CUP_Weapons_Ammunition\data\ui\m_ak74_ca.paa";
		descriptionShort = "$STR_CUP_dss_ak74_30rnd_M";
		mass = 9.09;
		model = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines\CUP_mag_30Rnd_AK74.p3d";
		modelSpecial = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines_proxy\CUP_mag_30Rnd_AK74.p3d";
		modelSpecialIsProxy = 1;
		hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"\cup\weapons\cup_weapons_ak\data\ak74_magazine_co.paa"};
	};

    class SPEC_30Rnd_545x39_7N6M_AK_M: rhs_30Rnd_545x39_7N6M_AK
	{
		author = "$STR_CUP_AUTHOR_STRING";
		scope = 2;
		displayName = "30Птр. АК-74 (бакелит) 7Н6М";
		picture = "\CUP\Weapons\CUP_Weapons_Ammunition\data\ui\m_ak74_ca.paa";
		descriptionShort = "$STR_CUP_dss_ak74_30rnd_M";
		mass = 9.09;
		model = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines\CUP_mag_30Rnd_AK74.p3d";
		modelSpecial = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines_proxy\CUP_mag_30Rnd_AK74.p3d";
		modelSpecialIsProxy = 1;
		hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"\cup\weapons\cup_weapons_ak\data\ak74_magazine_co.paa"};
	};

    class SPEC_30Rnd_545x39_7N10_AK_M: rhs_30Rnd_545x39_7N10_AK
	{
		author = "$STR_CUP_AUTHOR_STRING";
		scope = 2;
		displayName = "30Птр. АК-74 (бакелит) 7Н10";
		picture = "\CUP\Weapons\CUP_Weapons_Ammunition\data\ui\m_ak74_ca.paa";
		descriptionShort = "$STR_CUP_dss_ak74_30rnd_M";
		mass = 9.09;
		model = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines\CUP_mag_30Rnd_AK74.p3d";
		modelSpecial = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines_proxy\CUP_mag_30Rnd_AK74.p3d";
		modelSpecialIsProxy = 1;
		hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"\cup\weapons\cup_weapons_ak\data\ak74_magazine_co.paa"};
	};

    class SPEC_30Rnd_545x39_7U1_AK_M: rhs_30Rnd_545x39_7U1_AK
	{
		author = "$STR_CUP_AUTHOR_STRING";
		scope = 2;
		displayName = "30Птр. АК-74 (бакелит) 7У1 (дозвуковой)";
		picture = "\CUP\Weapons\CUP_Weapons_Ammunition\data\ui\m_ak74_ca.paa";
		descriptionShort = "$STR_CUP_dss_ak74_30rnd_M";
		mass = 9.09;
		model = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines\CUP_mag_30Rnd_AK74.p3d";
		modelSpecial = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines_proxy\CUP_mag_30Rnd_AK74.p3d";
		modelSpecialIsProxy = 1;
		hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"\cup\weapons\cup_weapons_ak\data\ak74_magazine_co.paa"};
	};

    class SPEC_30Rnd_545x39_7T3M_AK_M: rhs_30Rnd_545x39_AK_green
	{
		author = "$STR_CUP_AUTHOR_STRING";
		scope = 2;
		displayName = "30Птр. АК-74 (бакелит) 7Т3М (Трассер)";
		picture = "\CUP\Weapons\CUP_Weapons_Ammunition\data\ui\m_ak74_ca.paa";
		descriptionShort = "$STR_CUP_dss_ak74_30rnd_M";
		mass = 9.09;
		model = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines\CUP_mag_30Rnd_AK74.p3d";
		modelSpecial = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines_proxy\CUP_mag_30Rnd_AK74.p3d";
		modelSpecialIsProxy = 1;
		hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"\cup\weapons\cup_weapons_ak\data\ak74_magazine_co.paa"};
	};

    class SPEC_45Rnd_TE4_LRT4_7N6_545x39_RPK_M: rhs_45Rnd_545X39_7N6_AK
	{
		author = "$STR_CUP_AUTHOR_STRING";
		scope = 2;
		displayName = "45Птр. РПК-74 (бакелит) 7Н6";
		picture = "\CUP\Weapons\CUP_Weapons_Ammunition\data\ui\m_rpk74_ca.paa";
		descriptionShort = "$STR_CUP_dss_rpk_45rnd_M";
        count = 45;
		mass = 13.64;
		ACE_isBelt = 0;
		model = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines\CUP_mag_45Rnd_RPK74.p3d";
		modelSpecial = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines_proxy\CUP_mag_45Rnd_RPK74.p3d";
		modelSpecialIsProxy = 1;
		hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"\cup\weapons\cup_weapons_ak\data\rpk_furniture_co.paa"};
	};

    class SPEC_45Rnd_TE4_LRT4_7N6M_545x39_RPK_M: rhs_30Rnd_545x39_7N6M_AK
	{
		author = "$STR_CUP_AUTHOR_STRING";
		scope = 2;
        count = 45;
		displayName = "45Птр. РПК-74 (бакелит) 7Н6М";
		picture = "\CUP\Weapons\CUP_Weapons_Ammunition\data\ui\m_rpk74_ca.paa";
		descriptionShort = "$STR_CUP_dss_rpk_45rnd_M";
        count = 45;
		mass = 13.64;
		ACE_isBelt = 0;
		model = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines\CUP_mag_45Rnd_RPK74.p3d";
		modelSpecial = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines_proxy\CUP_mag_45Rnd_RPK74.p3d";
		modelSpecialIsProxy = 1;
		hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"\cup\weapons\cup_weapons_ak\data\rpk_furniture_co.paa"};
	};

    class SPEC_45Rnd_TE4_LRT4_7N10_545x39_RPK_M: rhs_45Rnd_545X39_7N10_AK
	{
		author = "$STR_CUP_AUTHOR_STRING";
		scope = 2;
		displayName = "45Птр. РПК-74 (бакелит) 7Н10";
		picture = "\CUP\Weapons\CUP_Weapons_Ammunition\data\ui\m_rpk74_ca.paa";
		descriptionShort = "$STR_CUP_dss_rpk_45rnd_M";
        count = 45;
		mass = 13.64;
		ACE_isBelt = 0;
		model = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines\CUP_mag_45Rnd_RPK74.p3d";
		modelSpecial = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines_proxy\CUP_mag_45Rnd_RPK74.p3d";
		modelSpecialIsProxy = 1;
		hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"\cup\weapons\cup_weapons_ak\data\rpk_furniture_co.paa"};
	};

    class SPEC_45Rnd_TE4_LRT4_7U1_545x39_RPK_M: rhs_45Rnd_545X39_7U1_AK
	{
		author = "$STR_CUP_AUTHOR_STRING";
		scope = 2;
		displayName = "45Птр. РПК-74 (бакелит) 7У1 (дозвуковой)";
		picture = "\CUP\Weapons\CUP_Weapons_Ammunition\data\ui\m_rpk74_ca.paa";
		descriptionShort = "$STR_CUP_dss_rpk_45rnd_M";
        count = 45;
		mass = 13.64;
		ACE_isBelt = 0;
		model = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines\CUP_mag_45Rnd_RPK74.p3d";
		modelSpecial = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines_proxy\CUP_mag_45Rnd_RPK74.p3d";
		modelSpecialIsProxy = 1;
		hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"\cup\weapons\cup_weapons_ak\data\rpk_furniture_co.paa"};
	};

    class SPEC_45Rnd_TE4_LRT4_7T3M_545x39_RPK_M: rhs_45Rnd_545X39_AK_Green
	{
		author = "$STR_CUP_AUTHOR_STRING";
		scope = 2;
		displayName = "45Птр. РПК-74 (бакелит) 7Т3М (Трассер)";
		picture = "\CUP\Weapons\CUP_Weapons_Ammunition\data\ui\m_rpk74_ca.paa";
		descriptionShort = "$STR_CUP_dss_rpk_45rnd_M";
        count = 45;
		mass = 13.64;
		ACE_isBelt = 0;
		model = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines\CUP_mag_45Rnd_RPK74.p3d";
		modelSpecial = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines_proxy\CUP_mag_45Rnd_RPK74.p3d";
		modelSpecialIsProxy = 1;
		hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"\cup\weapons\cup_weapons_ak\data\rpk_furniture_co.paa"};
	};

    class SPEC_20Rnd_9x39_SP5_GROZA_M: gs_20r_9x39mm_SP5
	{
		author = "$STR_CUP_AUTHOR_STRING";
		scope = 2;
		displayName = "20Пат.9х39 СП5(ОЦ-14)";
		picture = "\CUP\Weapons\CUP_Weapons_Ammunition\data\ui\M_groza_CA.paa";
		descriptionShort = "$STR_CUP_dss_groza_20rnd_M";
		mass = 8.09;
		model = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines\CUP_mag_20Rnd_Groza.p3d";
		modelSpecial = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines_proxy\CUP_mag_20Rnd_Groza.p3d";
		modelSpecialIsProxy = 1;
		hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"\cup\weapons\cup_weapons_ammunition\magazines\data\groza_magazine_co.paa"};
	};

    class SPEC_20Rnd_9x39_SP6_GROZA_M: gs_20r_9x39mm_SP6
	{
		author = "$STR_CUP_AUTHOR_STRING";
		scope = 2;
		displayName = "20Пат.9х39 СП6(ОЦ-14)";
		picture = "\CUP\Weapons\CUP_Weapons_Ammunition\data\ui\M_groza_CA.paa";
		descriptionShort = "$STR_CUP_dss_groza_20rnd_M";
		mass = 8.09;
		model = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines\CUP_mag_20Rnd_Groza.p3d";
		modelSpecial = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines_proxy\CUP_mag_20Rnd_Groza.p3d";
		modelSpecialIsProxy = 1;
		hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"\cup\weapons\cup_weapons_ammunition\magazines\data\groza_magazine_co.paa"};
	};

    class SPEC_20Rnd_9x39_SPP_GROZA_M: gs_20r_9x39mm_SPP
	{
		author = "$STR_CUP_AUTHOR_STRING";
		scope = 2;
		displayName = "20Пат.9х39 СПП(ОЦ-14)";
		picture = "\CUP\Weapons\CUP_Weapons_Ammunition\data\ui\M_groza_CA.paa";
		descriptionShort = "$STR_CUP_dss_groza_20rnd_M";
		mass = 8.09;
		model = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines\CUP_mag_20Rnd_Groza.p3d";
		modelSpecial = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines_proxy\CUP_mag_20Rnd_Groza.p3d";
		modelSpecialIsProxy = 1;
		hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"\cup\weapons\cup_weapons_ammunition\magazines\data\groza_magazine_co.paa"};
	};

    class SPEC_20Rnd_9x39_BP_GROZA_M: gs_20r_9x39mm_BP
	{
		author = "$STR_CUP_AUTHOR_STRING";
		scope = 2;
		displayName = "20Пат.9х39 БП(ОЦ-14)";
		picture = "\CUP\Weapons\CUP_Weapons_Ammunition\data\ui\M_groza_CA.paa";
		descriptionShort = "$STR_CUP_dss_groza_20rnd_M";
		mass = 8.09;
		model = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines\CUP_mag_20Rnd_Groza.p3d";
		modelSpecial = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines_proxy\CUP_mag_20Rnd_Groza.p3d";
		modelSpecialIsProxy = 1;
		hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"\cup\weapons\cup_weapons_ammunition\magazines\data\groza_magazine_co.paa"};
	};

    class SPEC_45Rnd_545x39_7N10_RPK74M: rhs_30Rnd_545x39_7N10_AK
	{
		author = "$STR_CUP_AUTHOR_STRING";
		scope = 2;
		displayName = "45Птр. РПК-74М 7Н10";
		picture = "\CUP\Weapons\CUP_Weapons_Ammunition\data\ui\m_rpk74m_ca.paa";
		descriptionShort = "$STR_CUP_dss_rpk_45rnd_M";
        count = 45;
		mass = 13.64;
		ACE_isBelt = 0;
		model = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines\CUP_mag_45Rnd_RPK74M.p3d";
		modelSpecial = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines_proxy\CUP_mag_45Rnd_RPK74M.p3d";
		modelSpecialIsProxy = 1;
		hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"\cup\weapons\cup_weapons_ak\data\rpk74m_magazine_co.paa"};
	};

    class SPEC_45Rnd_545x39_7U1_RPK74M: rhs_30Rnd_545x39_7U1_AK
	{
		author = "$STR_CUP_AUTHOR_STRING";	
		scope = 2;	
		displayName = "45Птр. РПК-74М 7У1 (дозвуковой)";
		picture = "\CUP\Weapons\CUP_Weapons_Ammunition\data\ui\m_rpk74m_ca.paa";
		descriptionShort = "$STR_CUP_dss_rpk_45rnd_M";
        count = 45;
		mass = 13.64;
		ACE_isBelt = 0;
		model = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines\CUP_mag_45Rnd_RPK74M.p3d";
		modelSpecial = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines_proxy\CUP_mag_45Rnd_RPK74M.p3d";
		modelSpecialIsProxy = 1;
		hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"\cup\weapons\cup_weapons_ak\data\rpk74m_magazine_co.paa"};
	};

    class SPEC_45Rnd_545x39_green_RPK74M: rhs_30Rnd_545x39_AK_green
	{
		author = "$STR_CUP_AUTHOR_STRING";
		scope = 2;		
		displayName = "45Птр. РПК-74М 7Т3М (Трассер)";
		picture = "\CUP\Weapons\CUP_Weapons_Ammunition\data\ui\m_rpk74m_ca.paa";
		descriptionShort = "$STR_CUP_dss_rpk_45rnd_M";
        count = 45;
		mass = 13.64;
		ACE_isBelt = 0;
		model = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines\CUP_mag_45Rnd_RPK74M.p3d";
		modelSpecial = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines_proxy\CUP_mag_45Rnd_RPK74M.p3d";
		modelSpecialIsProxy = 1;
		hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"\cup\weapons\cup_weapons_ak\data\rpk74m_magazine_co.paa"};
	};

    class SPEC_45Rnd_545x39_7N22_RPK74M: rhs_30Rnd_545x39_7N22_AK
	{
		author = "$STR_CUP_AUTHOR_STRING";	
		scope = 2;	
		displayName = "45Птр. РПК-74М 7Н22";
		picture = "\CUP\Weapons\CUP_Weapons_Ammunition\data\ui\m_rpk74m_ca.paa";
		descriptionShort = "$STR_CUP_dss_rpk_45rnd_M";
        count = 45;
		mass = 13.64;
		ACE_isBelt = 0;
		model = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines\CUP_mag_45Rnd_RPK74M.p3d";
		modelSpecial = "\CUP\Weapons\CUP_Weapons_Ammunition\magazines_proxy\CUP_mag_45Rnd_RPK74M.p3d";
		modelSpecialIsProxy = 1;
		hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"\cup\weapons\cup_weapons_ak\data\rpk74m_magazine_co.paa"};
	};
};
