#define COMPONENT nametags
#include "\z\speclib\addons\main\script_mod.hpp"

// #define DEBUG_MODE_FULL
// #define DISABLE_COMPILE_CACHE

#ifdef DEBUG_ENABLED_NAMETAGS
    #define DEBUG_MODE_FULL
#endif
    #ifdef DEBUG_SETTINGS_NAMETAGS
    #define DEBUG_SETTINGS DEBUG_SETTINGS_NAMETAGS
#endif

#include "\z\speclib\addons\main\script_macros.hpp"
