/*
 * Author: commy2, esteldunedain
 * Draw the nametag and rank icon.
 *
 * Arguments:
 * 0: Unit (Player) <OBJECT>
 * 1: Target <OBJECT>
 * 2: Alpha <NUMBER>
 * 4: Height offset <NUMBER>
 * 5: Draw name <BOOL>
 * 5: Draw rank <BOOL>
 * 6: Draw soundwave <BOOL>
 *
 * Return Value:
 * None
 *
 * Example:
 * [ACE_player, bob, 0.5, height, true, true, true] call ace_nametags_fnc_drawNameTagIcon
 *
 * Public: No
 */

#include "script_component.hpp"

TRACE_1("drawName:", _this);

params ["", "_target", "", "_heightOffset"];

//--> Kaban
//проверим, попадает ли голова игрока в экран
private _headPos = worldToScreen(_target modelToWorldVisual ((_target selectionPosition "pilot") vectorAdd [0,0,-0.1]));
if(_headPos isEqualTo [])exitWith{}; //голова игрока далеко за границами экрана
if(!(_headPos inArea [[0.5, 0.5], 0.5, 0.5, 0, true]))exitWith{}; //голова в координатах экрана
//<-- Kaban

_fnc_parameters = {
    params ["_player", "_target", "_alpha", "_heightOffset", "_drawName", "_drawRank", "_drawSoundwave"];

    //Set Icon:
    private _icon = "";
    private _size = 0;
	//--> Kaban
	private _fov = param [7, 0.98]; //новый (7-й) параметр, который мы передаем в эту функцию
	/*
    if (_drawSoundwave) then {
        _icon = format [QPATHTOF(UI\soundwave%1.paa), floor random 10];
        _size = 1;
    } else {
        if (_drawRank && {rank _target != ""}) then {
            _icon = GVAR(factionRanks) getVariable (_target getVariable [QGVAR(faction), faction _target]);
            if (!isNil "_icon") then {
                _icon = _icon param [ALL_RANKS find rank _target, ""];
            } else {
                _icon = format ["\A3\Ui_f\data\GUI\Cfg\Ranks\%1_gs.paa", rank _target];
            };
            _size = 1;
        };
    };
	*/
	//<-- Kaban

    //Set Text:
    private _name = if (_drawName) then {
        [_target, true, true] call ace_common_fnc_getName
    } else {
        ""
    };

    //Set Color:
    private _color = [1, 1, 1, _alpha];
    if ((group _target) != (group _player)) then {
        _color = +GVAR(defaultNametagColor); //Make a copy, then multiply both alpha values (allows client to decrease alpha in settings)
        _color set [3, (_color select 3) * _alpha];
    } else {
        _color = [[1, 1, 1, _alpha], [1, 0, 0, _alpha], [0, 1, 0, _alpha], [0, 0, 1, _alpha], [1, 1, 0, _alpha]] select ((["MAIN", "RED", "GREEN", "BLUE", "YELLOW"] find ([assignedTeam _target] param [0, "MAIN"])) max 0);
    };

    private _scale = [0.333, 0.5, 0.666, 0.83333, 1] select GVAR(tagSize);

	//--> Kaban
	//изменим размер надписи в зависимости от дистанции (при этом сохраняются пользовательские настройки размера текста)
	//_heightOffset - это дистанция
	private _distance_scale = 3 - _heightOffset / 5;   //0 метров = 3, 5 метров = 2
	_distance_scale = (_distance_scale min 3) max 2; //множитель в диаппазне от 2 до 3
	_scale = _scale * _distance_scale;
	//<-- Kaban

    [
        _icon,
        _color,
        [],
        (_size * _scale),
        (_size * _scale),
        0,
        _name,
        2,
        (0.05 * _scale),
        "RobotoCondensed"
    ]
};

private _parameters = [_this, _fnc_parameters, _target, QGVAR(drawParameters), 0.1] call ace_common_fnc_cachedCall;
//--> Kaban
//_parameters set [2, _target modelToWorldVisual ((_target selectionPosition "pilot") vectorAdd [0,0,(_heightOffset + .3)])];
//изменим положение надписи, согласно текущему зуму игрока
_parameters set [2, positionCameraToWorld [0, -2 * _fov, 10]];
//<-- Kaban


drawIcon3D _parameters;
