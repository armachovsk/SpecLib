#define COMPONENT units_ind_chdkz
#include "\z\speclib\addons\main\script_mod.hpp"

// #define DEBUG_MODE_FULL
// #define DISABLE_COMPILE_CACHE

#ifdef DEBUG_ENABLED_UNITS_IND_CHDKZ
#define DEBUG_MODE_FULL
#endif
#ifdef DEBUG_SETTINGS_UNITS_IND_CHDKZ
#define DEBUG_SETTINGS DEBUG_SETTINGS_UNITS_IND_CHDKZ
#endif

#include "\z\speclib\addons\main\script_macros.hpp"

#define MED_ITEMS                                                              \
    "ACE_morphine", "ACE_tourniquet", "ACE_fieldDressing",                     \
        "ACE_fieldDressing", "ACE_packingBandage", "ACE_packingBandage"

#define REGULAR_ITEMS "ACE_EarPlugs"

#define SMERSH_ITEMS                                                           \
    "ACE_elasticBandage", "ACE_elasticBandage", "ACE_quikclot", "ACE_quikclot"

#define LINKED_ITEMS_FULL "ItemMap", "ItemCompass", "ItemWatch", "ItemRadio"

#define LINKED_ITEMS "ItemMap", "ItemCompass", "ItemWatch", "ItemRadio"

#define DEFAULT_UNIT_WEAPONS "Throw", "Put"

#define DEFAULT_GRENADES mag_2("rhs_mag_rgd5"), mag_2("rhs_mag_f1")

#define UNIT_ENTRIES                                                           \
    side = 2;                                                                  \
    author = CSTRING(SpecLibTeam);                                             \
    dlc = "speclib";                                                           \
    scope = 2;                                                                 \
    scopeCurator = 2;                                                          \
    faction = "rhsgref_faction_chdkz_g";                                       \
    editorSubcategory = "SpecLib_I_EdSubcat_ChDKZ";                            \
    nameSound = "veh_infantry_s";                                              \
    textPlural = "infantry";                                                   \
    textSingular = "infantry";                                                 \
    selectionClan = "";                                                        \
    class EventHandlers {                                                      \
        class CBA_Extended_EventHandlers : CBA_Extended_EventHandlers {};      \
    };                                                                         \
    headgearList[] = {};                                                       \
    identityTypes[] = {"LanguageRUS_F",                                        \
                       "Head_Russian",                                         \
                       "Head_Euro",                                            \
                       "Head_Enoch",                                           \
                       "G_RUS_SF"};                                            \
    genericNames = "RussianMen"
