class CfgVehicles {
    class Man;
    class CAManBase : Man {
        class ACE_SelfActions {
            class spec_anim {
                displayName = "Animations";
                icon = "\z\speclib\addons\animations\icons\ui_A.paa";
                class spec_poses {
                    displayName = "Relax Poses";
                    icon = "\z\speclib\addons\animations\icons\ui_RP.paa";
                    ACE_STATIC_ANIMATION(pose1, "Pose 1", 'Acts_millerCamp_A');
                    ACE_STATIC_ANIMATION(pose2, "Pose 2", 'Acts_AidlPercMstpSloWWrflDnon_warmup_5_loop');
                    ACE_STATIC_ANIMATION(pose3, "Pose 3", 'Acts_AidlPercMstpSnonWnonDnon_warmup_1_loop');
                    ACE_STATIC_ANIMATION(pose4, "Pose 4", 'Acts_AidlPercMstpSnonWnonDnon_warmup_2_loop');
                    ACE_STATIC_ANIMATION(pose5, "Pose 5", 'Acts_AidlPercMstpSnonWnonDnon_warmup_3_loop');
                    ACE_STATIC_ANIMATION(pose6, "Pose 6", 'Acts_AidlPercMstpSnonWnonDnon_warmup_4_loop');
                    ACE_STATIC_ANIMATION(pose7, "Pose 7", 'Acts_AidlPercMstpSnonWnonDnon_warmup_5_loop');
                    ACE_STATIC_ANIMATION(pose8, "Pose 8", 'Acts_AidlPercMstpSnonWnonDnon_warmup_6_loop');
                    ACE_STATIC_ANIMATION(pose9, "Pose 9", 'Acts_AidlPercMstpSnonWnonDnon_warmup_7_loop');
                    ACE_STATIC_ANIMATION(pose10, "Pose 10", 'Acts_AidlPercMstpSnonWnonDnon_warmup_8_loop');
                };
                class spec_direone {
                    displayName = "Static Poses 1";
                    icon = "\z\speclib\addons\animations\icons\ui_SP.paa";
                    ACE_STATIC_REMOTE_ANIMATION(direone_1, "Pose 1", 'direone_1');
                    ACE_STATIC_REMOTE_ANIMATION(direone_2, "Pose 2", 'direone_2');
                    ACE_STATIC_REMOTE_ANIMATION(direone_3, "Pose 3", 'direone_3');
                    ACE_STATIC_REMOTE_ANIMATION(direone_4, "Pose 4", 'direone_4');
                    ACE_STATIC_REMOTE_ANIMATION(direone_5, "Pose 5", 'direone_5');
                    ACE_STATIC_REMOTE_ANIMATION(direone_6, "Pose 6", 'direone_6');
                    ACE_STATIC_REMOTE_ANIMATION(direone_7, "Pose 7", 'direone_7');
                };
                class spec_nikoaton_1 {
                    displayName = "Static Poses 2";
                    icon = "\z\speclib\addons\animations\icons\ui_SP.paa";
                    ACE_STATIC_REMOTE_ANIMATION(calm_1, "Calm 1", 'EL_stand1');
                    ACE_STATIC_REMOTE_ANIMATION(calm_2, "Calm 2", 'EL_at_cross');
                    ACE_STATIC_REMOTE_ANIMATION(calm_3, "Calm 3", 'pos_calm_45');
                    ACE_STATIC_REMOTE_ANIMATION(calm_4, "Calm 4", 'pos_calm_46');
                    ACE_STATIC_REMOTE_ANIMATION(calm_5, "Calm 5", 'pos_calm_47');
                    ACE_STATIC_REMOTE_ANIMATION(calm_6, "Calm 6", 'pos_calm_50');
                    ACE_STATIC_REMOTE_ANIMATION(calm_7, "Calm 7", 'pos_calm_52');
                    ACE_STATIC_REMOTE_ANIMATION(calm_8, "Calm 8", 'pos_calm_52_v2');
                    ACE_STATIC_REMOTE_ANIMATION(calm_9, "Calm 9", 'pos_calm_66');
                    ACE_STATIC_REMOTE_ANIMATION(calm_10, "Calm 10", 'EL_at_crouch');
                    ACE_STATIC_REMOTE_ANIMATION(calm_11, "Calm 11", 'pos_calm_69');
                    ACE_STATIC_REMOTE_ANIMATION(calm_12, "Calm 12", 'pos_calm_20_L');
                    ACE_STATIC_REMOTE_ANIMATION(calm_13, "Calm 13", 'pos_calm_20_R');
                    ACE_STATIC_REMOTE_ANIMATION(calm_14, "Calm 14", 'pos_calm_79');
                };
                class spec_nikoaton_2 {
                    displayName = "Static Poses 3";
                    icon = "\z\speclib\addons\animations\icons\ui_SP.paa";
                    ACE_STATIC_REMOTE_ANIMATION(calm_15, "Calm 15", 'p_40');
                    ACE_STATIC_REMOTE_ANIMATION(calm_16, "Calm 16", 'pos_calm_82');
                    ACE_STATIC_REMOTE_ANIMATION(calm_17, "Calm 17", 'pos_calm_83');
                    ACE_STATIC_REMOTE_ANIMATION(calm_18, "Calm 18", 'pos_calm_36_kneel');
                    ACE_STATIC_REMOTE_ANIMATION(calm_19, "Calm 19", 'pos_calm_32_obs');
                    ACE_STATIC_REMOTE_ANIMATION(calm_20, "Calm 20", 'pos_calm_31_obs');
                    ACE_STATIC_REMOTE_ANIMATION(calm_21, "Calm 21", 'pos_calm_27_cap');
                    ACE_STATIC_REMOTE_ANIMATION(calm_22, "Calm 22", 'p_14');
                    ACE_STATIC_REMOTE_ANIMATION(calm_23, "Calm 23", 'pos_calm_30_cap');
                    ACE_STATIC_REMOTE_ANIMATION(calm_24, "Calm 24", 'pos_calm_29_cap');
                    ACE_STATIC_REMOTE_ANIMATION(calm_25, "Calm 25", 'p_15b');
                    ACE_STATIC_REMOTE_ANIMATION(calm_26, "Calm 26", 'pos_calm_1_L');
                    ACE_STATIC_REMOTE_ANIMATION(calm_27, "Calm 27", 'pos_calm_1_R');
                    ACE_STATIC_REMOTE_ANIMATION(calm_28, "Calm 28", 'EL_crouch');
                    ACE_STATIC_REMOTE_ANIMATION(calm_29, "Calm 29", 'TFR_crouch3');
                };
                class spec_actions {
                    displayName = "Actions";
                    icon = "\z\speclib\addons\animations\icons\ui_Act.paa";
                    class spec_Pushup {
                        displayName = "Pushup";
                        condition = "";
                        exceptions[] = {};
                        statement = "_player playMove 'AmovPercMstpSnonWnonDnon_exercisePushUp'";
                        icon = "\z\speclib\addons\animations\icons\ui_pushup.paa";
                    };
                };
                class spec_cancel {
                    displayName = "Cancel Current Animation";
                    icon = "\z\speclib\addons\animations\icons\ui_cancel.paa";
                    condition = "";
                    exceptions[] = {};
                    statement = "[ace_player, ''] remoteExec ['switchMove', 0]";
                };
            };
        };
    };
};
