#include "script_component.hpp"
class CfgPatches {
    class ADDON {
        name = COMPONENT;
        units[] = {
            "SpecLib_O_R_W_Marine_exp_F",
            "SpecLib_O_R_W_Marine_crew_TL_F",
            "SpecLib_O_R_W_Marine_crew_F",
            "SpecLib_O_R_W_Marine_Officer_F",
            "SpecLib_O_R_W_Marine_TL_F",
            "SpecLib_O_R_W_Marine_MG_F",
            "SpecLib_O_R_W_Marine_AMG_F",
            "SpecLib_O_R_W_Marine_AR_F",
            "SpecLib_O_R_W_Marine_AT_F",
            "SpecLib_O_R_W_Marine_AAT_F",
            "SpecLib_O_R_W_Marine_medic_F",
            "SpecLib_O_R_W_Marine_engineer_F",
            "SpecLib_O_R_W_Marine_F",
            "SpecLib_O_R_W_Marine_GL_F",
            "SpecLib_O_R_W_Marine_LAT_01_F",
            "SpecLib_O_R_W_Marine_LAT_02_F",
            "SpecLib_O_R_W_Marine_AA_F",
            "SpecLib_O_R_W_Marine_M_F{"
        };
        weapons[] = {};
        requiredVersion = REQUIRED_VERSION;
        requiredAddons[] = {"cba_main",
                            "ace_main",
                            "speclib_units_r_marines_summer"};
        author = "Reidond";
        VERSION_CONFIG;
    };
};

class CBA_Extended_EventHandlers;

#include "CfgEventHandlers.hpp"
#include "CfgEditorSubcategories.hpp"
#include "CfgVehicles.hpp"
