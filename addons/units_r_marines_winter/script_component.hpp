#define COMPONENT units_r_marines_winter
#include "\z\speclib\addons\main\script_mod.hpp"

// #define DEBUG_MODE_FULL
// #define DISABLE_COMPILE_CACHE

#ifdef DEBUG_ENABLED_UNITS_R_MARINES_WINTER
#define DEBUG_MODE_FULL
#endif
#ifdef DEBUG_SETTINGS_UNITS_R_MARINES_WINTER
#define DEBUG_SETTINGS DEBUG_SETTINGS_UNITS_R_MARINES_WINTER
#endif

#include "\z\speclib\addons\main\script_macros.hpp"

#define GRENADE_LAUNCHER_ROUNDS mag_11("rhs_VOG25")

#define GRENADES mag_2("rhs_mag_rgd5"), mag_2("rhs_mag_f1")

#define GRENADES_MEDIC mag_2("rhs_mag_rgd5"), mag_2("rhs_mag_rdg2_white")

#define ACE_ITEMS                                                              \
    "ACE_morphine", "ACE_tourniquet", "ACE_fieldDressing",                     \
        "ACE_fieldDressing", "ACE_packingBandage", "ACE_packingBandage",       \
        "ACE_elasticBandage", "ACE_elasticBandage", "ACE_quikclot",            \
        "ACE_quikclot", "ACE_EarPlugs"

#define ITEMS ACE_ITEMS

#define LINKED_ITEMS "ItemMap", "ItemCompass", "ItemWatch", "G_LIB_GER_Gloves3", "ItemRadio"

#define LINKED_ITEMS_CREW LINKED_ITEMS, "ItemRadio"

#define LINKED_ITEMS_OFFICER                                                   \
    "ItemMap", "ItemCompass", "ItemWatch", "ItemRadio", "ItemGPS",             \
        "G_LIB_GER_Gloves3"

#define LINKED_ITEMS_TL                                                        \
    "ItemMap", "ItemCompass", "ItemWatch", "ItemRadio", "ItemGPS",             \
        "LIB_GER_Gloves3"

#define DEFAULT_UNIT_WEAPONS "Throw", "Put"

#define UNIT_ENTRIES                                                           \
    author = CSTRING(SpecLibTeam);                                             \
    dlc = "speclib";                                                           \
    scope = 2;                                                                 \
    scopeCurator = 2;                                                          \
    faction = "rhs_faction_vmf";                                               \
    editorSubcategory = "SpecLib_EdSubcat_R_Marines_Winter";                   \
    selectionClan = "";                                                        \
    class EventHandlers {                                                      \
        class CBA_Extended_EventHandlers : CBA_Extended_EventHandlers {};      \
    };                                                                         \
    headgearList[] = {}
