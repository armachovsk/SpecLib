class CfgVehicles {
#include "include\a3.hpp"
#include "include\cup.hpp"
#include "include\rds.hpp"
#include "include\rhs.hpp"

    class SpecLib_BRDM2_Taki_Gue : rhsgref_BRDM2 {
        PREVIEW(SpecLib_BRDM2_Taki_Gue);
        faction = "SpecLib_Faction_Taki_Locals";
        crew = "SpecLib_i_tk_gue_soldier_akm";
        rhs_decalParameters[] = {};
        typicalCargo[] = {"SpecLib_i_tk_gue_soldier_akm",
                          "SpecLib_i_tk_gue_soldier_akms"};
        class textureSources {
            class olive {
                displayName = "Green";
                author = "$STR_RHS_AUTHOR_FULL";
                textures[] = {"\rhsgref\addons\rhsgref_a2port_"
                              "armor\brdm2\data\brdm2_ru_01_co.paa",
                              "\rhsgref\addons\rhsgref_a2port_"
                              "armor\brdm2\data\brdm2_02_co.paa",
                              "\rhsgref\addons\rhsgref_a2port_"
                              "armor\brdm2\data\zbik_04_co.paa"};
                factions[] = {"SpecLib_Faction_Taki_Locals"};
            };
        };
        textureList[] = {};
        rhs_randomizedHabar[] = {};
        class Attributes {
            class ObjectTexture {
                control = "ObjectTexture";
                data = "ObjectTexture";
                displayName = "Skin";
                tooltip = "Texture and material set applied on the object.";
            };
            class rhs_decalNumber_type {
                displayName = "Define font type of plate number";
                tooltip = "Define kind of font that will be drawn on vehicle.";
                property = "rhs_decalNumber_type";
                control = "Combo";
                expression =
                    "_this setVariable ['%s', _value];[_this,[['Number', [3,4,5], _value]]] call rhs_fnc_decalsInit";
                defaultValue = 0;
                typeName = "STRING";
                class values {
                    class Default {
                        name = "Default";
                        value = "Default";
                        defaultValue = "Default";
                    };
                };
            };
            class rhs_decalNumber {
                collapsed = 1;
                displayName = "Set side number";
                tooltip =
                    "Set side number. 4 numbers are required. Type 0 to hide numbers complety & leave at -1 to get random number";
                property = "rhs_decalNumber";
                control = "Edit";
                validate = "Number";
                defaultValue = "0";
                expression =
                    "if(_value >= 0)then{if(_value == 0)then{{[_this setobjectTexture [_x,'a3\data_f\clear_empty.paa']]}foreach [3,4,5]}else{[_this, [['Number', [3,4,5], _this getVariable ['rhs_decalNumber_type','CDF'], _value] ] ] call rhs_fnc_decalsInit}};";
            };
            class rhs_decalPlatoon_type {
                displayName = "Define platoon symbol type";
                tooltip = "Decal type";
                property = "rhs_decalPlatoon_type";
                control = "Combo";
                expression = "_this setVariable ['%s', _value];";
                defaultValue = "0";
                typeName = "STRING";
                class values {
                    class Platoon {
                        name = "Platoon";
                        value = "Platoon";
                        defaultValue = "0";
                    };
                };
            };
            class rhs_decalPlatoon {
                displayName = "Set platoon symbol";
                tooltip =
                    "Set platoon symbol located on right & rear of vehicles. Usually used for platoon symbols. -1 leaves current symbol & 0 clears decal.";
                property = "rhs_decalPlatoon";
                control = "Edit";
                validate = "none";
                defaultValue = "0";
                expression =
                    "if(parseNumber _value >= 0)then{ [_this, [ [ 'Label', [6],  _this getVariable ['rhs_decalPlatoon_type','Platoon'],call compile _value] ] ] call rhs_fnc_decalsInit};";
            };
            class big_numbers_hide {
                displayName = "hide big decal numbers";
                property = "big_numbers_hide";
                control = "CheckboxNumber";
                expression = "_this animate ['%s',_value,true]";
                defaultValue = "0";
            };
            class small_numbers_hide : big_numbers_hide {
                displayName = "hide small decal numbers";
                property = "small_numbers_hide";
            };
            class driverViewHatch : big_numbers_hide {
                displayName = "Open driver view hatch";
                property = "driverViewHatch";
                expression = "_this animateDoor ['%s',_value,true]";
            };
            class commanderViewHatch : driverViewHatch {
                displayName = "Open commander view hatch";
                property = "commanderViewHatch";
            };
        };
        side = 2;
        author = "$STR_RHS_AUTHOR_FULL";
        scope = 2;
        scopeCurator = 2;
        hiddenselectionstextures[] = {
            "\rhsgref\addons\rhsgref_a2port_armor\brdm2\data\brdm2_ru_01_co.paa",
            "\rhsgref\addons\rhsgref_a2port_armor\brdm2\data\brdm2_02_co.paa",
            "\rhsgref\addons\rhsgref_a2port_armor\brdm2\data\zbik_04_co.paa"};
 		class TransportWeapons
		{
            class _xx_rhs_weap_fim92 
			{
                weapon = "rhs_weap_fim92";
                count = 1;
            };
		};
		class TransportItems
		{
			class _xx_FirstAidKit
			{
				name = "FirstAidKit";
				count = 4;
			};
		};
		class TransportMagazines
		{
			class _xx_rhs_mag_rgd5
			{
				magazine = "rhs_mag_rgd5";
				count = 10;
			};
			class _xx_rhs_mag_rdg2_white
			{
				magazine = "rhs_mag_rdg2_white";
				count = 2;
			};
			class _xx_SPEC_30Rnd_762x39_AK47_M
			{
				magazine = "SPEC_30Rnd_762x39_AK47_M";
				count = 20;
			};
			class _xx_rhs_100Rnd_762x54mmR
			{
				magazine = "rhs_100Rnd_762x54mmR";
				count = 3;
			};
            class _xx_rhs_fim92_mag 
			{
                magazine = "rhs_fim92_mag";
                count = 2;
			};
			class _xx_rhs_mag_20Rnd_762x51_m80_fnfal
			{
				magazine = "rhs_mag_20Rnd_762x51_m80_fnfal";
				count = 20;
			};
		};
		class TransportBackpacks
		{
			class _xx_YuE_RD54old
			{
				backpack = "YuE_RD54old";
				count = 3;
			};
		};
	};
    class SpecLib_BRDM2_ATGM_Taki_Gue : rhsgref_BRDM2_ATGM {
        PREVIEW(SpecLib_BRDM2_ATGM_Taki_Gue);
        faction = "SpecLib_Faction_Taki_Locals";
        crew = "SpecLib_i_tk_gue_soldier_akm";
        rhs_decalParameters[] = {};
        rhs_randomizedHabar[] = {};
        class Attributes {
            class ObjectTexture {
                control = "ObjectTexture";
                data = "ObjectTexture";
                displayName = "Skin";
                tooltip = "Texture and material set applied on the object.";
            };
            class rhs_decalNumber_type {
                displayName = "Define font type of plate number";
                tooltip = "Define kind of font that will be drawn on vehicle.";
                property = "rhs_decalNumber_type";
                control = "Combo";
                expression =
                    "_this setVariable ['%s', _value];[_this,[['Number', [3,4,5], _value]]] call rhs_fnc_decalsInit";
                defaultValue = 0;
                typeName = "STRING";
                class values {
                    class Default {
                        name = "Default";
                        value = "Default";
                        defaultValue = "Default";
                    };
                };
            };
            class rhs_decalNumber {
                collapsed = 1;
                displayName = "Set side number";
                tooltip =
                    "Set side number. 4 numbers are required. Type 0 to hide numbers complety & leave at -1 to get random number";
                property = "rhs_decalNumber";
                control = "Edit";
                validate = "Number";
                defaultValue = "0";
                expression =
                    "if(_value >= 0)then{if(_value == 0)then{{[_this setobjectTexture [_x,'a3\data_f\clear_empty.paa']]}foreach [3,4,5]}else{[_this, [['Number', [3,4,5], _this getVariable ['rhs_decalNumber_type','CDF'], _value] ] ] call rhs_fnc_decalsInit}};";
            };
            class rhs_decalPlatoon_type {
                displayName = "Define platoon symbol type";
                tooltip = "Decal type";
                property = "rhs_decalPlatoon_type";
                control = "Combo";
                expression = "_this setVariable ['%s', _value];";
                defaultValue = "0";
                typeName = "STRING";
                class values {
                    class Platoon {
                        name = "Platoon";
                        value = "Platoon";
                        defaultValue = "0";
                    };
                };
            };
            class rhs_decalPlatoon {
                displayName = "Set platoon symbol";
                tooltip =
                    "Set platoon symbol located on right & rear of vehicles. Usually used for platoon symbols. -1 leaves current symbol & 0 clears decal.";
                property = "rhs_decalPlatoon";
                control = "Edit";
                validate = "none";
                defaultValue = "0";
                expression =
                    "if(parseNumber _value >= 0)then{ [_this, [ [ 'Label', [6],  _this getVariable ['rhs_decalPlatoon_type','Platoon'],call compile _value] ] ] call rhs_fnc_decalsInit};";
            };
            class big_numbers_hide {
                displayName = "hide big decal numbers";
                property = "big_numbers_hide";
                control = "CheckboxNumber";
                expression = "_this animate ['%s',_value,true]";
                defaultValue = "0";
            };
            class small_numbers_hide : big_numbers_hide {
                displayName = "hide small decal numbers";
                property = "small_numbers_hide";
            };
            class driverViewHatch : big_numbers_hide {
                displayName = "Open driver view hatch";
                property = "driverViewHatch";
                expression = "_this animateDoor ['%s',_value,true]";
            };
            class commanderViewHatch : driverViewHatch {
                displayName = "Open commander view hatch";
                property = "commanderViewHatch";
            };
        };
        side = 2;
        author = "$STR_RHS_AUTHOR_FULL";
        scope = 2;
        scopeCurator = 2;
        hiddenselectionstextures[] = {
            "\rhsgref\addons\rhsgref_a2port_armor\brdm2\data\brdm2_atgm_ru_01_co.paa",
            "\rhsgref\addons\rhsgref_a2port_armor\brdm2\data\brdm2_02_co.paa",
            "\rhsgref\addons\rhsgref_a2port_armor\brdm2\data\zbik_04_co.paa"};
		class TransportWeapons
		{
            class _xx_rhs_weap_fim92 
			{
                weapon = "rhs_weap_fim92";
                count = 1;
            };
		};
		class TransportItems
		{
			class _xx_FirstAidKit
			{
				name = "FirstAidKit";
				count = 4;
			};
		};
		class TransportMagazines
		{
			class _xx_rhs_mag_rgd5
			{
				magazine = "rhs_mag_rgd5";
				count = 10;
			};
			class _xx_rhs_mag_rdg2_white
			{
				magazine = "rhs_mag_rdg2_white";
				count = 2;
			};
			class _xx_SPEC_30Rnd_762x39_AK47_M
			{
				magazine = "SPEC_30Rnd_762x39_AK47_M";
				count = 20;
			};
			class _xx_rhs_100Rnd_762x54mmR
			{
				magazine = "rhs_100Rnd_762x54mmR";
				count = 3;
			};
            class _xx_rhs_fim92_mag 
			{
                magazine = "rhs_fim92_mag";
                count = 2;
			};
			class _xx_rhs_mag_20Rnd_762x51_m80_fnfal
			{
				magazine = "rhs_mag_20Rnd_762x51_m80_fnfal";
				count = 20;
			};
		};
		class TransportBackpacks
		{
			class _xx_YuE_RD54old
			{
				backpack = "YuE_RD54old";
				count = 3;
			};
		};
    };
    class SpecLib_BRDM2_HQ_Taki_Gue : rhsgref_BRDM2_HQ {
        PREVIEW(SpecLib_BRDM2_HQ_Taki_Gue);
        faction = "SpecLib_Faction_Taki_Locals";
        crew = "SpecLib_i_tk_gue_soldier_akm";
        rhs_decalParameters[] = {};
        rhs_randomizedHabar[] = {};
        class Attributes {
            class ObjectTexture {
                control = "ObjectTexture";
                data = "ObjectTexture";
                displayName = "Skin";
                tooltip = "Texture and material set applied on the object.";
            };
            class rhs_decalNumber_type {
                displayName = "Define font type of plate number";
                tooltip = "Define kind of font that will be drawn on vehicle.";
                property = "rhs_decalNumber_type";
                control = "Combo";
                expression =
                    "_this setVariable ['%s', _value];[_this,[['Number', [3,4,5], _value]]] call rhs_fnc_decalsInit";
                defaultValue = 0;
                typeName = "STRING";
                class values {
                    class Default {
                        name = "Default";
                        value = "Default";
                        defaultValue = "Default";
                    };
                };
            };
            class rhs_decalNumber {
                collapsed = 1;
                displayName = "Set side number";
                tooltip =
                    "Set side number. 4 numbers are required. Type 0 to hide numbers complety & leave at -1 to get random number";
                property = "rhs_decalNumber";
                control = "Edit";
                validate = "Number";
                defaultValue = "0";
                expression =
                    "if(_value >= 0)then{if(_value == 0)then{{[_this setobjectTexture [_x,'a3\data_f\clear_empty.paa']]}foreach [3,4,5]}else{[_this, [['Number', [3,4,5], _this getVariable ['rhs_decalNumber_type','CDF'], _value] ] ] call rhs_fnc_decalsInit}};";
            };
            class rhs_decalPlatoon_type {
                displayName = "Define platoon symbol type";
                tooltip = "Decal type";
                property = "rhs_decalPlatoon_type";
                control = "Combo";
                expression = "_this setVariable ['%s', _value];";
                defaultValue = "0";
                typeName = "STRING";
                class values {
                    class Platoon {
                        name = "Platoon";
                        value = "Platoon";
                        defaultValue = "0";
                    };
                };
            };
            class rhs_decalPlatoon {
                displayName = "Set platoon symbol";
                tooltip =
                    "Set platoon symbol located on right & rear of vehicles. Usually used for platoon symbols. -1 leaves current symbol & 0 clears decal.";
                property = "rhs_decalPlatoon";
                control = "Edit";
                validate = "none";
                defaultValue = "0";
                expression =
                    "if(parseNumber _value >= 0)then{ [_this, [ [ 'Label', [6],  _this getVariable ['rhs_decalPlatoon_type','Platoon'],call compile _value] ] ] call rhs_fnc_decalsInit};";
            };
            class big_numbers_hide {
                displayName = "hide big decal numbers";
                property = "big_numbers_hide";
                control = "CheckboxNumber";
                expression = "_this animate ['%s',_value,true]";
                defaultValue = "0";
            };
            class small_numbers_hide : big_numbers_hide {
                displayName = "hide small decal numbers";
                property = "small_numbers_hide";
            };
            class driverViewHatch : big_numbers_hide {
                displayName = "Open driver view hatch";
                property = "driverViewHatch";
                expression = "_this animateDoor ['%s',_value,true]";
            };
            class commanderViewHatch : driverViewHatch {
                displayName = "Open commander view hatch";
                property = "commanderViewHatch";
            };
        };
        side = 2;
        author = "$STR_RHS_AUTHOR_FULL";
        scope = 2;
        scopeCurator = 2;
        hiddenselectionstextures[] = {
            "\rhsgref\addons\rhsgref_a2port_armor\brdm2\data\brdm2_ru_01_co.paa",
            "\rhsgref\addons\rhsgref_a2port_armor\brdm2\data\brdm2_02_co.paa",
            "\rhsgref\addons\rhsgref_a2port_armor\brdm2\data\zbik_04_co.paa"};
		class TransportWeapons
		{
            class _xx_rhs_weap_fim92 
			{
                weapon = "rhs_weap_fim92";
                count = 1;
            };
		};
		class TransportItems
		{
			class _xx_FirstAidKit
			{
				name = "FirstAidKit";
				count = 4;
			};
		};
		class TransportMagazines
		{
			class _xx_rhs_mag_rgd5
			{
				magazine = "rhs_mag_rgd5";
				count = 10;
			};
			class _xx_rhs_mag_rdg2_white
			{
				magazine = "rhs_mag_rdg2_white";
				count = 2;
			};
			class _xx_SPEC_30Rnd_762x39_AK47_M
			{
				magazine = "SPEC_30Rnd_762x39_AK47_M";
				count = 20;
			};
			class _xx_rhs_100Rnd_762x54mmR
			{
				magazine = "rhs_100Rnd_762x54mmR";
				count = 3;
			};
            class _xx_rhs_fim92_mag 
			{
                magazine = "rhs_fim92_mag";
                count = 2;
			};
			class _xx_rhs_mag_20Rnd_762x51_m80_fnfal
			{
				magazine = "rhs_mag_20Rnd_762x51_m80_fnfal";
				count = 20;
			};
		};
		class TransportBackpacks
		{
			class _xx_YuE_RD54old
			{
				backpack = "YuE_RD54old";
				count = 3;
			};
		};
    };

    class SpecLib_btr60_Taki_Gue : rhs_btr60_msv {
        PREVIEW(SpecLib_btr60_Taki_Gue);
        side = 2;
        author = "$STR_RHS_AUTHOR_FULL";
        scope = 2;
        scopeCurator = 2;
        faction = "SpecLib_Faction_Taki_Locals";
        crew = "SpecLib_i_tk_gue_crewman";
        rhs_decalParameters[] = {};
        class textureSources {
            class standard {
                displayName = "Standard";
                textures[] = {
                    "rhsafrf\addons\rhs_a2port_armor\Data\btr60_body_co.paa",
                    "rhsafrf\addons\rhs_a2port_armor\data\btr60_details_co.paa"};
                factions[] = {"SpecLib_Faction_Taki_Locals"};
            };
            delete 3tone;
        };
        textureList[] = {};
        class Attributes {
            class ObjectTexture {
                control = "ObjectTexture";
                data = "ObjectTexture";
                displayName = "Skin";
                tooltip = "Texture and material set applied on the object.";
            };
            class rhs_decalNumber_type {
                displayName = "Define font type of side number (3 digits)";
                tooltip =
                    "Define kind of font that will be drawn on vehicle. 3 digits";
                property = "rhs_decalNumber_type";
                control = "Combo";
                expression =
                    "if(_value != 'NoChange')then{ _this setVariable ['%s', _value];[_this,[['Number', cBTR3NumberPlaces, _value]]] call rhs_fnc_decalsInit}";
                defaultValue = 0;
                typeName = "STRING";
                class values {
                    class NoChange {
                        name = "Default defined";
                        value = "NoChange";
                        defaultValue = "NoChange";
                    };
                };
            };
            class rhs_decalNumber {
                collapsed = 1;
                displayName = "Set side number (3 digits)";
                tooltip =
                    "Set side number. 4 numbers are required. Type 0 to hide numbers complety & leave at -1 to get random number";
                property = "rhs_decalNumber";
                control = "Edit";
                validate = "Number";
                typeName = "Number";
                defaultValue = "0";
                expression =
                    "if( _value >= 0)then{if( _value == 0)then{{[_this setobjectTexture [_x,'a3\data_f\clear_empty.paa']]}foreach cBTR3NumberPlaces}else{[_this, [['Number', cBTR3NumberPlaces, _this getVariable ['rhs_decalNumber_type','Default'], _value] ] ] call rhs_fnc_decalsInit}};";
            };
            class rhs_decalNumber_type2 : rhs_decalNumber_type {
                displayName = "Define font type of side number (2 digits)";
                tooltip =
                    "Define kind of font that will be drawn on vehicle. 2 digits";
                property = "rhs_decalNumber_type2";
                expression =
                    "if(_value != 'NoChange')then{ _this setVariable ['%s', _value];[_this,[['Number', cBTR2NumberPlaces, _value]]] call rhs_fnc_decalsInit}";
            };
            class rhs_decalNumber2 : rhs_decalNumber {
                displayName = "Set side number (2 digits)";
                tooltip = "Set side number. 2 numbers are required. Hides on 0";
                property = "rhs_decalNumber2";
                expression =
                    "if( _value >= 0)then{if( _value == 0)then{{[_this setobjectTexture [_x,'a3\data_f\clear_empty.paa']]}foreach cBTR2NumberPlaces}else{[_this, [['Number', cBTR2NumberPlaces, _this getVariable ['rhs_decalNumber_type2','Default'], _value] ] ] call rhs_fnc_decalsInit}};";
            };
            class rhs_decalNumber_type3 : rhs_decalNumber_type {
                displayName = "Define font type of side number (4 digits)";
                tooltip =
                    "Define kind of font that will be drawn on vehicle. 4 digits";
                property = "rhs_decalNumber_type3";
                expression =
                    "if(_value != 'NoChange')then{ _this setVariable ['%s', _value];[_this,[['Number', cBTR4NumberPlaces, _value]]] call rhs_fnc_decalsInit}";
            };
            class rhs_decalNumber3 : rhs_decalNumber {
                displayName = "Set side number (4 digits)";
                tooltip = "Set side number. 4 numbers are required. Hides on 0";
                property = "rhs_decalNumber3";
                expression =
                    "if( _value >= 0)then{if( _value == 0)then{{[_this setobjectTexture [_x,'a3\data_f\clear_empty.paa']]}foreach cBTR4NumberPlaces}else{[_this, [['Number', cBTR4NumberPlaces, _this getVariable ['rhs_decalNumber_type3','Default'], _value] ] ] call rhs_fnc_decalsInit}};";
            };
            class rhs_decalNumber_type4 : rhs_decalNumber_type {
                displayName = "Define font type of side number (4 digits CDF)";
                tooltip =
                    "Define kind of font that will be drawn on vehicle. 4 digits, CDF style numbering";
                property = "rhs_decalNumber_type4";
                expression =
                    "if(_value != 'NoChange')then{ _this setVariable ['%s', _value];[_this,[['Number', cBTRCDF4NumberPlaces, _value]]] call rhs_fnc_decalsInit}";
            };
            class rhs_decalNumber4 : rhs_decalNumber {
                displayName = "Set side number (4 digits CDF)";
                tooltip = "Set side number. 4 numbers are required. Hides on 0";
                property = "rhs_decalNumber4";
                expression =
                    "if( _value >= 0)then{if( _value == 0)then{{[_this setobjectTexture [_x,'a3\data_f\clear_empty.paa']]}foreach cBTRCDF4NumberPlaces}else{[_this, [['Number', cBTRCDF4NumberPlaces, _this getVariable ['rhs_decalNumber_type4','CDF'], _value] ] ] call rhs_fnc_decalsInit}};";
            };
            class rhs_decalPlatoon_type {
                displayName = "Define platoon symbol type";
                tooltip = "Decal type";
                property = "rhs_decalPlatoon_type";
                control = "Combo";
                expression = "_this setVariable ['%s', _value];";
                defaultValue = "0";
                typeName = "STRING";
                class values {
                    class Platoon {
                        name = "Platoon";
                        value = "Platoon";
                        defaultValue = "0";
                    };
                };
            };
            class rhs_decalPlatoon {
                displayName = "Set platoon symbol";
                tooltip =
                    "Set platoon symbol located on all 4 sides of vehicle. Usually used for platoon symbols. -1 leaves current symbol & 0 clears decal.";
                property = "rhs_decalPlatoon";
                control = "Edit";
                validate = "none";
                defaultValue = "0";
                expression =
                    "if(parseNumber _value >= 0)then{ [_this, [ [ 'Label', cBTRPlnSymPlaces,  _this getVariable ['rhs_decalPlatoon_type','Platoon'],call compile _value] ] ] call rhs_fnc_decalsInit};";
            };
            class rhs_decalArmy_type : rhs_decalPlatoon_type {
                displayName = "Define back army symbol type";
                property = "rhs_decalArmy_type";
                class values : values {
                    class Platoon : Platoon {};
                };
            };
            class rhs_decalArmy : rhs_decalPlatoon {
                displayName = "Set back army symbol";
                tooltip =
                    "Define symbol located on right back side of vehicle hull. Usually used for army symbols. -1 leaves current symbol & 0 clears decal.";
                property = "rhs_decalArmy";
                expression =
                    "if(parseNumber _value >= 0)then{ [_this, [ [ 'Label', cBTRBackArmSymPlaces,  _this getVariable ['rhs_decalArmy_type','Army'],call compile _value] ] ] call rhs_fnc_decalsInit};";
            };
            class rhs_decalRightTurret_type : rhs_decalArmy_type {
                displayName = "Define right turret side symbol type";
                property = "rhs_decalRightTurret_type";
                class values : values {
                    class Platoon : Platoon {};
                };
            };
            class rhs_decalRightTurret : rhs_decalPlatoon {
                displayName = "Set right turret side symbol";
                tooltip =
                    "Define right turret side symbol. Usually used for honor symbols. -1 leaves current symbol & 0 clears decal.";
                property = "rhs_decalRightTurret";
                expression =
                    "if(parseNumber _value >= 0)then{ [_this, [ [ 'Label', cBTRRightGvardSymPlaces,  _this getVariable ['rhs_decalRightTurret_type','Honor'],call compile _value] ] ] call rhs_fnc_decalsInit};";
            };
            class rhs_decalLeftTurret_type : rhs_decalRightTurret_type {
                displayName = "Define left turret symbol type";
                property = "rhs_decalLeftTurret_type";
            };
            class rhs_decalLeftTurret : rhs_decalRightTurret {
                displayName = "Set left turret symbol";
                tooltip =
                    "Define symbol located on left side of vehicle turret. Usually used for honor symbols. -1 leaves current symbol & 0 clears decal.";
                property = "rhs_decalLeftTurret";
                expression =
                    "if(parseNumber _value >= 0)then{ [_this, [ [ 'Label', cBTRLeftGvardSymPlaces,  _this getVariable ['rhs_decalLeftTurret_type','Honor'],call compile _value] ] ] call rhs_fnc_decalsInit};";
            };
            class rhs_decalFront_type : rhs_decalArmy_type {
                displayName = "Define front side roundels type";
                property = "rhs_decalFront_type";
            };
            class rhs_decalFront : rhs_decalPlatoon {
                displayName = "Set front side roundels";
                tooltip =
                    "Define symbol located on front of vehicle hull. Usually used for OMON symbols. -1 leaves current symbol & 0 clears decal.";
                property = "rhs_decalFront";
                expression =
                    "if(parseNumber _value >= 0)then{ [_this, [ [ 'Label', cBTROMONSymbolPlaces,  _this getVariable ['rhs_decalFront_type','Army'],call compile _value] ] ] call rhs_fnc_decalsInit};";
            };
            class rhs_decalLowerFront_type : rhs_decalArmy_type {
                displayName = "Define front hull symbol type";
                property = "rhs_decalLowerFront_type";
            };
            class rhs_decalLowerFront : rhs_decalPlatoon {
                displayName = "Set front hull symbol";
                tooltip =
                    "Define symbol located on front of vehicle hull. Usually used for army symbols. -1 leaves current symbol & 0 clears decal.";
                property = "rhs_decalLowerFront";
                expression =
                    "if(parseNumber _value >= 0)then{ [_this, [ [ 'Label', cBTRFrontPlatePlaces,  _this getVariable ['rhs_decalLowerFront_type','Army'],call compile _value] ] ] call rhs_fnc_decalsInit};";
            };
            class rhs_decalFlag_type : rhs_decalArmy_type {
                displayName = "Define flag/side marking type";
                property = "rhs_decalFlag_type";
            };
            class rhs_decalFlag : rhs_decalPlatoon {
                displayName = "Set flag/side marking";
                tooltip =
                    "Define symbol located on front of vehicle hull. Usually used for navy flag symbols. -1 leaves current symbol & 0 clears decal.";
                property = "rhs_decalFlag";
                expression =
                    "if(parseNumber _value >= 0)then{ [_this, [ [ 'Label', cBTRFlagSymPlaces,  _this getVariable ['rhs_decalFlag_type','Army'],call compile _value] ] ] call rhs_fnc_decalsInit};";
            };
            class rhs_decalVV_type : rhs_decalPlatoon_type {
                displayName = "Define VV letter type";
                property = "rhs_decalVV_type";
            };
            class rhs_decalVV : rhs_decalPlatoon {
                displayName = "Set VV letter";
                tooltip =
                    "Define symbol next to VV 2 digits numbering system decal. Usually used for VV T letter for numbering system (code 13). -1 leaves current symbol & 0 clears decal.";
                property = "rhs_decalVV";
                expression =
                    "if(parseNumber _value >= 0)then{ [_this, [ [ 'Label', cBTRVVLetterSymPlaces,  _this getVariable ['rhs_decalVV_type','Platoon'],call compile _value] ] ] call rhs_fnc_decalsInit};";
            };
            class crate_l1_unhide {
                displayName = "hide l1 crate";
                property = "crate_l1_unhide";
                control = "CheckboxNumber";
                expression = "[_this,_value,'%s'] call rhs_fnc_setHabarEden";
                defaultValue = "1";
            };
            class crate_l2_unhide : crate_l1_unhide {
                displayName = "Hide l2 crate";
                property = "crate_l2_unhide";
                defaultValue = "1";
            };
            class crate_l3_unhide : crate_l1_unhide {
                displayName = "Hide l3 crate";
                property = "crate_l3_unhide";
                defaultValue = "1";
            };
            class crate_l4_unhide : crate_l1_unhide {
                displayName = "Hide l4 crate";
                property = "crate_l4_unhide";
                defaultValue = "0";
            };
            class crate_r1_unhide : crate_l1_unhide {
                displayName = "Hide r1 crate";
                property = "crate_r1_unhide";
                defaultValue = "1";
            };
            class crate_r2_unhide : crate_l1_unhide {
                displayName = "Hide r2 crate";
                property = "crate_r2_unhide";
                defaultValue = "1";
            };
            class crate_r3_unhide : crate_l1_unhide {
                displayName = "Hide r3 crate";
                property = "crate_r3_unhide";
                defaultValue = "0";
            };
            class crate_r4_unhide : crate_l1_unhide {
                displayName = "Hide r4 crate";
                property = "crate_r4_unhide";
                defaultValue = "0";
            };
            class water_1_unhide : crate_l1_unhide {
                displayName = "Hide water tank 1";
                property = "water_1_unhide";
                defaultValue = "0";
            };
            class water_2_unhide : crate_l1_unhide {
                displayName = "Hide water tank 2";
                property = "water_2_unhide";
                defaultValue = "0";
            };
            class wheel_1_unhide : crate_l1_unhide {
                displayName = "Hide spare wheel 1";
                property = "wheel_1_unhide";
                defaultValue = "1";
            };
            class wheel_2_unhide : crate_l1_unhide {
                displayName = "Hide spare wheel 2";
                property = "wheel_2_unhide";
                defaultValue = "0";
            };
            class rhs_disableHabar : crate_l1_unhide {
                displayName = "Disable habar";
                property = "rhs_disableHabar";
                expression =
                    "[_this,_value,'%s',_value] call rhs_fnc_setHabarEden";
                defaultValue = "0";
            };
            class dHatch : rhs_disableHabar {
                displayName = "Open driver top hatch";
                property = "dHatch";
                expression = "_this animateDoor ['%s',_value,true]";
            };
            class cHatch : dHatch {
                displayName = "Open commander top hatch";
                property = "cHatch";
            };
            class l_door : dHatch {
                displayName = "Open left door";
                property = "l_door";
            };
            class r_door : dHatch {
                displayName = "Open right door";
                property = "r_door";
            };
            class windows : dHatch {
                displayName = "Open firing ports";
                property = "windows";
            };
            class t_door_1 : dHatch {
                displayName = "Open top hatch 1";
                property = "t_door_1";
            };
            class t_door_2 : dHatch {
                displayName = "Open top hatch 2";
                property = "t_door_2";
            };
            class engineHatches : dHatch {
                displayName = "Open engine hatches";
                property = "engineHatches";
            };
            class propDoor : dHatch {
                displayName = "Open propulsion cover";
                property = "propDoor";
            };
            class driverViewHatch : dHatch {
                displayName = "Open driver view hatch";
                property = "driverViewHatch";
            };
            class commanderViewHatch : dHatch {
                displayName = "Open commander view hatch";
                property = "commanderViewHatch";
            };
            class rhs_externalMount : rhs_disableHabar {
                displayName = "Disable external mount";
                property = "rhs_externalMount";
                control = "Checkbox";
                expression = "[_this,_value] call rhs_fnc_lockTop";
            };
        };
        hiddenSelections[] = {"camo1",  "camo2", "camo3", "i1", "i2", "i3",
                              "i4",     "i5",    "i6",    "i7", "n1", "n2",
                              "n3",     "n4",    "n5",    "n6", "n7", "n8",
                              "n9",     "n10",   "n11",   "i8", "i9", "i10",
                              "cover1", "cover2"};
        hiddenSelectionsTextures[] = {
            "rhsafrf\addons\rhs_a2port_armor\Data\btr60_body_co.paa",
            "rhsafrf\addons\rhs_a2port_armor\data\btr60_details_co.paa",
            "\rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\rhs_btr70\habar\data\sa_gear_02_co.paa",
            "rhsafrf\addons\rhs_btr70\habar\data\sa_gear_02_co.paa"};
        rhs_randomizedHabar[] = {};
 		class TransportWeapons
		{
			class _xx_CUP_launch_RPG18
			{
				weapon = "CUP_launch_RPG18";
				count = 2;
			};
			class _xx_launch_RPG7_F
			{
				weapon = "launch_RPG7_F";
				count = 1;
			};
		};
		class TransportItems
		{
			class _xx_FirstAidKit
			{
				name = "FirstAidKit";
				count = 10;
			};
		};
		class TransportMagazines
		{
			class _xx_rhs_mag_rgd5
			{
				magazine = "rhs_mag_rgd5";
				count = 10;
			};
			class _xx_rhs_mag_rdg2_white
			{
				magazine = "rhs_mag_rdg2_white";
				count = 2;
			};
			class _xx_SPEC_30Rnd_762x39_AK47_M
			{
				magazine = "SPEC_30Rnd_762x39_AK47_M";
				count = 20;
			};
			class _xx_rhs_rpg7_PG7V_mag
			{
				magazine = "rhs_rpg7_PG7V_mag";
				count = 3;
			};
			class _xx_rhs_100Rnd_762x54mmR
			{
				magazine = "rhs_100Rnd_762x54mmR";
				count = 3;
			};
			class _xx_rhs_mag_20Rnd_762x51_m80_fnfal
			{
				magazine = "rhs_mag_20Rnd_762x51_m80_fnfal";
				count = 20;
			};
		};
		class TransportBackpacks
		{
			class _xx_YuE_RD54old
			{
				backpack = "YuE_RD54old";
				count = 2;
			};
			class _xx_rhs_rpg_empty
			{
				backpack = "rhs_rpg_empty";
				count = 1;
			};
		};
    };
    class SpecLib_Taki_Gue_2b14 : rhsgref_nat_2b14 {
        PREVIEW(SpecLib_Taki_Gue_2b14);
        scope = 2;
        author = "RHS (A2 port)";
        side = 2;
        faction = "SpecLib_Faction_Taki_Locals";
        crew = "SpecLib_i_tk_gue_soldier_akm";
        rhs_decalParameters[] = {};
        rhs_randomizedHabar[] = {};
    };

    class SpecLib_Taki_Gue_uaz : rhsgref_nat_uaz {
        PREVIEW(SpecLib_Taki_Gue_uaz);
        scope = 2;
        author = "RHS (A2 port)";
        side = 2;
        faction = "SpecLib_Faction_Taki_Locals";
        crew = "SpecLib_i_tk_gue_soldier_akm";
        accuracy = 0.5;
        rhs_decalParameters[] = {};
        rhs_randomizedHabar[] = {};
        hiddenSelections[] = {"camo1", "camo2g", "n1", "n2", "n3",      "n4",
                              "i1",    "i2",     "i3", "i4", "numplate"};
        hiddenSelectionsTextures[] = {
            "rhsafrf\addons\rhs_a2port_car\uaz\data\uaz_main_ind_co.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa"};
		class TransportItems
		{
			class _xx_FirstAidKit
			{
				name = "FirstAidKit";
				count = 4;
			};
		};
		class TransportBackpacks{};
    };

    class SpecLib_Taki_Gue_uaz_open : rhsgref_nat_uaz_open {
        PREVIEW(SpecLib_Taki_Gue_uaz_open);
        scope = 2;
        author = "RHS (A2 port)";
        side = 2;
        faction = "SpecLib_Faction_Taki_Locals";
        crew = "SpecLib_i_tk_gue_enfield";
        accuracy = 0.5;
        rhs_decalParameters[] = {};
        rhs_randomizedHabar[] = {};
        hiddenSelections[] = {"camo1", "camo2g", "n1", "n2", "n3",      "n4",
                              "i1",    "i2",     "i3", "i4", "numplate"};
        hiddenSelectionsTextures[] = {
            "rhsafrf\addons\rhs_a2port_car\uaz\data\uaz_main_ind_co.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa"};
		class TransportItems
		{
			class _xx_FirstAidKit
			{
				name = "FirstAidKit";
				count = 4;
			};
		};
		class TransportBackpacks{};
    };
    class SpecLib_Taki_Gue_uaz_dshkm : RHS_UAZ_DShKM_Base {
        PREVIEW(SpecLib_Taki_Gue_uaz_dshkm);
        scope = 2;
        author = "RHS (A2 port)";
        side = 2;
        faction = "SpecLib_Faction_Taki_Locals";
        crew = "SpecLib_i_tk_gue_soldier_akm";
        accuracy = 0.5;
        rhs_decalParameters[] = {};
        rhs_randomizedHabar[] = {};
        class AnimationSources : AnimationSources {
            class light_hide {
                source = "user";
                mass = 1;
                initPhase = 1;
                animPeriod = 1e-011;
                displayName = "hide light covers";
            };
        };
        class Turrets : Turrets {
            class MainTurret : MainTurret {
                gunnerType = "SpecLib_i_tk_gue_soldier_l1a1";
            };
            class CargoTurret_01 : CargoTurret_01 {};
        };
        hiddenSelections[] = {"camo1", "camo2g", "n1", "n2", "n3",      "n4",
                              "i1",    "i2",     "i3", "i4", "numplate"};
        hiddenSelectionsTextures[] = {
            "rhsafrf\addons\rhs_a2port_car\uaz\data\uaz_main_ind_co.paa",
            "rhsafrf\addons\rhs_a2port_car\UAZ\Data\uaz_mount_co.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa"};
		class TransportItems
		{
			class _xx_FirstAidKit
			{
				name = "FirstAidKit";
				count = 4;
			};
		};
		class TransportBackpacks{};
    };

    class SpecLib_Taki_Gue_uaz_ags : RHS_UAZ_AGS30_Base {
        PREVIEW(SpecLib_Taki_Gue_uaz_ags);
        scope = 2;
        author = "RHS (A2 port)";
        side = 2;
        faction = "SpecLib_Faction_Taki_Locals";
        crew = "SpecLib_i_tk_gue_soldier_l1a1";
        accuracy = 0.5;
        rhs_decalParameters[] = {};
        rhs_randomizedHabar[] = {};
        class AnimationSources : AnimationSources {
            class light_hide {
                source = "user";
                mass = 1;
                initPhase = 1;
                animPeriod = 1e-011;
                displayName = "hide light covers";
            };
        };
        class Turrets : Turrets {
            class MainTurret : MainTurret {
                gunnerType = "SpecLib_i_tk_gue_soldier_akm";
            };
            class CargoTurret_01 : CargoTurret_01 {};
        };
        hiddenSelections[] = {"camo1", "camo2g", "n1", "n2", "n3",      "n4",
                              "i1",    "i2",     "i3", "i4", "numplate"};
        hiddenSelectionsTextures[] = {
            "rhsafrf\addons\rhs_a2port_car\uaz\data\uaz_main_ind_co.paa",
            "rhsafrf\addons\rhs_a2port_car\UAZ\Data\uaz_mount_co.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa"};
		class TransportItems
		{
			class _xx_FirstAidKit
			{
				name = "FirstAidKit";
				count = 4;
			};
		};
		class TransportBackpacks{};
    };

    class SpecLib_Taki_Gue_uaz_spg9 : RHS_UAZ_SPG9_Base {
        PREVIEW(SpecLib_Taki_Gue_uaz_spg9);
        scope = 2;
        author = "RHS (A2 port)";
        side = 2;
        faction = "SpecLib_Faction_Taki_Locals";
        crew = "SpecLib_i_tk_gue_soldier_akm";
        accuracy = 0.5;
        rhs_decalParameters[] = {};
        rhs_randomizedHabar[] = {};
        class AnimationSources : AnimationSources {
            class light_hide {
                source = "user";
                mass = 1;
                initPhase = 1;
                animPeriod = 1e-011;
                displayName = "hide light covers";
            };
        };
        class Turrets : Turrets {
            class MainTurret : MainTurret {
                gunnerType = "SpecLib_i_tk_gue_soldier_l1a1";
            };
            class CargoTurret_01 : CargoTurret_01 {};
        };
        hiddenSelections[] = {"camo1", "camo2g", "n1", "n2", "n3",      "n4",
                              "i1",    "i2",     "i3", "i4", "numplate"};
        hiddenSelectionsTextures[] = {
            "rhsafrf\addons\rhs_a2port_car\uaz\data\uaz_main_ind_co.paa",
            "rhsafrf\addons\rhs_a2port_car\UAZ\Data\uaz_mount_co.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa"};
		class TransportItems
		{
			class _xx_FirstAidKit
			{
				name = "FirstAidKit";
				count = 4;
			};
		};
		class TransportBackpacks{};
    };

    class SpecLib_uh1h_Taki_Gue : rhs_uh1h_hidf {
        PREVIEW(SpecLib_uh1h_Taki_Gue);
        author = "$STR_RHS_AUTHOR_FULL";
        scope = 2;
        scopeCurator = 2;
        side = 2;
        rhs_randomizedHabar[] = {};
        faction = "SpecLib_Faction_Taki_Locals";
        displayName = "UH-1H";
        crew = "SpecLib_i_tk_gue_soldier_akm";
        typicalCargo[] = {"SpecLib_i_tk_gue_soldier_akms",
                          "SpecLib_i_tk_gue_soldier_akm",
                          "SpecLib_i_tk_gue_soldier_l1a1"};
        rhs_decalParameters[] = {};
		class TransportWeapons{};
		class TransportItems
		{
			class _xx_FirstAidKit
			{
				name = "FirstAidKit";
				count = 8;
			};
			class _xx_ToolKit
			{
				name = "ToolKit";
				count = 1;
			};
		};
		class TransportMagazines
		{
			class _xx_rhs_mag_rgd5
			{
				magazine = "rhs_mag_rgd5";
				count = 10;
			};
			class _xx_rhs_mag_rdg2_white
			{
				magazine = "rhs_mag_rdg2_white";
				count = 4;
			};
			class _xx_rhs_mag_nspn_red
			{
				magazine = "rhs_mag_nspn_red";
				count = 10;
			};
			class _xx_SPEC_30Rnd_762x39_AK47_M
			{
				magazine = "SPEC_30Rnd_762x39_AK47_M";
				count = 20;
			};
			class _xx_rhs_mag_20Rnd_762x51_m80_fnfal
			{
				magazine = "rhs_mag_20Rnd_762x51_m80_fnfal";
				count = 20;
			};
		};
		class TransportBackpacks
		{
			class _xx_YuE_RD54old
			{
				backpack = "YuE_RD54old";
				count = 1;
			};
		};
        enableSweep = 0;
        hiddenSelections[] = {"Camo1", "Camo2", "Camo_mlod", "decals"};
        hiddenSelectionsTextures[] = {
            "rhsgref\addons\rhsgref_air\uh1h\data\uh1h_gue_co.paa",
            "rhsgref\addons\rhsgref_air\uh1h\data\uh1h_gue_in_co.paa",
            "rhsgref\addons\rhsgref_air\uh1h\data\mlod_gue_co",
            "rhsgref\addons\rhsgref_air\uh1h\data\decals\blank_ca.paa"};
        class textureSources {
            class guerilla {
                displayName = "Guerilla";
                textures[] = {
                    "rhsgref\addons\rhsgref_air\uh1h\data\uh1h_gue_co.paa",
                    "rhsgref\addons\rhsgref_air\uh1h\data\uh1h_gue_in_co.paa",
                    "rhsgref\addons\rhsgref_air\uh1h\data\mlod_gue_co",
                    "rhsgref\addons\rhsgref_air\uh1h\data\decals\blank_ca.paa"};
                factions[] = {"SpecLib_Faction_Taki_Locals"};
            };
        };
        class AnimationSources : AnimationSources {
            class Hide_mid_doors {
                source = "user";
                animPeriod = 1e-006;
                initPhase = 1;
            };
        };
        textureList[] = {};
        class Attributes {
            class uh1h_textures {
                control = "ObjectTexture";
                data = "ObjectTexture";
                displayName = "Paint";
                tooltip = "Select paint color.";
            };
        };
    };
    class SpecLib_uh1h_Taki_Gue_gunship : SpecLib_uh1h_Taki_Gue {
        PREVIEW(SpecLib_uh1h_Taki_Gue_gunship);
        author = "$STR_RHS_AUTHOR_FULL";
        forceInGarage = 1;
        rhs_randomizedHabar[] = {};
        displayName = "UH-1H Gunship";
        availableForSupportTypes[] = {"CAS_Heli", "Drop", "Transport"};
        enableSweep = 1;
        rhs_decalParameters[] = {};
        class AnimationSources : AnimationSources {
            class hide_crosshair {
                source = "user";
                animPeriod = 1e-006;
                initPhase = 0;
            };
            class Hide_mid_doors {
                source = "user";
                animPeriod = 1e-006;
                initPhase = 1;
            };
            class hide_gunmount {
                source = "user";
                animPeriod = 1e-006;
                initPhase = 0;
            };
        };
        class Components : Components {
            class TransportPylonsComponent {
                UIPicture =
                    "\rhsgref\addons\rhsgref_air\uh1h\ui\RHS_UH1H_EDEN_CA.paa";
                class pylons {
                    class pylon1 {
                        hardpoints[] = {"RHS_HP_FFAR_ARMY"};
                        UIposition[] = {0.573, 0.44};
                        maxweight = 1200;
                        priority = 1;
                        attachment = "rhs_mag_M151_19";
                        bay = -1;
                    };
                    class pylon2 : pylon1 {
                        UIposition[] = {0.1, 0.44};
                        mirroredMissilePos = 1;
                    };
                };
                class Presets {
                    class Empty {
                        attachment[] = {"", ""};
                        displayname = "<empty>";
                    };
                    class Light {
                        attachment[] = {"rhs_mag_M151_7", "rhs_mag_M151_7"};
                        displayname = "Light";
                    };
                    class Heavy {
                        attachment[] = {"rhs_mag_M151_19", "rhs_mag_M151_19"};
                        displayname = "Heavy";
                    };
                };
            };
        };
    };
    class SpecLib_uh1h_Taki_Gue_unarmed : SpecLib_uh1h_Taki_Gue {
        PREVIEW(SpecLib_uh1h_Taki_Gue_unarmed);
        crew = "SpecLib_i_tk_gue_soldier_akms";
        typicalCargo[] = {};
        author = "$STR_RHS_AUTHOR_FULL";
        forceInGarage = 1;
        displayName = "UH-1H Unarmed";
        rhs_randomizedHabar[] = {};
        cargoAction[] = {"passenger_apc_narrow_generic02",
                         "passenger_apc_narrow_generic02",
                         "passenger_generic02_foldhands",
                         "passenger_generic01_leanleft",
                         "passenger_flatground_generic02",
                         "passenger_flatground_leanleft",
                         "Heli_Light_02_cargo",
                         "Heli_Light_02_cargo"};
        transportSoldier = 8;
        cargoProxyIndexes[] = {1, 2, 3, 4, 5, 6, 9, 10};
        getInProxyOrder[] = {2, 3, 1, 4, 5, 6, 9, 10};
        rhs_decalParameters[] = {};
        class AnimationSources : AnimationSources {
            class Hide_mid_doors {
                source = "user";
                animPeriod = 1e-006;
                initPhase = 0;
            };
            class Hide_guns {
                source = "user";
                animPeriod = 1e-006;
                initPhase = 1;
            };
        };
        class UserActions {
            class Open_Side_Doors {
                displayName = "Open Side Doors";
                onlyforplayer = 1;
                position = "aimpoint";
                radius = 2;
                priority = 1;
                condition =
                    "this doorPhase 'close_cargo_doors' > 0 and (alive this) and player in this;";
                statement = "this animateDoor ['close_cargo_doors',0]";
                showWindow = 0;
            };
            class Close_Side_Doors : Open_Side_Doors {
                displayName = "Close Side Doors";
                condition =
                    "this doorPhase 'close_cargo_doors' == 0 and (alive this) and player in this;";
                statement = "this animateDoor ['close_cargo_doors',1]";
            };
        };
        class Attributes : Attributes {
            class uh1h_textures : uh1h_textures {};
            class close_cargo_doors {
                displayName = "Close Side Doors";
                property = "close_cargo_doors";
                control = "CheckboxNumber";
                defaultValue = "0";
                expression = "_this animateDoor ['%s',_value,true]";
            };
        };
        class Turrets : Turrets {
            class CopilotTurret : CopilotTurret {};
        };
    };

    class SpecLib_bmp1_Taki_Gue : rhs_bmp1_msv {
        PREVIEW(SpecLib_bmp1_Taki_Gue);
        faction = "SpecLib_Faction_Taki_Locals";
        crew = "SpecLib_i_tk_gue_crewman";
        author = "$STR_RHS_AUTHOR_FULL";
        rhs_decalParameters[] = {};
        rhs_randomizedHabar[] = {};
        class textureSources {
            class standard {
                displayName = "Standard";
                author = "$STR_RHS_AUTHOR_FULL";
                textures[] = {"rhsafrf\addons\rhs_bmp\textures\bmp_1_co.paa",
                              "rhsafrf\addons\rhs_bmp\textures\bmp_2_co.paa",
                              "rhsafrf\addons\rhs_bmp\textures\bmp_3_co.paa",
                              "rhsafrf\addons\rhs_bmp\textures\bmp_4_co.paa",
                              "rhsafrf\addons\rhs_bmp\textures\bmp_5_co.paa",
                              "rhsafrf\addons\rhs_bmp\textures\bmp_6_co.paa"};
                factions[] = {"SpecLib_Faction_Taki_Locals"};
            };
        };
        textureList[] = {};
        class Attributes {
            class ObjectTexture {
                control = "ObjectTexture";
                data = "ObjectTexture";
                displayName = "Skin";
                tooltip = "Texture and material set applied on the object.";
            };
            class rhs_decalNumber_type {
                displayName = "Define font type of plate number";
                tooltip = "Define kind of font that will be drawn on vehicle.";
                property = "rhs_decalNumber_type";
                control = "Combo";
                expression =
                    "_this setVariable ['%s', _value];[_this,[['Number', cBMP3NumberPlaces, _value]]] call rhs_fnc_decalsInit";
                defaultValue = 0;
                typeName = "STRING";
                class values {
                    class Default {
                        name = "Default";
                        value = "Default";
                        defaultValue = "Default";
                    };
                };
            };
            class rhs_decalNumber {
                collapsed = 1;
                displayName = "Set side number";
                tooltip =
                    "Set side number. 4 numbers are required. Type 0 to hide numbers complety & leave at -1 to get random number";
                property = "rhs_decalNumber";
                control = "Edit";
                validate = "Number";
                typeName = "Number";
                defaultValue = "0";
                expression =
                    "if( _value >= 0)then{if( _value == 0)then{{[_this setobjectTexture [_x,'a3\data_f\clear_empty.paa']]}foreach cBMP3NumberPlaces}else{[_this, [['Number', cBMP3NumberPlaces, _this getVariable ['rhs_decalNumber_type','Default'], _value] ] ] call rhs_fnc_decalsInit}};";
            };
            class rhs_decalPlatoon_type {
                displayName = "Define platoon symbol type";
                tooltip = "Decal type";
                property = "rhs_decalPlatoon_type";
                control = "Combo";
                expression = "_this setVariable ['%s', _value];";
                defaultValue = "0";
                typeName = "STRING";
                class values {
                    class Platoon {
                        name = "Platoon";
                        value = "Platoon";
                        defaultValue = "0";
                    };
                };
            };
            class rhs_decalPlatoon {
                displayName = "Set platoon symbol";
                tooltip =
                    "Set platoon symbol located on right & rear of vehicles. Usually used for platoon symbols. -1 leaves current symbol & 0 clears decal.";
                property = "rhs_decalPlatoon";
                control = "Edit";
                validate = "none";
                defaultValue = "0";
                expression =
                    "if(parseNumber _value >= 0)then{ [_this, [ [ 'Label', cBMPPlatoon,  _this getVariable ['rhs_decalPlatoon_type','Platoon'],call compile _value] ] ] call rhs_fnc_decalsInit};";
            };
            class rhs_decalArmy_type : rhs_decalPlatoon_type {
                displayName = "Define left back symbol type";
                property = "rhs_decalArmy_type";
                class values : values {
                    class Platoon : Platoon {};
                };
            };
            class rhs_decalArmy : rhs_decalPlatoon {
                displayName = "Set left back symbol";
                tooltip =
                    "Define symbol located on left back side of vehicle. Usually used for army symbols. -1 leaves current symbol & 0 clears decal.";
                property = "rhs_decalArmy";
                expression =
                    "if(parseNumber _value >= 0)then{ [_this, [ [ 'Label', cBMPLeftBack,  _this getVariable ['rhs_decalArmy_type','Army'],call compile _value] ] ] call rhs_fnc_decalsInit};";
            };
            class rhs_decalRightTurret_type : rhs_decalArmy_type {
                displayName = "Define right turret symbol type";
                property = "rhs_decalRightTurret_type";
            };
            class rhs_decalRightTurret : rhs_decalPlatoon {
                displayName = "Set right turret symbol";
                tooltip =
                    "Define symbol located on right side of vehicle turret. Usually used for army symbols. -1 leaves current symbol & 0 clears decal.";
                property = "rhs_decalRightTurret";
                expression =
                    "if(parseNumber _value >= 0)then{ [_this, [ [ 'Label', cBMPRightTurret,  _this getVariable ['rhs_decalRightTurret_type','Army'],call compile _value] ] ] call rhs_fnc_decalsInit};";
            };
            class rhs_decalLeftTurret_type : rhs_decalArmy_type {
                displayName = "Define left turret symbol type";
                property = "rhs_decalLeftTurret_type";
            };
            class rhs_decalLeftTurret : rhs_decalPlatoon {
                displayName = "Set left turret symbol";
                tooltip =
                    "Define symbol located on left side of vehicle turret. Usually used for army symbols. -1 leaves current symbol & 0 clears decal.";
                property = "rhs_decalLeftTurret";
                expression =
                    "if(parseNumber _value >= 0)then{ [_this, [ [ 'Label', cBMPLeftTurret,  _this getVariable ['rhs_decalLeftTurret_type','Army'],call compile _value] ] ] call rhs_fnc_decalsInit};";
            };
            class rhs_decalFront_type : rhs_decalArmy_type {
                displayName = "Define front symbol type";
                property = "rhs_decalFront_type";
            };
            class rhs_decalFront : rhs_decalPlatoon {
                displayName = "Set front symbol";
                tooltip =
                    "Define symbol located on front of vehicle hull. Usually used for army symbols. -1 leaves current symbol & 0 clears decal.";
                property = "rhs_decalFront";
                expression =
                    "if(parseNumber _value >= 0)then{ [_this, [ [ 'Label', cBMPFront,  _this getVariable ['rhs_decalFront_type','Army'],call compile _value] ] ] call rhs_fnc_decalsInit};";
            };
            class crate_l1_unhide {
                displayName = "hide l1 crate";
                property = "crate_l1_unhide";
                control = "CheckboxNumber";
                expression = "[_this,_value,'%s'] call rhs_fnc_setHabarEden";
                defaultValue = "1";
            };
            class crate_l2_unhide : crate_l1_unhide {
                displayName = "Hide l2 crate";
                property = "crate_l2_unhide";
                defaultValue = "1";
            };
            class crate_l3_unhide : crate_l1_unhide {
                displayName = "Hide l3 crate";
                property = "crate_l3_unhide";
                defaultValue = "1";
            };
            class crate_r1_unhide : crate_l1_unhide {
                displayName = "Hide r1 crate";
                property = "crate_r1_unhide";
                defaultValue = "1";
            };
            class crate_r2_unhide : crate_l1_unhide {
                displayName = "Hide r2 crate";
                property = "crate_r2_unhide";
                defaultValue = "1";
            };
            class crate_r3_unhide : crate_l1_unhide {
                displayName = "Hide r3 crate";
                property = "crate_r3_unhide";
                defaultValue = "1";
            };
            class wood_1_unhide : crate_l1_unhide {
                displayName = "Hide wood log 1";
                property = "wood_1_unhide";
            };
            class wood_2_unhide : crate_l1_unhide {
                displayName = "Hide wood log 2";
                property = "wood_2_unhide";
            };
            class rhs_disableHabar : crate_l1_unhide {
                displayName = "Disable habar";
                property = "rhs_disableHabar";
                expression =
                    "[_this,_value,'%s',_value] call rhs_fnc_setHabarEden";
                defaultValue = "0";
            };
            class rhs_snorkel : rhs_disableHabar {
                displayName = "Rise Snorkel";
                property = "rhs_snorkel";
                expression = "_this animateSource ['Snorkel',_value,true]";
            };
            class rhs_externalMount : rhs_disableHabar {
                displayName = "Disable external mount";
                property = "rhs_externalMount";
                control = "Checkbox";
                expression = "[_this,_value] call rhs_fnc_lockTop";
            };
        };
		class TransportWeapons
		{
			class _xx_CUP_launch_RPG18
			{
				weapon = "CUP_launch_RPG18";
				count = 2;
			};
			class _xx_launch_RPG7_F
			{
				weapon = "launch_RPG7_F";
				count = 1;
			};
		};
		class TransportItems
		{
			class _xx_FirstAidKit
			{
				name = "FirstAidKit";
				count = 10;
			};
			class _xx_ToolKit
			{
				name = "ToolKit";
				count = 1;
			};
		};
		class TransportMagazines
		{
			class _xx_rhs_mag_rgd5
			{
				magazine = "rhs_mag_rgd5";
				count = 10;
			};
			class _xx_rhs_mag_rdg2_white
			{
				magazine = "rhs_mag_rdg2_white";
				count = 2;
			};
			class _xx_SPEC_30Rnd_762x39_AK47_M
			{
				magazine = "SPEC_30Rnd_762x39_AK47_M";
				count = 20;
			};
			class _xx_rhs_rpg7_PG7V_mag
			{
				magazine = "rhs_rpg7_PG7V_mag";
				count = 3;
			};
			class _xx_rhs_100Rnd_762x54mmR
			{
				magazine = "rhs_100Rnd_762x54mmR";
				count = 3;
			};
			class _xx_rhs_mag_20Rnd_762x51_m80_fnfal
			{
				magazine = "rhs_mag_20Rnd_762x51_m80_fnfal";
				count = 20;
			};
		};
		class TransportBackpacks
		{
			class _xx_YuE_RD54old
			{
				backpack = "YuE_RD54old";
				count = 2;
			};
			class _xx_rhs_rpg_empty
			{
				backpack = "rhs_rpg_empty";
				count = 1;
			};
		};
        side = 2;
    };

    class SpecLib_Taki_Gue_ural_zu23 : rhsgref_nat_ural_Zu23 {
        PREVIEW(SpecLib_Taki_Gue_ural_zu23);
        scope = 2;
        rhs_randomizedHabar[] = {};
        crew = "SpecLib_i_tk_gue_soldier_akms";
        author = "RHS (A2 port)";
        side = 2;
        faction = "SpecLib_Faction_Taki_Locals";
        accuracy = 0.5;
        rhs_decalParameters[] = {};
        hiddenSelections[] = {"camo1", "camo2", "n1",       "n2",
                              "n3",    "n4",    "i1",       "i2",
                              "i3",    "i4",    "numplate", "numplate2"};
        hiddenSelectionsTextures[] = {
            "rhsafrf\addons\rhs_a2port_car\ural\data\ural_kabina_khk_co.paa",
            "rhsafrf\addons\rhs_a2port_car\ural\data\ural_open_co.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa"};
		class TransportItems
		{
			class _xx_FirstAidKit
			{
				name = "FirstAidKit";
				count = 4;
			};
		};
		class TransportBackpacks{};
        class Turrets : Turrets {
            class MainTurret : MainTurret {
                gunnerType = "SpecLib_i_tk_gue_soldier_akm";
            };
            class CargoTurret_01 : CargoTurret_01 {
                gunnerType = "SpecLib_i_tk_gue_soldier_l1a1";
            };
            class CargoTurret_02 : CargoTurret_02 {};
            class CargoTurret_03 : CargoTurret_03 {};
        };
        class textureSources {
            class standard {
                displayName = "Khaki";
                author = "$STR_RHS_AUTHOR_FULL";
                textures[] = {
                    "rhsafrf\addons\rhs_a2port_car\ural\data\ural_kabina_khk_co.paa",
                    "rhsafrf\addons\rhs_a2port_car\ural\data\ural_open_co.paa"};
                factions[] = {"SpecLib_Faction_Taki_Locals"};
            };
        };
        textureList[] = {};
    };

    class SpecLib_gaz66_zu23_Taki_Gue : rhs_gaz66_zu23_msv {
        PREVIEW(SpecLib_gaz66_zu23_Taki_Gue);
        scope = 2;
        side = 2;
        faction = "SpecLib_Faction_Taki_Locals";
        crew = "SpecLib_i_tk_gue_soldier_akms";
        rhs_decalParameters[] = {};
        rhs_randomizedHabar[] = {};
        author = "$STR_RHS_AUTHOR_FULL";
        class Turrets : Turrets {
            class MainTurret : MainTurret {
                gunnerType = "SpecLib_i_tk_gue_soldier_akm";
            };
            class CargoTurret_01 : CargoTurret_01 {
                gunnerType = "SpecLib_i_tk_gue_soldier_l1a1";
            };
            class CargoTurret_02 : CargoTurret_02 {};
        };
        class textureSources {
            class standard {
                displayName = "Standard";
                author = "$STR_RHS_AUTHOR_FULL";
                textures[] = {
                    "\rhsafrf\addons\rhs_gaz66\data\gaz66_co.paa",
                    "\rhsafrf\addons\rhs_gaz66\data\tent_co.paa",
                    "\rhsafrf\addons\rhs_gaz66\data\rhs_gaz66_kung_co.paa",
                    "\rhsafrf\addons\rhs_gaz66\data\rhs_gaz66_ap2kung_co.paa",
                    "\rhsafrf\addons\rhs_gaz66\data\rhs_gaz66_repkung_co.paa"};
            };
        };
        textureList[] = {};
        class Attributes : Attributes {
            class ObjectTexture {
                control = "ObjectTexture";
                data = "ObjectTexture";
                displayName = "Skin";
                tooltip = "Texture and material set applied on the object.";
            };
            class rhs_decalNumber_type {
                displayName = "Define font type of plate number";
                tooltip = "Define kind of font that will be drawn on vehicle.";
                property = "rhs_decalNumber_type";
                control = "Combo";
                expression =
                    "_this setVariable ['%s', _value];[_this,[['Number', cTrucksGaz4NumberPlaces, _value]]] call rhs_fnc_decalsInit";
                defaultValue = 0;
                typeName = "STRING";
                class values {
                    class Default {
                        name = "Default";
                        value = "Default";
                        defaultValue = "Default";
                    };
                    class DefaultRed {
                        name = "Default (Red)";
                        value = "DefaultRed";
                    };
                    class BoldRed {
                        name = "Bold Red";
                        value = "BoldRed";
                    };
                    class CDF {
                        name = "CDF";
                        value = "CDF";
                    };
                    class Handpaint {
                        name = "Handpaint";
                        value = "Handpaint";
                    };
                    class HandpaintBlack {
                        name = "Handpaint Black";
                        value = "HandpaintBlack";
                    };
                    class Iraqi {
                        name = "Iraqi";
                        value = "Iraqi";
                    };
                    class LicensePlate {
                        name = "License Plate";
                        value = "LicensePlate";
                    };
                };
            };
            class rhs_decalNumber {
                collapsed = 1;
                displayName = "Set plate number";
                tooltip =
                    "Set plate number. 4 numbers are required. If 0, random number will be generated";
                property = "rhs_decalNumber";
                control = "Edit";
                validate = "Number";
                typeName = "Number";
                defaultValue = "-1";
                expression =
                    "if( _value >= 0)then{[_this,[['Number', cTrucksGaz4NumberPlaces, _this getVariable ['rhs_decalNumber_type','Default'], _value]]] call rhs_fnc_decalsInit};";
            };
            class rhs_decalArmy_type {
                displayName = "Define large door roundel type";
                tooltip = "Decal type";
                property = "rhs_decalArmy_type";
                control = "Combo";
                expression = "_this setVariable ['%s', _value];";
                defaultValue = "0";
                typeName = "STRING";
                class values {
                    class Army {
                        name = "Army";
                        value = "Army";
                        defaultValue = "Army";
                    };
                    class Honor {
                        name = "Honor";
                        value = "Honor";
                    };
                    class HonorGDR {
                        name = "Honor GDR";
                        value = "HonorGDR";
                    };
                    class Platoon {
                        name = "Platoon";
                        value = "Platoon";
                    };
                    class PlatoonGDR {
                        name = "Platoon GDR";
                        value = "PlatoonGDR";
                    };
                    class PlatoonVDV {
                        name = "Platoon VDV";
                        value = "PlatoonVDV";
                    };
                };
            };
            class rhs_decalArmy {
                displayName = "Set large door roundel symbol";
                tooltip =
                    "Set large door roundel located on both sides. Usually used for army symbols. -1 leaves current symbol & 0 clears decal.";
                property = "rhs_decalArmy";
                control = "Edit";
                validate = "none";
                defaultValue = "0";
                expression =
                    "if(parseNumber _value >= 0)then{ [_this, [ [ 'Label', cTrucksGazRightArmyPlaces,  _this getVariable ['rhs_decalArmy_type','Army'],call compile _value] ] ] call rhs_fnc_decalsInit};";
            };
            class rhs_decalPlatoon_type : rhs_decalArmy_type {
                displayName = "Define small door roundel type";
                property = "rhs_decalPlatoon_type";
                expression = "_this setVariable ['%s', _value];";
                defaultValue = "0";
            };
            class rhs_decalPlatoon : rhs_decalArmy {
                displayName = "Set small door roundel symbol";
                tooltip =
                    "Define small door roundel located on both sides. Usually used for platoon symbols. -1 leaves current symbol & 0 clears decal.";
                property = "rhs_decalPlatoon";
                expression =
                    "if(parseNumber _value >= 0)then{ [_this, [ [ 'Label', cTrucksGazRightPlatoonPlaces,  _this getVariable ['rhs_decalPlatoon_type','Army'],call compile _value] ] ] call rhs_fnc_decalsInit};";
            };
            class rhs_hideLightCover {
                displayName = "Hide light covers";
                property = "rhs_hideLightCover";
                control = "CheckboxNumber";
                expression = "_this animate ['light_hide',_value,true]";
                defaultValue = "0";
            };
            class rhs_hidespare : rhs_hideLightCover {
                displayName = "Remove spare wheel";
                property = "rhs_hidespare";
                expression = "_this animate ['spare_hide',_value,true]";
            };
        };
        hiddenSelections[] = {"camo1", "camo2",    "camo3",    "camo4",
                              "camo5", "n1",       "n2",       "n3",
                              "n4",    "i1",       "i2",       "i3",
                              "i4",    "numplate", "numplate2"};
        hiddenSelectionsTextures[] = {
            "\rhsafrf\addons\rhs_gaz66\data\gaz66_co.paa",
            "\rhsafrf\addons\rhs_gaz66\data\tent_co.paa",
            "\rhsafrf\addons\rhs_gaz66\data\rhs_gaz66_kung_co.paa",
            "\rhsafrf\addons\rhs_gaz66\data\rhs_gaz66_ap2kung_co.paa",
            "\rhsafrf\addons\rhs_gaz66\data\rhs_gaz66_repkung_co.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa"};
		class TransportItems
		{
			class _xx_FirstAidKit
			{
				name = "FirstAidKit";
				count = 4;
			};
		};
		class TransportBackpacks{};
    };

    class SpecLib_Ural_Fuel_Taki_Gue_01 : RHS_Ural_Fuel_MSV_01 {
        faction = "SpecLib_Faction_Taki_Locals";
        crew = "SpecLib_i_tk_gue_soldier_akm";
        PREVIEW(SpecLib_Ural_Fuel_Taki_Gue_01);
        scope = 2;
        side = 2;
        displayName = "$STR_RHS_URAL4320FUEL_NAME";
        author = "$STR_RHS_AUTHOR_FULL";
        model = "\rhsafrf\addons\rhs_a2port_car\Ural\Ural_refuel.p3d";
        icon = "\A3\soft_f_gamma\Truck_02\data\UI\Map_Truck_02_fuel_CA.paa";
        picture =
            "\rhsafrf\addons\rhs_a2port_car\data\ico\rhs_ural4320_fuel_pic_ca.paa";
        rhs_decalParameters[] = {};
        transportfuel = 10000;
        supplyRadius = 9.5;
        rhs_randomizedHabar[] = {};
        class Hitpoints : HitPoints {
            class HitFuelTank {
                armor = 0.5;
                material = -1;
                name = "Hit_FuelTank";
                armorComponent = "Hit_FuelTank";
                visual = "-";
                passThrough = 0.2;
                class DestructionEffects {};
            };
        };
        hiddenSelections[] = {"camo1", "camo2", "n1",    "n2",
                              "n3",    "n4",    "i1",    "i2",
                              "i3",    "i4",    "camo3", "numplate"};
        hiddenSelectionsTextures[] = {
            "rhsafrf\addons\rhs_a2port_car\ural\data\ural_kabina_khk_co.paa",
            "rhsafrf\addons\rhs_a2port_car\ural\data\ural_open_co.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\rhs_a2port_car\ural\data\ural_fuel_khk_co.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa"};
		class TransportItems
		{
			class _xx_FirstAidKit
			{
				name = "FirstAidKit";
				count = 4;
			};
		};
		class TransportBackpacks{};
        class textureSources {
            class standard {
                displayName = "Khaki";
                author = "$STR_RHS_AUTHOR_FULL";
                textures[] = {
                    "rhsafrf\addons\rhs_a2port_car\ural\data\ural_kabina_khk_co.paa",
                    "rhsafrf\addons\rhs_a2port_car\ural\data\ural_open_co.paa",
                    "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
                    "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
                    "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
                    "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
                    "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
                    "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
                    "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
                    "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
                    "rhsafrf\addons\rhs_a2port_car\ural\data\ural_fuel_khk_co.paa"};
                factions[] = {"rhs_Faction_vmf", "rhs_Faction_vdv",
                              "rhs_Faction_vdv", "rhs_Faction_vv"};
            };
        };
        textureList[] = {};
        class Attributes : Attributes {
            class ObjectTexture {
                control = "ObjectTexture";
                data = "ObjectTexture";
                displayName = "Skin";
                tooltip = "Texture and material set applied on the object.";
            };
            class rhs_decalNumber_type {
                displayName = "Define font type of plate number";
                tooltip = "Define kind of font that will be drawn on vehicle.";
                property = "rhs_decalNumber_type";
                control = "Combo";
                expression =
                    "_this setVariable ['%s', _value];[_this,[['Number', cTrucksGaz4NumberPlaces, _value]]] call rhs_fnc_decalsInit";
                defaultValue = 0;
                typeName = "STRING";
                class values {
                    class Default {
                        name = "Default";
                        value = "Default";
                        defaultValue = "Default";
                    };
                    class DefaultRed {
                        name = "Default (Red)";
                        value = "DefaultRed";
                    };
                    class BoldRed {
                        name = "Bold Red";
                        value = "BoldRed";
                    };
                    class CDF {
                        name = "CDF";
                        value = "CDF";
                    };
                    class Handpaint {
                        name = "Handpaint";
                        value = "Handpaint";
                    };
                    class HandpaintBlack {
                        name = "Handpaint Black";
                        value = "HandpaintBlack";
                    };
                    class Iraqi {
                        name = "Iraqi";
                        value = "Iraqi";
                    };
                    class LicensePlate {
                        name = "License Plate";
                        value = "LicensePlate";
                    };
                };
            };
            class rhs_decalNumber {
                collapsed = 1;
                displayName = "Set plate number";
                tooltip =
                    "Set plate number. 4 numbers are required. If 0, random number will be generated";
                property = "rhs_decalNumber";
                control = "Edit";
                validate = "Number";
                typeName = "Number";
                defaultValue = "-1";
                expression =
                    "if( _value >= 0)then{[_this,[['Number', cTrucksGaz4NumberPlaces, _this getVariable ['rhs_decalNumber_type','Default'], _value]]] call rhs_fnc_decalsInit};";
            };
            class rhs_decalArmy_type {
                displayName = "Define large door roundel type";
                tooltip = "Decal type";
                property = "rhs_decalArmy_type";
                control = "Combo";
                expression = "_this setVariable ['%s', _value];";
                defaultValue = "0";
                typeName = "STRING";
                class values {
                    class Army {
                        name = "Army";
                        value = "Army";
                        defaultValue = "Army";
                    };
                    class Honor {
                        name = "Honor";
                        value = "Honor";
                    };
                    class HonorGDR {
                        name = "Honor GDR";
                        value = "HonorGDR";
                    };
                    class Platoon {
                        name = "Platoon";
                        value = "Platoon";
                    };
                    class PlatoonGDR {
                        name = "Platoon GDR";
                        value = "PlatoonGDR";
                    };
                    class PlatoonVDV {
                        name = "Platoon VDV";
                        value = "PlatoonVDV";
                    };
                };
            };
            class rhs_decalArmy {
                displayName = "Set large door roundel symbol";
                tooltip =
                    "Set large door roundel located on both sides. Usually used for army symbols. -1 leaves current symbol & 0 clears decal.";
                property = "rhs_decalArmy";
                control = "Edit";
                validate = "none";
                defaultValue = "0";
                expression =
                    "if(parseNumber _value >= 0)then{ [_this, [ [ 'Label', cTrucksGazRightArmyPlaces,  _this getVariable ['rhs_decalArmy_type','Army'],call compile _value] ] ] call rhs_fnc_decalsInit};";
            };
            class rhs_decalPlatoon_type : rhs_decalArmy_type {
                displayName = "Define small door roundel type";
                property = "rhs_decalPlatoon_type";
                expression = "_this setVariable ['%s', _value];";
                defaultValue = "0";
            };
            class rhs_decalPlatoon : rhs_decalArmy {
                displayName = "Set small door roundel symbol";
                tooltip =
                    "Define small door roundel located on both sides. Usually used for platoon symbols. -1 leaves current symbol & 0 clears decal.";
                property = "rhs_decalPlatoon";
                expression =
                    "if(parseNumber _value >= 0)then{ [_this, [ [ 'Label', cTrucksGazRightPlatoonPlaces,  _this getVariable ['rhs_decalPlatoon_type','Army'],call compile _value] ] ] call rhs_fnc_decalsInit};";
            };
            class rhs_hideLightCover {
                displayName = "Hide light covers";
                property = "rhs_hideLightCover";
                control = "CheckboxNumber";
                expression = "_this animate ['light_hide',_value,true]";
                defaultValue = "0";
            };
            class rhs_hidespare : rhs_hideLightCover {
                displayName = "Remove spare wheel";
                property = "rhs_hidespare";
                expression = "_this animate ['spare_hide',_value,true]";
            };
        };
    };

    class SpecLib_gaz66_repair_Taki_Gue : rhs_gaz66_repair_msv {
        faction = "SpecLib_Faction_Taki_Locals";
        crew = "SpecLib_i_tk_gue_soldier_akm";
        PREVIEW(SpecLib_gaz66_repair_Taki_Gue);
        scope = 2;
        side = 2;
        rhs_decalParameters[] = {};
        author = "$STR_RHS_AUTHOR_FULL";
        ace_repair_canRepair = 1;
        rhs_randomizedHabar[] = {};
        hiddenSelections[] = {"camo1", "camo2",    "camo3",    "camo4",
                              "camo5", "n1",       "n2",       "n3",
                              "n4",    "i1",       "i2",       "i3",
                              "i4",    "numplate", "numplate2"};
        hiddenSelectionsTextures[] = {
            "rhsafrf\addons\rhs_gaz66\data\gaz66_co.paa",
            "rhsafrf\addons\rhs_gaz66\data\tent_co.paa",
            "rhsafrf\addons\rhs_gaz66\data\rhs_gaz66_kung_co.paa",
            "rhsafrf\addons\rhs_gaz66\data\rhs_gaz66_ap2kung_co.paa",
            "rhsafrf\addons\rhs_gaz66\data\rhs_gaz66_repkung_co.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa"};
        class textureSources {
            class standard {
                displayName = "Khaki";
                author = "$STR_RHS_AUTHOR_FULL";
                textures[] = {
                    "\rhsafrf\addons\rhs_gaz66\data\gaz66_co.paa",
                    "\rhsafrf\addons\rhs_gaz66\data\tent_co.paa",
                    "\rhsafrf\addons\rhs_gaz66\data\rhs_gaz66_kung_co.paa",
                    "\rhsafrf\addons\rhs_gaz66\data\rhs_gaz66_ap2kung_co.paa",
                    "\rhsafrf\addons\rhs_gaz66\data\rhs_gaz66_repkung_co.paa",
                    "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
                    "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
                    "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
                    "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
                    "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
                    "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
                    "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
                    "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa"};
                factions[] = {"rhs_Faction_vmf", "rhs_Faction_vdv",
                              "rhs_Faction_vdv", "rhs_Faction_vv"};
            };
        };
        textureList[] = {};
        class Attributes : Attributes {
            class ObjectTexture {
                control = "ObjectTexture";
                data = "ObjectTexture";
                displayName = "Skin";
                tooltip = "Texture and material set applied on the object.";
            };
            class rhs_decalNumber_type {
                displayName = "Define font type of plate number";
                tooltip = "Define kind of font that will be drawn on vehicle.";
                property = "rhs_decalNumber_type";
                control = "Combo";
                expression =
                    "_this setVariable ['%s', _value];[_this,[['Number', cTrucksGaz4NumberPlaces, _value]]] call rhs_fnc_decalsInit";
                defaultValue = 0;
                typeName = "STRING";
                class values {
                    class Default {
                        name = "Default";
                        value = "Default";
                        defaultValue = "Default";
                    };
                    class DefaultRed {
                        name = "Default (Red)";
                        value = "DefaultRed";
                    };
                    class BoldRed {
                        name = "Bold Red";
                        value = "BoldRed";
                    };
                    class CDF {
                        name = "CDF";
                        value = "CDF";
                    };
                    class Handpaint {
                        name = "Handpaint";
                        value = "Handpaint";
                    };
                    class HandpaintBlack {
                        name = "Handpaint Black";
                        value = "HandpaintBlack";
                    };
                    class Iraqi {
                        name = "Iraqi";
                        value = "Iraqi";
                    };
                    class LicensePlate {
                        name = "License Plate";
                        value = "LicensePlate";
                    };
                };
            };
            class rhs_decalNumber {
                collapsed = 1;
                displayName = "Set plate number";
                tooltip =
                    "Set plate number. 4 numbers are required. If 0, random number will be generated";
                property = "rhs_decalNumber";
                control = "Edit";
                validate = "Number";
                typeName = "Number";
                defaultValue = "-1";
                expression =
                    "if( _value >= 0)then{[_this,[['Number', cTrucksGaz4NumberPlaces, _this getVariable ['rhs_decalNumber_type','Default'], _value]]] call rhs_fnc_decalsInit};";
            };
            class rhs_decalArmy_type {
                displayName = "Define large door roundel type";
                tooltip = "Decal type";
                property = "rhs_decalArmy_type";
                control = "Combo";
                expression = "_this setVariable ['%s', _value];";
                defaultValue = "0";
                typeName = "STRING";
                class values {
                    class Army {
                        name = "Army";
                        value = "Army";
                        defaultValue = "Army";
                    };
                    class Honor {
                        name = "Honor";
                        value = "Honor";
                    };
                    class HonorGDR {
                        name = "Honor GDR";
                        value = "HonorGDR";
                    };
                    class Platoon {
                        name = "Platoon";
                        value = "Platoon";
                    };
                    class PlatoonGDR {
                        name = "Platoon GDR";
                        value = "PlatoonGDR";
                    };
                    class PlatoonVDV {
                        name = "Platoon VDV";
                        value = "PlatoonVDV";
                    };
                };
            };
            class rhs_decalArmy {
                displayName = "Set large door roundel symbol";
                tooltip =
                    "Set large door roundel located on both sides. Usually used for army symbols. -1 leaves current symbol & 0 clears decal.";
                property = "rhs_decalArmy";
                control = "Edit";
                validate = "none";
                defaultValue = "0";
                expression =
                    "if(parseNumber _value >= 0)then{ [_this, [ [ 'Label', cTrucksGazRightArmyPlaces,  _this getVariable ['rhs_decalArmy_type','Army'],call compile _value] ] ] call rhs_fnc_decalsInit};";
            };
            class rhs_decalPlatoon_type : rhs_decalArmy_type {
                displayName = "Define small door roundel type";
                property = "rhs_decalPlatoon_type";
                expression = "_this setVariable ['%s', _value];";
                defaultValue = "0";
            };
            class rhs_decalPlatoon : rhs_decalArmy {
                displayName = "Set small door roundel symbol";
                tooltip =
                    "Define small door roundel located on both sides. Usually used for platoon symbols. -1 leaves current symbol & 0 clears decal.";
                property = "rhs_decalPlatoon";
                expression =
                    "if(parseNumber _value >= 0)then{ [_this, [ [ 'Label', cTrucksGazRightPlatoonPlaces,  _this getVariable ['rhs_decalPlatoon_type','Army'],call compile _value] ] ] call rhs_fnc_decalsInit};";
            };
            class rhs_hideLightCover {
                displayName = "Hide light covers";
                property = "rhs_hideLightCover";
                control = "CheckboxNumber";
                expression = "_this animate ['light_hide',_value,true]";
                defaultValue = "0";
            };
            class rhs_hidespare : rhs_hideLightCover {
                displayName = "Remove spare wheel";
                property = "rhs_hidespare";
                expression = "_this animate ['spare_hide',_value,true]";
            };
        };
		class TransportItems
		{
			class _xx_FirstAidKit
			{
				name = "FirstAidKit";
				count = 10;
			};
            class _xx_Toolkit 
			{
                name = "Toolkit";
                count = 2;
            };
		};
		class TransportBackpacks
		{
			class _xx_YuE_RD54old
			{
				backpack = "YuE_RD54old";
				count = 2;
			};
		};
    };

    class SpecLib_gaz66_ammo_Taki_Gue : rhs_gaz66_ammo_msv {
        faction = "SpecLib_Faction_Taki_Locals";
        crew = "SpecLib_i_tk_gue_soldier_akm";
        side = 2;
        hiddenSelections[] = {"camo1", "camo2",    "camo3",    "camo4",
                              "camo5", "n1",       "n2",       "n3",
                              "n4",    "i1",       "i2",       "i3",
                              "i4",    "numplate", "numplate2"};
        hiddenSelectionsTextures[] = {
            "rhsafrf\addons\rhs_gaz66\data\gaz66_co.paa",
            "rhsafrf\addons\rhs_gaz66\data\tent_co.paa",
            "rhsafrf\addons\rhs_gaz66\data\rhs_gaz66_kung_co.paa",
            "rhsafrf\addons\rhs_gaz66\data\rhs_gaz66_ap2kung_co.paa",
            "rhsafrf\addons\rhs_gaz66\data\rhs_gaz66_repkung_co.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
            "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa"};
        class textureSources {
            class standard {
                displayName = "Khaki";
                author = "$STR_RHS_AUTHOR_FULL";
                textures[] = {
                    "\rhsafrf\addons\rhs_gaz66\data\gaz66_co.paa",
                    "\rhsafrf\addons\rhs_gaz66\data\tent_co.paa",
                    "\rhsafrf\addons\rhs_gaz66\data\rhs_gaz66_kung_co.paa",
                    "\rhsafrf\addons\rhs_gaz66\data\rhs_gaz66_ap2kung_co.paa",
                    "\rhsafrf\addons\rhs_gaz66\data\rhs_gaz66_repkung_co.paa",
                    "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
                    "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
                    "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
                    "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
                    "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
                    "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
                    "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa",
                    "rhsafrf\addons\RHS_Decals\Data\Labels\Misc\no_ca.paa"};
                factions[] = {"rhs_Faction_vmf", "rhs_Faction_vdv",
                              "rhs_Faction_vdv", "rhs_Faction_vv"};
            };
        };
        textureList[] = {};
        rhs_randomizedHabar[] = {};
        class Attributes : Attributes {
            class ObjectTexture {
                control = "ObjectTexture";
                data = "ObjectTexture";
                displayName = "Skin";
                tooltip = "Texture and material set applied on the object.";
            };
            class rhs_decalNumber_type {
                displayName = "Define font type of plate number";
                tooltip = "Define kind of font that will be drawn on vehicle.";
                property = "rhs_decalNumber_type";
                control = "Combo";
                expression =
                    "_this setVariable ['%s', _value];[_this,[['Number', cTrucksGaz4NumberPlaces, _value]]] call rhs_fnc_decalsInit";
                defaultValue = 0;
                typeName = "STRING";
                class values {
                    class Default {
                        name = "Default";
                        value = "Default";
                        defaultValue = "Default";
                    };
                    class DefaultRed {
                        name = "Default (Red)";
                        value = "DefaultRed";
                    };
                    class BoldRed {
                        name = "Bold Red";
                        value = "BoldRed";
                    };
                    class CDF {
                        name = "CDF";
                        value = "CDF";
                    };
                    class Handpaint {
                        name = "Handpaint";
                        value = "Handpaint";
                    };
                    class HandpaintBlack {
                        name = "Handpaint Black";
                        value = "HandpaintBlack";
                    };
                    class Iraqi {
                        name = "Iraqi";
                        value = "Iraqi";
                    };
                    class LicensePlate {
                        name = "License Plate";
                        value = "LicensePlate";
                    };
                };
            };
            class rhs_decalNumber {
                collapsed = 1;
                displayName = "Set plate number";
                tooltip =
                    "Set plate number. 4 numbers are required. If 0, random number will be generated";
                property = "rhs_decalNumber";
                control = "Edit";
                validate = "Number";
                typeName = "Number";
                defaultValue = "-1";
                expression =
                    "if( _value >= 0)then{[_this,[['Number', cTrucksGaz4NumberPlaces, _this getVariable ['rhs_decalNumber_type','Default'], _value]]] call rhs_fnc_decalsInit};";
            };
            class rhs_decalArmy_type {
                displayName = "Define large door roundel type";
                tooltip = "Decal type";
                property = "rhs_decalArmy_type";
                control = "Combo";
                expression = "_this setVariable ['%s', _value];";
                defaultValue = "0";
                typeName = "STRING";
                class values {
                    class Army {
                        name = "Army";
                        value = "Army";
                        defaultValue = "Army";
                    };
                    class Honor {
                        name = "Honor";
                        value = "Honor";
                    };
                    class HonorGDR {
                        name = "Honor GDR";
                        value = "HonorGDR";
                    };
                    class Platoon {
                        name = "Platoon";
                        value = "Platoon";
                    };
                    class PlatoonGDR {
                        name = "Platoon GDR";
                        value = "PlatoonGDR";
                    };
                    class PlatoonVDV {
                        name = "Platoon VDV";
                        value = "PlatoonVDV";
                    };
                };
            };
            class rhs_decalArmy {
                displayName = "Set large door roundel symbol";
                tooltip =
                    "Set large door roundel located on both sides. Usually used for army symbols. -1 leaves current symbol & 0 clears decal.";
                property = "rhs_decalArmy";
                control = "Edit";
                validate = "none";
                defaultValue = "0";
                expression =
                    "if(parseNumber _value >= 0)then{ [_this, [ [ 'Label', cTrucksGazRightArmyPlaces,  _this getVariable ['rhs_decalArmy_type','Army'],call compile _value] ] ] call rhs_fnc_decalsInit};";
            };
            class rhs_decalPlatoon_type : rhs_decalArmy_type {
                displayName = "Define small door roundel type";
                property = "rhs_decalPlatoon_type";
                expression = "_this setVariable ['%s', _value];";
                defaultValue = "0";
            };
            class rhs_decalPlatoon : rhs_decalArmy {
                displayName = "Set small door roundel symbol";
                tooltip =
                    "Define small door roundel located on both sides. Usually used for platoon symbols. -1 leaves current symbol & 0 clears decal.";
                property = "rhs_decalPlatoon";
                expression =
                    "if(parseNumber _value >= 0)then{ [_this, [ [ 'Label', cTrucksGazRightPlatoonPlaces,  _this getVariable ['rhs_decalPlatoon_type','Army'],call compile _value] ] ] call rhs_fnc_decalsInit};";
            };
            class rhs_hideLightCover {
                displayName = "Hide light covers";
                property = "rhs_hideLightCover";
                control = "CheckboxNumber";
                expression = "_this animate ['light_hide',_value,true]";
                defaultValue = "0";
            };
            class rhs_hidespare : rhs_hideLightCover {
                displayName = "Remove spare wheel";
                property = "rhs_hidespare";
                expression = "_this animate ['spare_hide',_value,true]";
            };
        };
        PREVIEW(SpecLib_gaz66_ammo_Taki_Gue);
        scope = 2;
        rhs_decalParameters[] = {};
        author = "$STR_RHS_AUTHOR_FULL";
		class TransportWeapons
		{
			class _xx_CUP_launch_RPG18
			{
				weapon = "CUP_launch_RPG18";
				count = 4;
			};
		};
		class TransportItems
		{
			class _xx_FirstAidKit
			{
				name = "FirstAidKit";
				count = 10;
			};
		};
		class TransportMagazines
		{
			class _xx_rhs_mag_rgd5
			{
				magazine = "rhs_mag_rgd5";
				count = 20;
			};
			class _xx_SPEC_30Rnd_762x39_AK47_M
			{
				magazine = "SPEC_30Rnd_762x39_AK47_M";
				count = 20;
			};
			class _xx_rhs_100Rnd_762x54mmR_green
			{
				magazine = "rhs_100Rnd_762x54mmR_green";
				count = 20;
			};
			class _xx_rhs_rpg7_PG7V_mag
			{
				magazine = "rhs_rpg7_PG7V_mag";
				count = 6;
			};
			class _xx_VTN_SVD_10s_SC
			{
				magazine = "VTN_SVD_10s_SC";
				count = 20;
			};
			class _xx_SPEC_40Rnd_TE4_LRT4_762x39_RPK_M
			{
				magazine = "SPEC_40Rnd_TE4_LRT4_762x39_RPK_M";
				count = 20;
			};
			class _xx_rhs_100Rnd_762x54mmR
			{
				magazine = "rhs_100Rnd_762x54mmR";
				count = 20;
			};
			class _xx_rhs_mag_20Rnd_762x51_m80_fnfal
			{
				magazine = "rhs_mag_20Rnd_762x51_m80_fnfal";
				count = 20;
			};
		};
		class TransportBackpacks
		{
			class _xx_YuE_RD54old
			{
				backpack = "YuE_RD54old";
				count = 2;
			};
		};
    };

    class SpecLib_Taki_Gue_ural : rhsgref_nat_ural {
        PREVIEW(SpecLib_Taki_Gue_ural);
        scope = 2;
        displayName = "$STR_RHS_URAL4320_NAME";
        author = "RHS (A2 port)";
        faction = "SpecLib_Faction_Taki_Locals";
        crew = "SpecLib_i_tk_gue_soldier_akm";
        side = 2;
        rhs_randomizedHabar[] = {};
        rhs_decalParameters[] = {};
        class AnimationSources : AnimationSources {
            class light_hide {
                source = "user";
                mass = 1;
                initPhase = 1;
                animPeriod = 1e-011;
                displayName = "hide light covers";
            };
        };
        hiddenSelectionsTextures[] = {
            "rhsafrf\addons\rhs_a2port_car\ural\data\ural_kabina_khk_co.paa",
            "rhsafrf\addons\rhs_a2port_car\ural\data\ural_plachta_co.paa"};
		class TransportItems
		{
			class _xx_FirstAidKit
			{
				name = "FirstAidKit";
				count = 4;
			};
		};
		class TransportBackpacks{};
        class textureSources {
            class standard {
                displayName = "Khaki";
                author = "$STR_RHS_AUTHOR_FULL";
                textures[] = {
                    "rhsafrf\addons\rhs_a2port_car\ural\data\ural_kabina_khk_co.paa",
                    "rhsafrf\addons\rhs_a2port_car\ural\data\ural_open_co.paa"};
                factions[] = {"SpecLib_Faction_Taki_Locals"};
            };
        };
        textureList[] = {};
    };

    class SpecLib_Taki_Gue_ural_open : rhsgref_nat_ural_open {
        scope = 2;
        displayName = "$STR_RHS_URAL4320OPEN_NAME";
        author = "RHS (A2 port)";
        faction = "SpecLib_Faction_Taki_Locals";
        crew = "SpecLib_i_tk_gue_soldier_akm";
        rhs_decalParameters[] = {};
        rhs_randomizedHabar[] = {};
        side = 2;
        class AnimationSources : AnimationSources {
            class light_hide {
                source = "user";
                mass = 1;
                initPhase = 1;
                animPeriod = 1e-011;
                displayName = "hide light covers";
            };
        };
        model = "\rhsafrf\addons\rhs_a2port_car\Ural\Ural_open.p3d";
        picture = "\rhsafrf\addons\rhs_a2port_car\data\ico\ural_open_ca.paa";
        hiddenSelectionsTextures[] = {
            "rhsafrf\addons\rhs_a2port_car\ural\data\ural_kabina_khk_co.paa",
            "rhsafrf\addons\rhs_a2port_car\ural\data\ural_plachta_co.paa"};
		class TransportItems
		{
			class _xx_FirstAidKit
			{
				name = "FirstAidKit";
				count = 4;
			};
		};
		class TransportBackpacks{};
        class textureSources {
            class standard {
                displayName = "Khaki";
                author = "$STR_RHS_AUTHOR_FULL";
                textures[] = {
                    "rhsafrf\addons\rhs_a2port_car\ural\data\ural_kabina_khk_co.paa",
                    "rhsafrf\addons\rhs_a2port_car\ural\data\ural_open_co.paa"};
                factions[] = {"SpecLib_Faction_Taki_Locals"};
            };
        };
        textureList[] = {};
    };

    class SpecLib_Taki_Gue_ZU23 : rhsgref_nat_ZU23 {
        PREVIEW(SpecLib_Taki_Gue_ZU23);
        author = "RHS (A2 port)";
        rhs_randomizedHabar[] = {};
        faction = "SpecLib_Faction_Taki_Locals";
        crew = "SpecLib_i_tk_gue_soldier_akms";
        side = 2;
        scope = 2;
        rhs_decalParameters[] = {};
    };

    class SpecLib_Taki_Gue_SPG9 : rhsgref_nat_SPG9 {
        PREVIEW(SpecLib_Taki_Gue_SPG9);
        author = "RHS (A2 port)";
        faction = "SpecLib_Faction_Taki_Locals";
        crew = "SpecLib_i_tk_gue_soldier_akm";
        side = 2;
        scope = 2;
        rhs_decalParameters[] = {};
        rhs_randomizedHabar[] = {};
    };

    class SpecLib_Taki_Gue_DSHKM : rhsgref_nat_DSHKM {
        PREVIEW(SpecLib_Taki_Gue_DSHKM);
        author = "RHS (A2 port)";
        faction = "SpecLib_Faction_Taki_Locals";
        crew = "SpecLib_i_tk_gue_soldier_akms";
        side = 2;
        rhs_decalParameters[] = {};
        rhs_randomizedHabar[] = {};
        scope = 2;
    };

    class SpecLib_Taki_Gue_DSHKM_Mini_TriPod : rhsgref_nat_DSHKM_Mini_TriPod {
        PREVIEW(SpecLib_Taki_Gue_DSHKM_Mini_TriPod);
        author = "RHS (A2 port)";
        faction = "SpecLib_Faction_Taki_Locals";
        crew = "SpecLib_i_tk_gue_soldier_akms";
        side = 2;
        scope = 2;
        rhs_decalParameters[] = {};
        rhs_randomizedHabar[] = {};
    };

class ace_dragon_staticBase;
class ace_dragon_staticAssembled : ace_dragon_staticBase {};
	class SpecLib_Taki_Gue_dragon: ace_dragon_staticAssembled
	{
		scope = 2;
		author = "$STR_ace_common_ACETeam";
		displayname = "$STR_ace_dragon_dragonName";
		side = 2;
		faction = "SpecLib_Faction_Taki_Locals";
		crew = "SpecLib_i_tk_gue_soldier_akms";
	};

    class SpecLib_Taki_Gue_AGS30_TriPod : rhsgref_nat_AGS30_TriPod {
        PREVIEW(SpecLib_Taki_Gue_AGS30_TriPod);
        author = "RHS (A2 port)";
        faction = "SpecLib_Faction_Taki_Locals";
        crew = "SpecLib_i_tk_gue_soldier_akm";
        side = 2;
        scope = 2;
        rhs_decalParameters[] = {};
    };
};
