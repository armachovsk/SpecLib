class CfgAmmo {
    class BulletBase;
    class B_303_Ball : BulletBase {
        hit = 15;
        indirectHit = 0;
        indirectHitRange = 0;
        cartridge = "FxCartridge_338_Ball";
        audibleFire = 80;
        visibleFireTime = 3;
        dangerRadiusBulletClose = 10;
        dangerRadiusHit = 14;
        suppressionRadiusBulletClose = 8;
        suppressionRadiusHit = 10;
        cost = 6;
        airLock = 1;
        caliber = 1.9;
        typicalSpeed = 844;
        timeToLive = 10;
        model = "\A3\Weapons_f\Data\bullettracer\tracer_red";
        tracerScale = 1.2;
        tracerStartTime = 0.075;
        tracerEndTime = 1;
        airFriction = -0.0006;
        class CamShakeExplode {
            power = "(10^0.5)";
            duration = "((round (10^0.5))*0.2 max 0.2)";
            frequency = 20;
            distance = "((10^0.5)*3)";
        };
        class CamShakeHit {
            power = 10;
            duration = "((round (10^0.25))*0.2 max 0.2)";
            frequency = 20;
            distance = 1;
        };
    };
};
