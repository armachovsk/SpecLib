class Air;
class Plane : Air {
    class HitPoints;
    class EventHandlers;
};
class Plane_Base_F : Plane {
    class NewTurret;
    class Turrets;
    class AnimationSources;
    class HitPoints : HitPoints {
        class HitHull;
    };
    class ViewPilot;
    class ViewGunner;
};
class Helicopter : Air {
    class ViewPilot;
    class Turrets;
    class HitPoints {
        class HitGlass1;
        class HitGlass2;
        class HitGlass3;
        class HitGlass4;
        class HitGlass5;
        class HitGlass6;
        class HitHull;
        class HitEngine;
        class HitAvionics;
    };
};
class Helicopter_Base_F : Helicopter {
    class Turrets : Turrets {
        class MainTurret;
    };
    class HitPoints : HitPoints {
        class HitGlass1;
        class HitGlass2;
        class HitGlass3;
        class HitGlass4;
        class HitGlass5;
        class HitGlass6;
        class HitMissiles;
        class HitHull;
        class HitFuel;
        class HitEngine;
        class HitAvionics;
        class HitVRotor;
        class HitHRotor;
    };
    class AnimationSources;
    class Eventhandlers;
    class ViewOptics;
    class CargoTurret;
    class RotorLibHelicopterProperties;
};
class Heli_Attack_02_base_F : Helicopter_Base_F {
    class RotorLibHelicopterProperties;
    class Sounds;
    class HitPoints : HitPoints {
        class HitEngine1;
    };
};
class Helicopter_Base_H : Helicopter_Base_F {
    class ViewOptics;
    class Turrets : Turrets {
        class CopilotTurret;
        class MainTurret;
    };
    class AnimationSources;
    class HitPoints : HitPoints {
        class HitHull;
        class HitFuel;
        class HitAvionics;
        class HitMissiles;
        class HitEngine;
        class HitHRotor;
        class HitVRotor;
        class HitGlass1;
        class HitGlass2;
        class HitGlass3;
        class HitGlass4;
        class HitGlass5;
        class HitGlass6;
    };
    class CargoTurret;
    class RotorLibHelicopterProperties;
    class Eventhandlers;
    class Reflectors {
        class Right;
    };
};
class LandVehicle;
class Tank : LandVehicle {
    class NewTurret;
    class Sounds;
    class HitPoints;
    class CommanderOptics;
};
class Tank_F : Tank {
    class Turrets {
        class MainTurret : NewTurret {
            class Turrets {
                class CommanderOptics;
            };
        };
    };
    class AnimationSources;
    class ViewPilot;
    class ViewOptics;
    class ViewCargo;
    class HeadLimits;
    class HitPoints : HitPoints {
        class HitHull;
        class HitEngine;
        class HitLTrack;
        class HitRTrack;
    };
    class Sounds : Sounds {
        class Engine;
        class Movement;
    };
    class EventHandlers;
    class Components;
};
class Car : LandVehicle {
    class HitPoints;
    class NewTurret;
};
class Car_F : Car {
    class Turrets {
        class MainTurret : NewTurret {
            class ViewOptics;
        };
    };
    class HitPoints {
        class HitLFWheel;
        class HitLF2Wheel;
        class HitRFWheel;
        class HitRF2Wheel;
        class HitBody;
        class HitGlass1;
        class HitGlass2;
        class HitGlass3;
        class HitGlass4;
        class HitGlass5;
        class HitGlass6;
    };
    class EventHandlers;
    class AnimationSources;
};
class Truck_F : Car_F {
    class ViewPilot;
    class HitPoints : HitPoints {
        class HitLFWheel;
        class HitLBWheel;
        class HitLMWheel;
        class HitLF2Wheel;
        class HitRFWheel;
        class HitRBWheel;
        class HitRMWheel;
        class HitRF2Wheel;
    };
    class Attributes;
    class AnimationSources;
};
class Wheeled_APC_F : Car_F {};
class Offroad_01_base_F : Car_F {};
class Man;
class CAManBase : Man {
    model = "\A3\Characters_F\OPFOR\o_soldier_01.p3d";
    class HitPoints {
        class HitHead;
        class HitBody;
        class HitHands;
        class HitLegs;
    };
};
class soldierWB : CAManBase {
    threat[] = {1, 0.1, 0.1};
    model = "\A3\Characters_F\BLUFOR\b_soldier_01.p3d";
    class EventHandlers;
};
class B_Soldier_F;
