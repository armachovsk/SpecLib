class Extended_PreStart_EventHandlers {
    class ADDON {
        init = QUOTE(call COMPILE_FILE(XEH_preStart));
    };
};

class Extended_PreInit_EventHandlers {
    class ADDON {
        init = QUOTE(call COMPILE_FILE(XEH_preInit));
    };
};

class Extended_Init_EventHandlers {
    class CUP_Creatures_Military_TKG_Guerrilla_Base {
        class BIS_RemoveRandomization {
            init = "(_this select 0) setVariable ['BIS_enableRandomization', false];";
        };
    };
    class CUP_Creatures_Military_TKC_Civ_Base {
        class BIS_RemoveRandomization {
            init = "(_this select 0) setVariable ['BIS_enableRandomization', false];";
        };
    };
    class INS1 {
        class BIS_RemoveRandomization {
            init = "(_this select 0) setVariable ['BIS_enableRandomization', false];";
        };
    };
    class INS1_Green {
        class BIS_RemoveRandomization {
            init = "(_this select 0) setVariable ['BIS_enableRandomization', false];";
        };
    };
    class INS1_White {
        class BIS_RemoveRandomization {
            init = "(_this select 0) setVariable ['BIS_enableRandomization', false];";
        };
    };
    class SpecLib_i_tk_gue_shakhid {
        class SpecLib_Shakhid {
            init =
                "_this call speclib_main_fnc_randomUniform;[(_this select 0), 300] spawn speclib_units_takistani_fnc_SSSB;";
        };
    };
};
