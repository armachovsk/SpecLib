class CfgMagazines {
    class CA_Magazine;
    class 10Rnd_303_Magazine : CA_Magazine {
        author = "Bnae";
        dlc = "p8_mod";
        scope = 2;
        model =
            "\z\speclib\addons\units_takistani\weapons\bnae_core\mesh\bnae_mk1_mag";
        displayName = "10Rnd .303";
        ammo = "B_303_Ball";
        count = 10;
        initSpeed = 915;
        picture =
            "\z\speclib\addons\units_takistani\weapons\bnae_core\UI\gear_bnae_mk1_mag_x_ca";
        descriptionShort = ".303 British";
        mass = 8;
    };
};
