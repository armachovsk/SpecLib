#include "script_component.hpp"

class CBA_Extended_EventHandlers;

#include "CfgAmmo.hpp"
#include "CfgEditorSubcategories.hpp"
#include "CfgEventHandlers.hpp"
#include "CfgGesturesMale.hpp"
#include "CfgMagazines.hpp"
#include "CfgMovesBasic.hpp"
#include "CfgPatches.hpp"
#include "CfgSounds.hpp"
#include "CfgVehicles.hpp"
#include "CfgWeapons.hpp"
#include "cfgFactionClasses.hpp"
