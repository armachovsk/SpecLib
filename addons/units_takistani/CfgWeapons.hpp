class Mode_SemiAuto;
class Mode_Burst;
class Mode_FullAuto;
class SlotInfo;
class MuzzleSlot;
class CowsSlot;
class PointerSlot;
class UnderBarrelSlot;
class CfgWeapons {
    class ItemCore;
    class InventoryOpticsItem_Base_F;
    class bnae_scope_v3_base : ItemCore {
        displayName = "A.K & S";
        author = "Bnae";
        dlc = "p8_mod";
        picture =
            "\z\speclib\addons\units_takistani\weapons\bnae_attachments\scope_v3\UI\gear_bnae_scope_v3_x_ca";
        model =
            "\z\speclib\addons\units_takistani\weapons\bnae_attachments\scope_v3\bnae_scope_v3.p3d";
        scope = 0;
        descriptionShort = "Long-range military optic";
        weaponInfoType = "RscWeaponZeroing";
        class ItemInfo : InventoryOpticsItem_Base_F {
            mass = 10;
            opticType = 2;
            weaponInfoType = "RscWeaponRangeZeroingFOV";
            modelOptics =
                "\z\speclib\addons\units_takistani\weapons\bnae_attachments\scope_v2\data\reticle\bnae_optic";
            optics = 1;
            class OpticsModes {
                class bnae_scope_base {
                    opticsID = 1;
                    opticsDisplayName = "WFOV";
                    useModelOptics = 1;
                    opticsPPEffects[] = {"OpticsCHAbera1", "OpticsBlur1"};
                    opticsZoomMin = 0.04;
                    opticsZoomMax = 0.04;
                    opticsZoomInit = 0.04;
                    discreteDistance[] = {300, 400,  500,  600,  700, 800,
                                          900, 1000, 1100, 1200, 1300};
                    discreteDistanceInitIndex = 0;
                    distanceZoomMin = 300;
                    distanceZoomMax = 1000;
                    discretefov[] = {0.04, 0.04};
                    discreteInitIndex = 0;
                    memoryPointCamera = "opticView";
                    modelOptics[] = {
                        "\z\speclib\addons\units_takistani\weapons\bnae_attachments\scope_v3\data\reticle\bnae_optic",
                        "\z\speclib\addons\units_takistani\weapons\bnae_attachments\scope_v3\data\reticle\bnae_optic"};
                    visionMode[] = {"Normal"};
                    opticsFlare = 1;
                    opticsDisablePeripherialVision = 1;
                    cameraDir = "";
                };
            };
        };
        inertia = 0.1;
    };
    class bnae_scope_v3_virtual : bnae_scope_v3_base {
        scope = 2;
        author = "Bnae";
    };
    class Rifle_Base_F;
    class Rifle_Long_Base_F : Rifle_Base_F {
        class WeaponSlotsInfo;
        class EventHandlers;
    };
    class bnae_mk1_base : Rifle_Long_Base_F {
        author = "Bnae";
        dlc = "p8_mod";
        displayname = "Mk.I No.4";
        model = "\z\speclib\addons\units_takistani\weapons\bnae_mk1\bnae_mk1";
        picture =
            "\z\speclib\addons\units_takistani\weapons\bnae_mk1\UI\gear_bnae_mk1_x_ca";
        descriptionshort = "Lee-Enfield MK.I No.4";
        aiRateOfFireDistance = 500;
        aiDispersionCoefY = 3;
        aiDispersionCoefX = 2;
        bullet1[] = {"\A3\sounds_f\weapons\shells\7_62\metal_762_01", 0.630957,
                     1, 15};
        bullet2[] = {"\A3\sounds_f\weapons\shells\7_62\metal_762_02", 0.630957,
                     1, 15};
        bullet3[] = {"\A3\sounds_f\weapons\shells\7_62\metal_762_03", 0.630957,
                     1, 15};
        bullet4[] = {"\A3\sounds_f\weapons\shells\7_62\metal_762_04", 0.630957,
                     1, 15};
        bullet5[] = {"\A3\sounds_f\weapons\shells\7_62\dirt_762_01", 0.630957,
                     1, 15};
        bullet6[] = {"\A3\sounds_f\weapons\shells\7_62\dirt_762_02", 0.630957,
                     1, 15};
        bullet7[] = {"\A3\sounds_f\weapons\shells\7_62\dirt_762_03", 0.630957,
                     1, 15};
        bullet8[] = {"\A3\sounds_f\weapons\shells\7_62\dirt_762_04", 0.630957,
                     1, 15};
        bullet9[] = {"\A3\sounds_f\weapons\shells\7_62\grass_762_01", 0.630957,
                     1, 15};
        bullet10[] = {"\A3\sounds_f\weapons\shells\7_62\grass_762_02", 0.630957,
                      1, 15};
        bullet11[] = {"\A3\sounds_f\weapons\shells\7_62\grass_762_03", 0.630957,
                      1, 15};
        bullet12[] = {"\A3\sounds_f\weapons\shells\7_62\grass_762_04", 0.630957,
                      1, 15};
        cursor = "srifle";
        drySound[] = {
            "A3\Sounds_F_Mark\arsenal\weapons\LongRangeRifles\DMR_02_MAR10\DMR_02_Dry_01",
            0.25118864, 1, 20};
        reloadMagazineSound[] = {
            "\z\speclib\addons\units_takistani\weapons\bnae_mk1\sound\MK1_reload",
            1, 1, 15};
        handAnim[] = {
            "OFP2_ManSkeleton",
            "\z\speclib\addons\units_takistani\weapons\bnae_mk1\Anim\mk1_handanim.rtm"};
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {
            "\z\speclib\addons\units_takistani\weapons\bnae_mk1\Data\Body\Body_co.paa"};
        class GunParticles {
            class FirstEffect {
                effectName = "SniperCloud";
                positionName = "Usti hlavne";
                directionName = "Konec hlavne";
            };
        };
        magazines[] = {"10Rnd_303_Magazine"};
        maxZeroing = 300;
        modes[] = {"Single", "far_optic1", "medium_optic2", "far_optic2"};
        overviewPicture = "\A3\Data_F_Mark\Images\watermarkInfo_page06_ca.paa";
        reloadAction = "GestureReloadMK1";
        recoil = "recoil_dmr_05";
        UiPicture = "\A3\weapons_f\data\UI\icon_regular_CA.paa";
        selectionFireAnim = "muzzleFlash";
        class EventHandlers : EventHandlers {
            fired = "_this call CBA_fnc_weaponEvents";
        };
        class CBA_weaponEvents {
            handAction = "GestureFiredMK1";
            sound = "MK1fired";
            soundLocation = "RightHandMiddle1";
            delay = 0.1;
            onEmpty = 0;
            hasOptic = 1;
            soundEmpty = "";
            soundLocationEmpty = "";
        };
        class WeaponSlotsInfo : WeaponSlotsInfo {
            mass = 220;
            class MuzzleSlot {};
            class CowsSlot {};
            class PointerSlot {};
            class UnderBarrelSlot {};
        };
        class Single : Mode_SemiAuto {
            class BaseSoundModeType;
            class StandardSound : BaseSoundModeType {
                soundSetShot[] = {"DMR03_Shot_SoundSet", "DMR03_tail_SoundSet",
                                  "DMR03_InteriorTail_SoundSet"};
            };
            class SilencedSound : BaseSoundModeType {
                SoundSetShot[] = {"DMR03_silencerShot_SoundSet",
                                  "DMR03_silencerTail_SoundSet",
                                  "DMR03_silencerInteriorTail_SoundSet"};
            };
            reloadTime = 1;
            dispersion = 0.00044;
            recoil = "recoil_single_dmr";
            recoilProne = "recoil_single_prone_dmr";
            minRange = 2;
            minRangeProbab = 0.3;
            midRange = 350;
            midRangeProbab = 0.7;
            maxRange = 500;
            maxRangeProbab = 0.05;
        };
        class far_optic1 : Single {
            showToPlayer = 0;
            minRange = 150;
            minRangeProbab = 0.1;
            midRange = 500;
            midRangeProbab = 0.7;
            maxRange = 1000;
            maxRangeProbab = 0.3;
            aiRateOfFire = 5;
            aiRateOfFireDistance = 700;
            requiredOpticType = 1;
        };
        class medium_optic2 : Single {
            showToPlayer = 0;
            minRange = 250;
            minRangeProbab = 0.1;
            midRange = 750;
            midRangeProbab = 0.7;
            maxRange = 1000;
            maxRangeProbab = 0.3;
            aiRateOfFire = 6;
            aiRateOfFireDistance = 1000;
            requiredOpticType = 2;
        };
        class far_optic2 : far_optic1 {
            minRange = 500;
            minRangeProbab = 0.1;
            midRange = 1050;
            midRangeProbab = 0.7;
            maxRange = 2000;
            maxRangeProbab = 0.3;
            aiRateOfFire = 8;
            aiRateOfFireDistance = 2000;
            requiredOpticType = 2;
        };
    };
    class bnae_mk1_virtual : bnae_mk1_base {
        scope = 2;
        author = "Bnae";
    };
    class bnae_mk1_t_virtual : bnae_mk1_base {
        displayname = "Mk.I No.4 (t)";
        scope = 2;
        author = "Bnae";
        cameraDir = "OP_look";
        discreteDistanceCameraPoint[] = {"OP_eye"};
        maxZeroing = 1300;
        model = "\z\speclib\addons\units_takistani\weapons\bnae_mk1\bnae_mk1t";
        reloadAction = "GestureReloadMK1t";
        reloadMagazineSound[] = {
            "\z\speclib\addons\units_takistani\weapons\bnae_mk1\sound\mk1t_reload",
            1, 1, 15};
        class WeaponSlotsInfo : WeaponSlotsInfo {
            class MuzzleSlot {};
            class CowsSlot : CowsSlot {
                access = 1;
                compatibleitems[] = {"bnae_scope_v3_virtual"};
                linkproxy = "\A3\data_f\proxies\weapon_slots\TOP";
                iconPosition[] = {0.5, 0.35};
                iconScale = 0.2;
            };
            class PointerSlot {};
            class UnderBarrelSlot {};
        };
    };
    class bnae_mk1_t_scope_virtual : bnae_mk1_t_virtual {
        author = "Bnae";
        class LinkedItems {
            class LinkedItemsOptic {
                slot = "CowsSlot";
                item = "bnae_scope_v3_virtual";
            };
        };
    };
};
