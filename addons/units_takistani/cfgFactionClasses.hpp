class cfgFactionClasses {
    class SpecLib_Faction_Taki_Locals {
        displayName = "Население Такистана";
        priority = 3;
        side = 2;
        icon = "";
    };
    class SpecLib_Faction_Taki_Civilians {
        displayName = "Гражданские (Такистан)";
        priority = 4;
        side = 3;
        icon = "";
    };
};
