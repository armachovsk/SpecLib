class CfgEditorSubcategories {
    class SpecLib_EdSubcat_Taki_Locals {
        displayName = "Люди";
    };
    class SpecLib_EdSubcat_APCs {
        displayName = "БТР";
    };
    class SpecLib_EdSubcat_IFVs {
        displayName = "БМП";
    };
    class SpecLib_EdSubcat_Trucks {
        displayName = "Грузовик";
    };
    class SpecLib_EdSubcat_Turrets {
        displayName = "Турели";
    };
    class SpecLib_EdSubcat_Civilians {
        displayName = "Люди";
    };
    class SpecLib_EdSubcat_Bikes {
        displayName = "Мотоцикл";
    };
    class SpecLib_EdSubcat_Cars {
        displayName = "Машина";
    };
    class SpecLib_EdSubcat_Planes {
        displayName = "Самолеты";
    };
};
