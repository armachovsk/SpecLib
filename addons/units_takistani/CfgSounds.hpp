class CfgSounds {
    sounds[] = {};
    class SpecLib_AllahuAkbar {
        name = "Allahu Akbar!";
        sound[] = {"\z\speclib\addons\units_takistani\sounds\allahu_akbar.ogg",
                   1, 1, 75};
        titles[] = {0, ""};
    };
    class MK1fired {
        name = "mk1_fired";
        sound[] = {
            "\z\speclib\addons\units_takistani\weapons\bnae_mk1\sound\mk1_fired",
            1, 1, 10};
        titles[] = {};
    };
};
