class CfgMovesBasic {
    class default;
    class DefaultDie;
    class ManActions {
        GestureReloadMK1[] = {"GestureReloadMK1", "gesture"};
        GestureReloadMK1t[] = {"GestureReloadMK1t", "gesture"};
        GestureFiredMK1[] = {"GestureFiredMK1", "gesture"};
    };
    class Actions {
        class RifleBaseStandActions;
        class RifleAdjustProneBaseActions;
        class NoActions : ManActions {
            GestureReloadMK1[] = {"GestureReloadMK1", "Gesture"};
            GestureReloadMK1t[] = {"GestureReloadMK1t", "gesture"};
            GestureFiredMK1[] = {"GestureFiredMK1", "Gesture"};
        };
        class RifleProneActions : RifleBaseStandActions {
            GestureReloadMK1[] = {"GestureReloadMK1Prone", "Gesture"};
            GestureReloadMK1t[] = {"GestureReloadMK1tProne", "gesture"};
            GestureFiredMK1[] = {"GestureFiredMK1Prone", "Gesture"};
        };
        class RifleAdjustRProneActions : RifleAdjustProneBaseActions {
            GestureReloadMK1[] = {"GestureReloadMK1Context", "Gesture"};
            GestureReloadMK1t[] = {"GestureReloadMK1tContext", "gesture"};
            GestureFiredMK1[] = {"GestureFiredMK1Context", "Gesture"};
        };
        class RifleAdjustLProneActions : RifleAdjustProneBaseActions {
            GestureReloadMK1[] = {"GestureReloadMK1Context", "Gesture"};
            GestureReloadMK1t[] = {"GestureReloadMK1tContext", "gesture"};
            GestureFiredMK1[] = {"GestureFiredMK1Context", "Gesture"};
            class RifleAdjustFProneActions : RifleAdjustProneBaseActions {
                GestureReloadMK1[] = {"GestureReloadMK1Context", "Gesture"};
                GestureReloadMK1t[] = {"GestureReloadMK1tContext", "gesture"};
                GestureFiredMK1[] = {"GestureFiredMK1Context", "Gesture"};
            };
        };
    };
};
