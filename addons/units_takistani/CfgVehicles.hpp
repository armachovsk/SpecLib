class CfgVehicles {
#include "include\a3.hpp"
#include "include\cup.hpp"
#include "include\rds.hpp"
#include "include\rhs.hpp"

    class INS1 : B_Soldier_F {
        class EventHandlers;
    };
    class INS1_Green : B_Soldier_F {
        class EventHandlers;
    };
    class INS1_White : B_Soldier_F {
        class EventHandlers;
    };
    class Weapon_Base_F;
    class bnae_mk1_editor : Weapon_Base_F {
        scope = 2;
        scopeCurator = 2;
        displayName = "MK.1 No.4 (t)";
        editorCategory = "EdCat_Weapons";
        editorSubcategory = "EdSubcat_SniperRifles";
        vehicleClass = "WeaponsPrimary";
        faction = "BLU_F";
        author = "Bnae";
        class TransportWeapons {
            class bnae_mk1_t_virtual {
                weapon = "bnae_mk1_t_virtual";
                count = 1;
            };
        };
        class TransportMagazines {
            class 10Rnd_303_Magazine {
                magazine = "10Rnd_303_Magazine";
                count = 1;
            };
        };
    };
    class YuE_RD54old;
    class SpecLib_Taki_medic_bag : YuE_RD54old {
        scope = 1;
        class TransportMagazines {
            mag_xx(rhs_mag_rdg2_white, 4);
        };
        class TransportItems {
            item_xx(ACE_fieldDressing, 14);
            item_xx(ACE_elasticBandage, 14);
            item_xx(ACE_quikclot, 14);
            item_xx(ACE_packingBandage, 14);
            item_xx(ACE_tourniquet, 4);
            item_xx(ACE_morphine, 6);
            item_xx(ACE_epinephrine, 6);
            item_xx(ACE_adenosine, 6);
            item_xx(ACE_salineIV_500, 4);
            item_xx(ACE_surgicalKit, 1);
        };
    };
    class SpecLib_Taki_bomber_bag : YuE_RD54old {
        scope = 1;
        class TransportItems {
            item_xx(ACE_DefusalKit, 1);
            item_xx(IEDLandSmall_Remote_Mag, 1);
            item_xx(IEDLandBig_Remote_Mag, 1);
            item_xx(ACE_Cellphone, 1);
        };
    };
    class SpecLib_Taki_rpg7_bag : rhs_rpg_empty {
        scope = 1;
        class TransportItems {
            item_xx(rhs_rpg7_PG7V_mag, 3);
        };
    };
    class SpecLib_Taki_mechanic_bag : YuE_RD54old {
        scope = 1;
        class TransportItems {
            item_xx(ToolKit, 1);
            item_xx(ACE_EntrenchingTool, 1);
        };
    };
    class SpecLib_Taki_machinegunner_bag : YuE_RD54old {
        scope = 1;
        class TransportItems {
            item_xx(rhs_100Rnd_762x54mmR, 3);
        };
    };
    class SpecLib_Taki_autorifleman_bag : YuE_RD54old {
        scope = 1;
        class TransportItems {
            item_xx(SPEC_40Rnd_TE4_LRT4_762x39_RPK_M, 6);
        };
    };
    class SpecLib_i_tk_gue_soldier_aa : CUP_I_TKG_Man_01 {
        PREVIEW(SpecLib_i_tk_gue_soldier_aa);
        DEFAULT_GUE_UNIT_ENTRIES;

        uniformClass = "CUP_I_TKG_Khet_Partug_01";

        accuracy = 3.9;
        displayName = "Оператор ПЗРК";
		icon = "iconManAT";
        threat[] = {1, 0.1, 1};
        weapons[] = {"CUP_arifle_AKM_Early", "rhs_weap_fim92", "Throw", "Put"};
        respawnWeapons[] = {"CUP_arifle_AKM_Early", "rhs_weap_fim92", "Throw", "Put"};
        magazines[] = {mag_3("SPEC_30Rnd_762x39_AK47_M"), "rhs_fim92_mag"};
        respawnMagazines[] = {mag_3("SPEC_30Rnd_762x39_AK47_M"), "rhs_fim92_mag"};
        Items[] = {DEFAULT_ACE_ITEMS};
        RespawnItems[] = {DEFAULT_ACE_ITEMS};
        linkedItems[] = {"CUP_H_TKI_Lungee_02",
                         "CUP_V_OI_TKI_Jacket1_01",
                         DEFAULT_BOOMER_ITEMS};
        respawnLinkedItems[] = {"CUP_H_TKI_Lungee_02",
                                "CUP_V_OI_TKI_Jacket1_01",
                                DEFAULT_BOOMER_ITEMS};
    };
    class SpecLib_i_tk_gue_medic : CUP_I_TKG_Man_01 {
        PREVIEW(SpecLib_i_tk_gue_medic);
        DEFAULT_GUE_UNIT_ENTRIES;

        uniformClass = "CUP_I_TKG_Khet_Partug_01";

        accuracy = 3.9;
        attendant = 1;
        displayName = "Костоправ";
		icon = "iconManMedic";
        weapons[] = {"bnae_mk1_virtual", "Throw", "Put"};
        respawnWeapons[] = {"bnae_mk1_virtual", "Throw", "Put"};
        magazines[] = {mag_7("10Rnd_303_Magazine")};
        respawnMagazines[] = {mag_7("10Rnd_303_Magazine")};
        Items[] = {DEFAULT_ACE_ITEMS};
        RespawnItems[] = {DEFAULT_ACE_ITEMS};
        linkedItems[] = {"CUP_H_TKI_Lungee_05",
                         "CUP_V_OI_TKI_Jacket3_04",
                         DEFAULT_BOOMER_ITEMS};
        respawnLinkedItems[] = {"CUP_H_TKI_Lungee_05",
                                "CUP_V_OI_TKI_Jacket3_04",
                                DEFAULT_BOOMER_ITEMS};
        backpack = "SpecLib_Taki_medic_bag";
    };
    class SpecLib_i_tk_gue_bomber : INS1 {
        PREVIEW(SpecLib_i_tk_gue_bomber);
        DEFAULT_GUE_UNIT_ENTRIES;

        uniformClass = "CUP_I_TKG_Khet_Partug_07";

        side = 2;
        faction = "SpecLib_Faction_Taki_Locals";
        editorSubcategory = "SpecLib_EdSubcat_Taki_Locals";
        threat[] = {1, 0.5, 0.1};
        canDeactivateMines = 1;
        detectSkill = 40;
        backpack = "SpecLib_Taki_bomber_bag";
        accuracy = 3.9;
        displayName = "Подрывник";
		icon = "iconManExplosive";
        weapons[] = {"CUP_arifle_AKM_Early", "Throw", "Put"};
        respawnWeapons[] = {"CUP_arifle_AKM_Early", "Throw", "Put"};
        magazines[] = {mag_4("SPEC_30Rnd_762x39_AK47_M")};
        respawnMagazines[] = {mag_4("SPEC_30Rnd_762x39_AK47_M")};
        Items[] = {DEFAULT_ACE_ITEMS};
        RespawnItems[] = {DEFAULT_ACE_ITEMS};
        linkedItems[] = {"CUP_H_TKI_Lungee_04",
                         "CUP_V_OI_TKI_Jacket2_06",
                         DEFAULT_BOOMER_ITEMS};
        respawnLinkedItems[] = {"CUP_H_TKI_Lungee_04",
                                "CUP_V_OI_TKI_Jacket2_06",
                                DEFAULT_BOOMER_ITEMS};
    };
    class SpecLib_i_tk_gue_soldier_akm : CUP_I_TKG_Man_06 {
        PREVIEW(SpecLib_i_tk_gue_soldier_akm);
        DEFAULT_GUE_UNIT_ENTRIES;

        uniformClass = "CUP_I_TKG_Khet_Partug_06";

        accuracy = 3.9;
        displayName = "Партизан";
        weapons[] = {"CUP_arifle_AKM_Early", "Throw", "Put"};
        respawnWeapons[] = {"CUP_arifle_AKM_Early", "Throw", "Put"};
        magazines[] = {mag_5("SPEC_30Rnd_762x39_AK47_M"), mag_2("rhs_mag_rgd5")};
        respawnMagazines[] = {mag_5("SPEC_30Rnd_762x39_AK47_M"),
                              mag_2("rhs_mag_rgd5")};
        Items[] = {DEFAULT_ACE_ITEMS};
        RespawnItems[] = {DEFAULT_ACE_ITEMS};
        linkedItems[] = {"CUP_Beard_Black", "CUP_H_TKI_Pakol_2_03",
                         "CUP_V_OI_TKI_Jacket4_06",
                         DEFAULT_BOOMER_ITEMS};
        respawnLinkedItems[] = {"CUP_Beard_Black", "CUP_H_TKI_Pakol_2_03",
                                "CUP_V_OI_TKI_Jacket4_06",
                                DEFAULT_BOOMER_ITEMS};
    };
    class SpecLib_i_tk_gue_soldier_lat : CUP_I_TKG_Man_06 {
        PREVIEW(SpecLib_i_tk_gue_soldier_akm);
        DEFAULT_GUE_UNIT_ENTRIES;

        uniformClass = "CUP_I_TKG_Khet_Partug_06";

        accuracy = 3.9;
        displayName = "Партизан (РПГ-18)";
		icon = "iconManAT";
        weapons[] = {"CUP_arifle_AKM_Early", "CUP_launch_RPG18", "Throw", "Put"};
        respawnWeapons[] = {"CUP_arifle_AKM_Early", "CUP_launch_RPG18", "Throw", "Put"};
        magazines[] = {mag_5("SPEC_30Rnd_762x39_AK47_M"), mag_2("rhs_mag_rgd5")};
        respawnMagazines[] = {mag_5("SPEC_30Rnd_762x39_AK47_M"),
                              mag_2("rhs_mag_rgd5")};
        Items[] = {DEFAULT_ACE_ITEMS};
        RespawnItems[] = {DEFAULT_ACE_ITEMS};
        linkedItems[] = {"CUP_Beard_Black", "CUP_H_TKI_Pakol_2_03",
                         "CUP_V_OI_TKI_Jacket4_06",
                         DEFAULT_BOOMER_ITEMS};
        respawnLinkedItems[] = {"CUP_Beard_Black", "CUP_H_TKI_Pakol_2_03",
                                "CUP_V_OI_TKI_Jacket4_06",
                                DEFAULT_BOOMER_ITEMS};
    };
    class SpecLib_i_tk_gue_soldier_akms : CUP_I_TKG_Man_01 {
        PREVIEW(SpecLib_i_tk_gue_soldier_akms);
        DEFAULT_GUE_UNIT_ENTRIES;

        uniformClass = "CUP_I_TKG_Khet_Partug_02";

        accuracy = 3.9;
        displayName = "Партизан (АКМС)";
        weapons[] = {"CUP_arifle_AKMS_Early", "Throw", "Put"};
        respawnWeapons[] = {"CUP_arifle_AKMS_Early", "Throw", "Put"};
        magazines[] = {mag_5("SPEC_30Rnd_762x39_AK47_M"), mag_2("rhs_mag_rgd5")};
        respawnMagazines[] = {mag_5("SPEC_30Rnd_762x39_AK47_M"),
                              mag_2("rhs_mag_rgd5")};
        Items[] = {DEFAULT_ACE_ITEMS};
        RespawnItems[] = {DEFAULT_ACE_ITEMS};
        linkedItems[] = {"CUP_Beard_Black", "CUP_H_TKI_Pakol_2_01",
                         "CUP_V_OI_TKI_Jacket4_01",
                         DEFAULT_BOOMER_ITEMS};
        respawnLinkedItems[] = {"CUP_Beard_Black", "CUP_H_TKI_Pakol_2_01",
                                "CUP_V_OI_TKI_Jacket4_01",
                                DEFAULT_BOOMER_ITEMS};
    };
    class SpecLib_i_tk_gue_soldier_dragon : CUP_I_TKG_Man_06 {
        PREVIEW(SpecLib_i_tk_gue_soldier_dragon);
        DEFAULT_GUE_UNIT_ENTRIES;
        accuracy = 3.9;
        displayName = "Оператор ПТРК";
		icon = "iconManAT";
        weapons[] = {"CUP_arifle_AKM_Early", "ace_dragon_sight", "ace_dragon_super", "Throw", "Put"};
        respawnWeapons[] = {"CUP_arifle_AKM_Early","ace_dragon_sight", "ace_dragon_super", "Throw", "Put"};
        magazines[] = {mag_4("SPEC_30Rnd_762x39_AK47_M")};
        respawnMagazines[] = {mag_4("SPEC_30Rnd_762x39_AK47_M")};
        Items[] = {DEFAULT_ACE_ITEMS};
        RespawnItems[] = {DEFAULT_ACE_ITEMS};
        linkedItems[] = {"CUP_H_TKI_Lungee_06",
                         "CUP_V_OI_TKI_Jacket2_04",
                         DEFAULT_BOOMER_ITEMS};
        respawnLinkedItems[] = {"CUP_H_TKI_Lungee_06",
                                "CUP_V_OI_TKI_Jacket2_04",
                                DEFAULT_BOOMER_ITEMS};
    };
    class SpecLib_i_tk_gue_soldier_l1a1 : CUP_I_TKG_Man_01 {
        PREVIEW(SpecLib_i_tk_gue_soldier_l1a1);
        DEFAULT_GUE_UNIT_ENTRIES;

        uniformClass = "CUP_I_TKG_Khet_Partug_01";

        accuracy = 3.9;
        displayName = "Партизан (L1A1)";
        weapons[] = {"rhs_weap_l1a1", "Throw", "Put"};
        respawnWeapons[] = {"rhs_weap_l1a1", "Throw", "Put"};
        magazines[] = {mag_4("rhs_mag_20Rnd_762x51_m80_fnfal")};
        respawnMagazines[] = {mag_4("rhs_mag_20Rnd_762x51_m80_fnfal")};
        Items[] = {DEFAULT_ACE_ITEMS};
        RespawnItems[] = {DEFAULT_ACE_ITEMS};
        linkedItems[] = {"CUP_V_OI_TKI_Jacket2_05",
                         "CUP_H_TKI_Lungee_06",
                         DEFAULT_BOOMER_ITEMS};
        respawnLinkedItems[] = {"CUP_H_TKI_Lungee_06",
                                "CUP_V_OI_TKI_Jacket2_05",
                                DEFAULT_BOOMER_ITEMS};
    };
    class SpecLib_i_tk_gue_enfield : CUP_I_TKG_Man_01 {
        PREVIEW(SpecLib_i_tk_gue_enfield);
        DEFAULT_GUE_UNIT_ENTRIES;

        uniformClass = "CUP_I_TKG_Khet_Partug_03";

        threat[] = {1, 0.5, 0.1};
        detectSkill = 40;
        accuracy = 3.9;
        displayName = "Партизан (Lee-Enfield)";
        weapons[] = {"bnae_mk1_virtual", "Throw", "Put"};
        respawnWeapons[] = {"bnae_mk1_virtual", "Throw", "Put"};
        magazines[] = {mag_7("10Rnd_303_Magazine")};
        respawnMagazines[] = {mag_7("10Rnd_303_Magazine")};
        Items[] = {DEFAULT_ACE_ITEMS};
        RespawnItems[] = {DEFAULT_ACE_ITEMS};
        linkedItems[] = {"CUP_Beard_Black", "CUP_H_TKI_Lungee_Open_04",
                         "CUP_V_OI_TKI_Jacket3_04",
                         DEFAULT_BOOMER_ITEMS};
        respawnLinkedItems[] = {"CUP_Beard_Black", "CUP_H_TKI_Lungee_Open_04",
                                "CUP_V_OI_TKI_Jacket3_04",
                                DEFAULT_BOOMER_ITEMS};
    };
    class SpecLib_i_tk_gue_shakhid : CUP_I_TKG_Man_03 {
        // PREVIEW(SpecLib_i_tk_gue_enfield);
        DEFAULT_GUE_UNIT_ENTRIES;
        threat[] = {1, 0.5, 0.1};
        detectSkill = 40;
        accuracy = 3.9;
        displayName = "Шахид";
        weapons[] = {"Throw", "Put"};
        respawnWeapons[] = {"Throw", "Put"};
        magazines[] = {};
        respawnMagazines[] = {};
        Items[] = {};
        RespawnItems[] = {};
        speclib_randomCloths[] = {
            "CUP_I_TKG_Khet_Partug_04", 0.25, "CUP_I_TKG_Khet_Partug_03", 0.25,
            "CUP_I_TKG_Khet_Partug_02", 0.25, "CUP_I_TKG_Khet_Partug_01", 0.25,
            "CUP_I_TKG_Khet_Partug_07", 0.25, "CUP_I_TKG_Khet_Partug_08", 0.25,
            "CUP_I_TKG_Khet_Partug_05", 0.25, "CUP_I_TKG_Khet_Partug_06", 0.25,};
        linkedItems[] = {"H_Headwrap_RED", "C4_belt", DEFAULT_BOOMER_ITEMS};
        respawnLinkedItems[] = {"H_Headwrap_RED",
                                "C4_belt",
                                DEFAULT_BOOMER_ITEMS};
    };
    class SpecLib_i_tk_gue_soldier_sniper : CUP_I_TKG_Man_02 {
        PREVIEW(SpecLib_i_tk_gue_soldier_sniper);
        DEFAULT_GUE_UNIT_ENTRIES;

        uniformClass = "CUP_I_TKG_Khet_Partug_06";

        accuracy = 3.9;
        displayName = "Снайпер";
        weapons[] = {"VTN_SVD_1963_PSO1", "VTN_B8", "Throw", "Put"};
        respawnWeapons[] = {"VTN_SVD_1963_PSO1", "VTN_B8", "Throw", "Put"};
        magazines[] = {mag_5("VTN_SVD_10s_SC"), mag_2("rhs_mag_rgd5")};
        respawnMagazines[] = {mag_5("VTN_SVD_10s_SC"), mag_2("rhs_mag_rgd5")};
        Items[] = {DEFAULT_ACE_ITEMS};
        RespawnItems[] = {DEFAULT_ACE_ITEMS};
        linkedItems[] = {"CUP_Beard_Black", "CUP_V_OI_TKI_Jacket4_05",
                         "CUP_H_TKI_Pakol_2_04",
                         DEFAULT_BOOMER_ITEMS};
        respawnLinkedItems[] = {"CUP_Beard_Black", "CUP_H_TKI_Pakol_2_04",
                                "CUP_V_OI_TKI_Jacket4_05",
                                DEFAULT_BOOMER_ITEMS};
    };
    class SpecLib_i_tk_gue_soldier_rpg7 : INS1_White {
        PREVIEW(SpecLib_i_tk_gue_soldier_rpg7);
        DEFAULT_GUE_UNIT_ENTRIES;

        uniformClass = "CUP_I_TKG_Khet_Partug_06";

        side = 2;
        faction = "SpecLib_Faction_Taki_Locals";
        editorSubcategory = "SpecLib_EdSubcat_Taki_Locals";
        backpack = "SpecLib_Taki_rpg7_bag";
        accuracy = 3.9;
        displayName = "Гранатомётчик";
		icon = "iconManAT";
        weapons[] = {"CUP_arifle_AKM_Early", "launch_RPG7_F", "Throw", "Put"};
        respawnWeapons[] = {"CUP_arifle_AKM_Early", "launch_RPG7_F", "Throw", "Put"};
        magazines[] = {mag_4("SPEC_30Rnd_762x39_AK47_M"),
                       "rhs_rpg7_PG7V_mag"};
        respawnMagazines[] = {mag_4("SPEC_30Rnd_762x39_AK47_M"),
                              "rhs_rpg7_PG7V_mag"};
        Items[] = {DEFAULT_ACE_ITEMS};
        RespawnItems[] = {DEFAULT_ACE_ITEMS};
        linkedItems[] = {"CUP_V_OI_TKI_Jacket2_05",
                         "CUP_H_TKI_Lungee_06",
                         DEFAULT_BOOMER_ITEMS};
        respawnLinkedItems[] = {"CUP_H_TKI_Lungee_06",
                                "CUP_V_OI_TKI_Jacket2_05",
                                DEFAULT_BOOMER_ITEMS};
    };
    class SpecLib_i_tk_gue_soldier_rpg7_ammo : CUP_I_TKG_Man_04 {
        PREVIEW(SpecLib_i_tk_gue_soldier_rpg7_ammo);
        DEFAULT_GUE_UNIT_ENTRIES;

        uniformClass = "CUP_I_TKG_Khet_Partug_06";

        backpack = "SpecLib_Taki_rpg7_bag";
        accuracy = 3.9;
        displayName = "Помощник гранатомётчика";
        weapons[] = {"CUP_arifle_AKM_Early", "Throw", "Put"};
        respawnWeapons[] = {"CUP_arifle_AKM_Early", "Throw", "Put"};
        magazines[] = {mag_4("SPEC_30Rnd_762x39_AK47_M"), mag_2("rhs_mag_rgd5")};
        respawnMagazines[] = {mag_4("SPEC_30Rnd_762x39_AK47_M"),
                              mag_2("rhs_mag_rgd5")};
        Items[] = {DEFAULT_ACE_ITEMS};
        RespawnItems[] = {DEFAULT_ACE_ITEMS};
        linkedItems[] = {"CUP_Beard_Black", "CUP_V_OI_TKI_Jacket4_04",
                         "CUP_H_TKI_Pakol_2_05",
                         DEFAULT_BOOMER_ITEMS};
        respawnLinkedItems[] = {"CUP_Beard_Black", "CUP_H_TKI_Pakol_2_05",
                                "CUP_V_OI_TKI_Jacket4_04",
                                DEFAULT_BOOMER_ITEMS};
    };
    class SpecLib_i_tk_gue_crewman : INS1_Green {
        PREVIEW(SpecLib_i_tk_gue_soldier_rpg7_ammo);
        DEFAULT_GUE_UNIT_ENTRIES;

        uniformClass = "CUP_I_TKG_Khet_Partug_08";

        side = 2;
        faction = "SpecLib_Faction_Taki_Locals";
        editorSubcategory = "SpecLib_EdSubcat_Taki_Locals";
		engineer = 1;
        accuracy = 3.9;
        displayName = "Член экипажа";
        weapons[] = {"CUP_arifle_AKMS_Early", "Throw", "Put"};
        respawnWeapons[] = {"CUP_arifle_AKMS_Early", "Throw", "Put"};
        magazines[] = {mag_4("SPEC_30Rnd_762x39_AK47_M")};
        respawnMagazines[] = {mag_4("SPEC_30Rnd_762x39_AK47_M")};
        Items[] = {"ACE_elasticBandage","ACE_elasticBandage","ACE_quikclot","ACE_quikclot","ACE_morphine","ACE_tourniquet","ACE_fieldDressing","ACE_fieldDressing","ACE_packingBandage","ACE_packingBandage"};
        RespawnItems[] = {"ACE_elasticBandage","ACE_elasticBandage","ACE_quikclot","ACE_quikclot","ACE_morphine","ACE_tourniquet","ACE_fieldDressing","ACE_fieldDressing","ACE_packingBandage","ACE_packingBandage"};
        linkedItems[] = { "CUP_Beard_Black","CUP_V_OI_TKI_Jacket2_04",
                         "rhs_tsh4",
                         DEFAULT_BOOMER_ITEMS};
        respawnLinkedItems[] = {"rhs_tsh4", "CUP_Beard_Black",
                                "CUP_V_OI_TKI_Jacket2_04",
                                DEFAULT_BOOMER_ITEMS};
    };
    class SpecLib_i_tk_gue_mechanic : CUP_I_TKG_Man_02 {
        PREVIEW(SpecLib_i_tk_gue_mechanic);
        DEFAULT_GUE_UNIT_ENTRIES;

        uniformClass = "CUP_I_TKG_Khet_Partug_07";

        threat[] = {1, 0.5, 0.1};
        engineer = 1;
        backpack = "SpecLib_Taki_mechanic_bag";
        accuracy = 3.9;
        displayName = "Механик";
		icon = "iconManEngineer";
        weapons[] = {"bnae_mk1_virtual", "Throw", "Put"};
        respawnWeapons[] = {"bnae_mk1_virtual", "Throw", "Put"};
        magazines[] = {mag_7("10Rnd_303_Magazine")};
        respawnMagazines[] = {mag_7("10Rnd_303_Magazine")};
        Items[] = {DEFAULT_ACE_ITEMS};
        RespawnItems[] = {DEFAULT_ACE_ITEMS};
        linkedItems[] = {"CUP_Beard_Black", "CUP_H_TKI_Pakol_2_04",
                         "CUP_V_OI_TKI_Jacket3_04",
                         DEFAULT_BOOMER_ITEMS};
        respawnLinkedItems[] = {"CUP_Beard_Black", "CUP_H_TKI_Pakol_2_04",
                                "CUP_V_OI_TKI_Jacket3_04",
                                DEFAULT_BOOMER_ITEMS};
    };
    class SpecLib_i_tk_gue_machinegunner : CUP_I_TKG_Man_04 {
        PREVIEW(SpecLib_i_tk_gue_machinegunner);
        DEFAULT_GUE_UNIT_ENTRIES;

        uniformClass = "CUP_I_TKG_Khet_Partug_02";

        threat[] = {1, 0.1, 0.6};
        backpack = "SpecLib_Taki_machinegunner_bag";
        accuracy = 3.9;
        displayName = "Пулемётчик";
		icon = "iconManMG";
        weapons[] = {"VTN_PKM_1974", "Throw", "Put"};
        respawnWeapons[] = {"VTN_PKM_1974", "Throw", "Put"};
        magazines[] = {mag_2("rhs_100Rnd_762x54mmR")};
        respawnMagazines[] = {mag_2("rhs_100Rnd_762x54mmR")};
        Items[] = {DEFAULT_ACE_ITEMS};
        RespawnItems[] = {DEFAULT_ACE_ITEMS};
        linkedItems[] = {"CUP_H_TKI_Lungee_05",
                         "CUP_V_OI_TKI_Jacket1_03",
                         DEFAULT_BOOMER_ITEMS};
        respawnLinkedItems[] = {"CUP_H_TKI_Lungee_05",
                                "CUP_V_OI_TKI_Jacket1_03",
                                DEFAULT_BOOMER_ITEMS};
    };
    class SpecLib_i_tk_gue_autorifleman : CUP_I_TKG_Man_04 {
        PREVIEW(SpecLib_i_tk_gue_machinegunner);
        DEFAULT_GUE_UNIT_ENTRIES;

        uniformClass = "CUP_I_TKG_Khet_Partug_06";

        threat[] = {1, 0.1, 0.6};
        backpack = "SpecLib_Taki_autorifleman_bag";
        accuracy = 3.9;
        displayName = "Пулемётчик (РПК)";
		icon = "iconManMG";
        weapons[] = {"CUP_arifle_RPK74", "Throw", "Put"};
        respawnWeapons[] = {"CUP_arifle_RPK74", "Throw", "Put"};
        magazines[] = {mag_4("SPEC_40Rnd_TE4_LRT4_762x39_RPK_M"), mag_2("rhs_mag_rgd5")};
        respawnMagazines[] = {mag_4("SPEC_40Rnd_TE4_LRT4_762x39_RPK_M"), mag_2("rhs_mag_rgd5")};
        Items[] = {DEFAULT_ACE_ITEMS};
        RespawnItems[] = {DEFAULT_ACE_ITEMS};
        linkedItems[] = {"CUP_Beard_Black", "CUP_H_TKI_Pakol_2_02",
                         "CUP_V_OI_TKI_Jacket4_04",
                         DEFAULT_BOOMER_ITEMS};
        respawnLinkedItems[] = {"CUP_Beard_Black", "CUP_H_TKI_Pakol_2_02",
                                "CUP_V_OI_TKI_Jacket4_04",
                                DEFAULT_BOOMER_ITEMS};
    };
    class SpecLib_i_tk_gue_soldier_tl : CUP_I_TKG_Man_02 {
        PREVIEW(SpecLib_i_tk_gue_soldier_tl);
        DEFAULT_GUE_UNIT_ENTRIES;

        uniformClass = "CUP_I_TKG_Khet_Partug_05";

        accuracy = 3.9;
        displayName = "Командир отделения";
		icon = "iconManLeader";
        weapons[] = {"rhs_weap_l1a1", "VTN_B8", "Throw", "Put"};
        respawnWeapons[] = {"rhs_weap_l1a1", "VTN_B8", "Throw", "Put"};
        magazines[] = {mag_3("rhs_mag_20Rnd_762x51_m80_fnfal")};
        respawnMagazines[] = {mag_3("rhs_mag_20Rnd_762x51_m80_fnfal")};
        Items[] = {DEFAULT_ACE_ITEMS};
        RespawnItems[] = {DEFAULT_ACE_ITEMS};
        linkedItems[] = {"CUP_H_TKI_Lungee_02",
                         "CUP_V_OI_TKI_Jacket1_06",
                         DEFAULT_COMMANDER_ITEMS};
        respawnLinkedItems[] = {"CUP_H_TKI_Lungee_02",
                                "CUP_V_OI_TKI_Jacket1_06",
                                DEFAULT_COMMANDER_ITEMS};
    };
    class SpecLib_i_tk_gue_commander : CUP_I_TKG_Man_01 {
        PREVIEW(SpecLib_i_tk_gue_commander);
        DEFAULT_GUE_UNIT_ENTRIES;

        uniformClass = "CUP_I_TKG_Khet_Partug_02";

        accuracy = 3.9;
        displayName = "Полевой командир";
		icon = "iconManOfficer";
        weapons[] = {"CUP_arifle_AKMS_Early", "Throw", "Put"};
        respawnWeapons[] = {"CUP_arifle_AKMS_Early", "Throw", "Put"};
        magazines[] = {mag_4("rhs_30Rnd_762x39mm")};
        respawnMagazines[] = {mag_4("rhs_30Rnd_762x39mm")};
        Items[] = {DEFAULT_ACE_ITEMS};
        RespawnItems[] = {DEFAULT_ACE_ITEMS};
        linkedItems[] = {"CUP_Beard_Black", "CUP_H_TKI_Pakol_2_02",
                         "CUP_V_OI_TKI_Jacket1_03",
                         DEFAULT_COMMANDER_ITEMS};
        respawnLinkedItems[] = {"CUP_Beard_Black", "CUP_H_TKI_Pakol_2_02",
                                "CUP_V_OI_TKI_Jacket1_03",
                                DEFAULT_COMMANDER_ITEMS};
    };
    class SpecLib_c_tk_civ_black : CUP_C_TKG_Man_04 {
        PREVIEW(SpecLib_c_tk_civ_black);
        DEFAULT_CIV_UNIT_ENTRIES;
        displayName = "Гражданский (Чёрный)";
        DEFAULT_CIV_EQUIPMENT;
        linkedItems[] = {"CUP_Beard_Black", "CUP_H_TKI_SkullCap_03", "ItemWatch"};
        respawnLinkedItems[] = {"CUP_Beard_Black","CUP_H_TKI_SkullCap_03", "ItemWatch"};
    };
    class SpecLib_c_tk_civ_black_brown_jacket : CUP_C_TKG_Man_04 {
        PREVIEW(SpecLib_c_tk_civ_black_brown_jacket);
        DEFAULT_CIV_UNIT_ENTRIES;
        displayName = "Гражданский (Чёрный, Коричневая куртка)";
        DEFAULT_CIV_EQUIPMENT;
        linkedItems[] = {"CUP_Beard_Black", "CUP_V_OI_TKI_Jacket5_05",
                         "CUP_H_TKI_Lungee_Open_04",
                         "ItemWatch"};
        respawnLinkedItems[] = {"CUP_Beard_Black", "CUP_V_OI_TKI_Jacket5_05",
                                "CUP_H_TKI_Lungee_Open_04",
                                "ItemWatch"};
    };
    class SpecLib_c_tk_civ_black_brown_waistcoat : CUP_C_TKG_Man_04 {
        PREVIEW(SpecLib_c_tk_civ_black_brown_waistcoat);
        DEFAULT_CIV_UNIT_ENTRIES;
        displayName = "Гражданский (Чёрный, Коричневый жилет)";
        DEFAULT_CIV_EQUIPMENT;
        linkedItems[] = {"G_Squares_Tinted", "CUP_V_OI_TKI_Jacket6_06",
                         "CUP_H_TKI_Pakol_2_01",
                         "ItemWatch"};
        respawnLinkedItems[] = {"G_Squares_Tinted", "CUP_V_OI_TKI_Jacket6_06",
                                "CUP_H_TKI_Pakol_2_01",
                                "ItemWatch"};
    };
    class SpecLib_c_tk_civ_blue : CUP_C_TKG_Man_07 {
        PREVIEW(SpecLib_c_tk_civ_blue);
        DEFAULT_CIV_UNIT_ENTRIES;
        displayName = "Гражданский (Синий)";
        DEFAULT_CIV_EQUIPMENT;
        linkedItems[] = {"G_Squares", "CUP_H_TKI_Lungee_Open_05", "ItemWatch"};
        respawnLinkedItems[] = {"G_Squares",
                                "CUP_H_TKI_Lungee_Open_05",
                                "ItemWatch"};
    };
    class SpecLib_c_tk_civ_blue_brown_coat : CUP_C_TKG_Man_07 {
        PREVIEW(SpecLib_c_tk_civ_blue_brown_coat);
        DEFAULT_CIV_UNIT_ENTRIES;
        displayName = "Гражданский (Синий, Коричневое пальто)";
        DEFAULT_CIV_EQUIPMENT;
        linkedItems[] = {"G_Squares_Tinted",
                         "CUP_H_TKI_SkullCap_03",
                         "CUP_V_OI_TKI_Jacket1_05",
                         "ItemWatch"};
        respawnLinkedItems[] = {"G_Squares_Tinted",
                                "CUP_H_TKI_SkullCap_03",
                                "CUP_V_OI_TKI_Jacket1_05",
                                "ItemWatch"};
    };
    class SpecLib_c_tk_civ_blue_grey_waistcoat : CUP_C_TKG_Man_07 {
        PREVIEW(SpecLib_c_tk_civ_blue_grey_waistcoat);
        DEFAULT_CIV_UNIT_ENTRIES;
        displayName = "Гражданский (Синий, Серый жилет)";
        DEFAULT_CIV_EQUIPMENT;
        linkedItems[] = {"CUP_Beard_Black", "CUP_H_TKI_Pakol_2_02",
                         "CUP_V_OI_TKI_Jacket6_04",
                         "ItemWatch"};
        respawnLinkedItems[] = {"CUP_Beard_Black", "CUP_H_TKI_Pakol_2_02",
                                "CUP_V_OI_TKI_Jacket6_04",
                                "ItemWatch"};
    };
    class SpecLib_c_tk_civ_brown : CUP_C_TKG_Man_08 {
        PREVIEW(SpecLib_c_tk_civ_brown);
        DEFAULT_CIV_UNIT_ENTRIES;
        displayName = "Гражданский (Коричневый)";
        DEFAULT_CIV_EQUIPMENT;
        linkedItems[] = {"CUP_Beard_Black", "CUP_H_TKI_SkullCap_01", "ItemWatch"};
        respawnLinkedItems[] = {"CUP_Beard_Black", "CUP_H_TKI_SkullCap_01", "ItemWatch"};
    };
    class SpecLib_c_tk_civ_brown_brown_jacket : CUP_C_TKG_Man_08 {
        PREVIEW(SpecLib_c_tk_civ_brown_brown_jacket);
        DEFAULT_CIV_UNIT_ENTRIES;
        displayName = "Гражданский (Коричневый, Коричневая куртка)";
        DEFAULT_CIV_EQUIPMENT;
        linkedItems[] = {"CUP_Beard_Black", "CUP_H_TKI_Lungee_Open_05",
                         "CUP_V_OI_TKI_Jacket5_05",
                         "ItemWatch"};
        respawnLinkedItems[] = {"CUP_Beard_Black", "CUP_H_TKI_Lungee_Open_05",
                                "CUP_V_OI_TKI_Jacket5_05",
                                "ItemWatch"};
    };
    class SpecLib_c_tk_civ_brown_light_brown_waistcoat : CUP_C_TKG_Man_08 {
        PREVIEW(SpecLib_c_tk_civ_brown_light_brown_waistcoat);
        DEFAULT_CIV_UNIT_ENTRIES;
        displayName = "Гражданский (Коричневый, Коричневый жилет)";
        DEFAULT_CIV_EQUIPMENT;
        linkedItems[] = {"G_Squares",
                         "CUP_H_TKI_Pakol_2_06",
                         "CUP_V_OI_TKI_Jacket6_06",
                         "ItemWatch"};
        respawnLinkedItems[] = {"G_Squares",
                                "CUP_H_TKI_Pakol_2_06",
                                "CUP_V_OI_TKI_Jacket6_06",
                                "ItemWatch"};
    };
    class SpecLib_c_tk_civ_cream_grey_coat : CUP_C_TKG_Man_05 {
        PREVIEW(SpecLib_c_tk_civ_cream_grey_coat);
        DEFAULT_CIV_UNIT_ENTRIES;
        displayName = "Гражданский (Кремовый, Серое пальто)";
        DEFAULT_CIV_EQUIPMENT;
        linkedItems[] = {"CUP_Beard_Black", "CUP_H_TKI_SkullCap_03",
                         "CUP_V_OI_TKI_Jacket1_06",
                         "ItemWatch"};
        respawnLinkedItems[] = {"CUP_Beard_Black", "CUP_H_TKI_SkullCap_03",
                                "CUP_V_OI_TKI_Jacket1_06",
                                "ItemWatch"};
    };
    class SpecLib_c_tk_civ_cream_grey_jacket : CUP_C_TKG_Man_05 {
        PREVIEW(SpecLib_c_tk_civ_cream_grey_jacket);
        DEFAULT_CIV_UNIT_ENTRIES;
        displayName = "Гражданский (Кремовый, Серая куртка)";
        DEFAULT_CIV_EQUIPMENT;
        linkedItems[] = {"CUP_G_Squares_Tinted",
                         "CUP_H_TKI_SkullCap_02",
                         "CUP_V_OI_TKI_Jacket5_04",
                         "ItemWatch"};
        respawnLinkedItems[] = {"CUP_G_Squares_Tinted",
                                "CUP_H_TKI_SkullCap_02",
                                "CUP_V_OI_TKI_Jacket5_04",
                                "ItemWatch"};
    };
    class SpecLib_c_tk_civ_cream_tricolor_waistcoat : CUP_C_TKG_Man_05 {
        PREVIEW(SpecLib_c_tk_civ_cream_tricolor_waistcoat);
        DEFAULT_CIV_UNIT_ENTRIES;
        displayName = "Гражданский (Кремовый, Трехцветный жилет)";
        DEFAULT_CIV_EQUIPMENT;
        linkedItems[] = {"CUP_Beard_Black", "CUP_H_TKI_Lungee_Open_02",
                         "CUP_V_OI_TKI_Jacket6_03",
                         "ItemWatch"};
        respawnLinkedItems[] = {"CUP_Beard_Black", "CUP_H_TKI_Lungee_Open_02",
                                "CUP_V_OI_TKI_Jacket6_03",
                                "ItemWatch"};
    };
    class SpecLib_c_tk_civ_grey_black_coat : CUP_C_TKG_Man_06 {
        PREVIEW(SpecLib_c_tk_civ_grey_black_coat);
        DEFAULT_CIV_UNIT_ENTRIES;
        displayName = "Гражданский (Серый, Чёрное пальто)";
        DEFAULT_CIV_EQUIPMENT;
        linkedItems[] = {"CUP_Beard_Black", "CUP_H_TKI_SkullCap_03",
                         "CUP_V_OI_TKI_Jacket1_04",
                         "ItemWatch"};
        respawnLinkedItems[] = {"CUP_Beard_Black", "CUP_H_TKI_SkullCap_03",
                                "CUP_V_OI_TKI_Jacket1_04",
                                "ItemWatch"};
    };
    class SpecLib_c_tk_civ_grey_brown_jacket : CUP_C_TKG_Man_06 {
        PREVIEW(SpecLib_c_tk_civ_grey_brown_jacket);
        DEFAULT_CIV_UNIT_ENTRIES;
        displayName = "Гражданский (Серый, Коричневая куртка)";
        DEFAULT_CIV_EQUIPMENT;
        linkedItems[] = {"CUP_Beard_Black", "CUP_H_TKI_Lungee_Open_03",
                         "CUP_V_OI_TKI_Jacket5_06",
                         "ItemWatch"};
        respawnLinkedItems[] = {"CUP_Beard_Black", "CUP_H_TKI_Lungee_Open_03",
                                "CUP_V_OI_TKI_Jacket5_06",
                                "ItemWatch"};
    };
    class SpecLib_c_tk_civ_grey_brown_waistcoat : CUP_C_TKG_Man_06 {
        PREVIEW(SpecLib_c_tk_civ_grey_brown_waistcoat);
        DEFAULT_CIV_UNIT_ENTRIES;
        displayName = "Гражданский (Серый, Коричневый жилет)";
        DEFAULT_CIV_EQUIPMENT;
        linkedItems[] = {"CUP_G_Squares_Tinted",
                         "CUP_H_TKI_SkullCap_03",
                         "CUP_V_OI_TKI_Jacket6_06",
                         "ItemWatch"};
        respawnLinkedItems[] = {"CUP_G_Squares_Tinted",
                                "CUP_H_TKI_SkullCap_03",
                                "CUP_V_OI_TKI_Jacket6_06",
                                "ItemWatch"};
    };
    class SpecLib_c_tk_civ_olive : CUP_C_TKG_Man_02 {
        PREVIEW(SpecLib_c_tk_civ_olive);
        DEFAULT_CIV_UNIT_ENTRIES;
        displayName = "Гражданский (Оливковый)";
        DEFAULT_CIV_EQUIPMENT;
        linkedItems[] = {"G_Squares", "CUP_H_TKI_Lungee_Open_05", "ItemWatch"};
        respawnLinkedItems[] = {"G_Squares", "CUP_H_TKI_Lungee_Open_05", "ItemWatch"};
    };
    class SpecLib_c_tk_civ_olive_light_brown_jacket : CUP_C_TKG_Man_02 {
        PREVIEW(SpecLib_c_tk_civ_olive_light_brown_jacket);
        DEFAULT_CIV_UNIT_ENTRIES;
        displayName = "Гражданский (Оливковый, Коричневая куртка)";
        DEFAULT_CIV_EQUIPMENT;
        linkedItems[] = {"CUP_Beard_Black", "CUP_H_TKI_Pakol_2_01",
                         "CUP_V_OI_TKI_Jacket5_06",
                         "ItemWatch"};
        respawnLinkedItems[] = {"CUP_Beard_Black", "CUP_H_TKI_Pakol_2_01",
                                "CUP_V_OI_TKI_Jacket5_06",
                                "ItemWatch"};
    };
    class SpecLib_c_tk_civ_olive_woodland_waistcoat : CUP_C_TKG_Man_02 {
        PREVIEW(SpecLib_c_tk_civ_olive_woodland_waistcoat);
        DEFAULT_CIV_UNIT_ENTRIES;
        displayName = "Гражданский (Оливковый, Лесной жилет)";
        DEFAULT_CIV_EQUIPMENT;
        linkedItems[] = {"CUP_Beard_Black", "CUP_H_TKI_Pakol_2_05",
                         "CUP_V_OI_TKI_Jacket6_01",
                         "ItemWatch"};
        respawnLinkedItems[] = {"CUP_Beard_Black", "CUP_H_TKI_Pakol_2_05",
                                "CUP_V_OI_TKI_Jacket6_01",
                                "ItemWatch"};
    };
    class SpecLib_c_tk_civ_tan_desert_waistcoat : CUP_C_TKG_Man_01 {
        PREVIEW(SpecLib_c_tk_civ_tan_desert_waistcoat);
        DEFAULT_CIV_UNIT_ENTRIES;
        displayName = "Гражданский (Песочный, Пустынный жилет)";
        DEFAULT_CIV_EQUIPMENT;
        linkedItems[] = {"CUP_G_Squares_Tinted",
                         "CUP_H_TKI_SkullCap_01",
                         "CUP_V_OI_TKI_Jacket6_02",
                         "ItemWatch"};
        respawnLinkedItems[] = {"CUP_G_Squares_Tinted",
                                "CUP_H_TKI_SkullCap_01",
                                "CUP_V_OI_TKI_Jacket6_02",
                                "ItemWatch"};
    };
    class SpecLib_c_tk_civ_tan_grey_coat : CUP_C_TKG_Man_01 {
        PREVIEW(SpecLib_c_tk_civ_tan_grey_coat);
        DEFAULT_CIV_UNIT_ENTRIES;
        displayName = "Гражданский (Песочный, Серое пальто)";
        DEFAULT_CIV_EQUIPMENT;
        linkedItems[] = {"CUP_Beard_Black", "CUP_H_TKI_Lungee_Open_01",
                         "CUP_V_OI_TKI_Jacket1_06",
                         "ItemWatch"};
        respawnLinkedItems[] = {"CUP_Beard_Black", "CUP_H_TKI_Lungee_Open_01",
                                "CUP_V_OI_TKI_Jacket1_06",
                                "ItemWatch"};
    };
    class SpecLib_c_tk_civ_tan_light_brown_jacket : CUP_C_TKG_Man_01 {
        PREVIEW(SpecLib_c_tk_civ_tan_light_brown_jacket);
        DEFAULT_CIV_UNIT_ENTRIES;
        displayName = "Гражданский (Песочный, Коричневая куртка)";
        DEFAULT_CIV_EQUIPMENT;
        linkedItems[] = {"CUP_Beard_Black", "CUP_H_TKI_SkullCap_03",
                         "CUP_V_OI_TKI_Jacket5_05",
                         "ItemWatch"};
        respawnLinkedItems[] = {"CUP_Beard_Black", "CUP_H_TKI_SkullCap_03",
                                "CUP_V_OI_TKI_Jacket5_05",
                                "ItemWatch"};
    };
    class SpecLib_c_tk_civ_white_grey_coat : CUP_C_TKG_Man_03 {
        PREVIEW(SpecLib_c_tk_civ_white_grey_coat);
        DEFAULT_CIV_UNIT_ENTRIES;
        displayName = "Гражданский (Белый, Серое пальто)";
        DEFAULT_CIV_EQUIPMENT;
        linkedItems[] = {"CUP_Beard_Black", "CUP_H_TKI_SkullCap_03",
                         "CUP_V_OI_TKI_Jacket1_05",
                         "ItemWatch"};
        respawnLinkedItems[] = {"CUP_Beard_Black", "CUP_H_TKI_SkullCap_03",
                                "CUP_V_OI_TKI_Jacket1_05",
                                "ItemWatch"};
    };
    class SpecLib_c_tk_civ_white_grey_jacket : CUP_C_TKG_Man_03 {
        PREVIEW(SpecLib_c_tk_civ_white_grey_jacket);
        DEFAULT_CIV_UNIT_ENTRIES;
        displayName = "Гражданский (Белый, Серая куртка)";
        DEFAULT_CIV_EQUIPMENT;
        linkedItems[] = {"CUP_G_Squares_Tinted",
                         "CUP_H_TKI_Pakol_2_01",
                         "CUP_V_OI_TKI_Jacket5_04",
                         "ItemWatch"};
        respawnLinkedItems[] = {"CUP_G_Squares_Tinted",
                                "CUP_H_TKI_Pakol_2_01",
                                "CUP_V_OI_TKI_Jacket5_04",
                                "ItemWatch"};
    };
    class SpecLib_c_tk_civ_white_grey_waistcoat : CUP_C_TKG_Man_03 {
        PREVIEW(SpecLib_c_tk_civ_white_grey_waistcoat);
        DEFAULT_CIV_UNIT_ENTRIES;
        displayName = "Гражданский (Белый, Серый жилет)";
        DEFAULT_CIV_EQUIPMENT;
        linkedItems[] = {"CUP_Beard_Black", "CUP_H_TKI_Lungee_Open_06",
                         "CUP_V_OI_TKI_Jacket6_04",
                         "ItemWatch"};
        respawnLinkedItems[] = {"CUP_Beard_Black", "CUP_H_TKI_Lungee_Open_06",
                                "CUP_V_OI_TKI_Jacket6_04",
                                "ItemWatch"};
    };
};
