class CfgMissions
{
	class Cutscenes
	{
		class Namalsk_intro2
		{
			directory = "z\speclib\addons\ambient\cutscenes\Namalsk_intro2.namalsk";
		};
		class Namalsk_intro3
		{
			directory = "z\speclib\addons\ambient\cutscenes\Namalsk_intro3.namalsk";
		};
		class Namalsk_intro4
		{
			directory = "z\speclib\addons\ambient\cutscenes\Namalsk_intro4.namalsk";
		};
	};
};
