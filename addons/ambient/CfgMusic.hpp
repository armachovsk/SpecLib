class CfgMusic {
    class MoozE_Wasteland_II {
        name = "MoozE Wasteland II";
        sound[] = {
            "\z\speclib\addons\ambient\music\MoozE_-_Stalker_-_25_-_Wasteland_II.ogg",
            1,
            1};
        duration = 127;
        theme = "safe";
        musicClass = "IntroAmbient";
    };
    class MoozE_Res_Outtake_Sketch {
        name = "MoozE Res Outtake Sketch";
        sound[] = {
            "\z\speclib\addons\ambient\music\MoozE_-_Stalker_-_26_-_Res__outtake__sketch_.ogg",
            1,
            1};
        duration = 117;
        theme = "safe";
        musicClass = "IntroAmbient";
    };
    class MoozE_Sleeping_In_Ashes_v1 {
        name = "MoozE Sleeping In Ashes v1";
        sound[] = {
            "\z\speclib\addons\ambient\music\MoozE_-_Stalker_-_27_-_Sleeping_In_Ashes_v1.ogg",
            1,
            1};
        duration = 164;
        theme = "safe";
        musicClass = "IntroAmbient";
    };
    class Ones_Atop_The_Unknown {
        name = "Ones Atop The Unknown";
        sound[] = {
            "\z\speclib\addons\ambient\music\Apócrýphos___Kammarheit___Atrium_Carceri_-_Ones_Atop_The_Unknown.ogg",
            1,
            1};
        duration = 322;
        theme = "safe";
        musicClass = "IntroAmbient";
    };
};
