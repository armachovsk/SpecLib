class RscText;
class RscCombo;
class RscPicture;
class RscActiveText;
class RscControlsGroup;
class RscStandardDisplay;
class RscPictureKeepAspect;
class RscDisplayStart : RscStandardDisplay {
    class controls {
        class LoadingStart : RscControlsGroup {
            class controls {
                class Logo : RscPictureKeepAspect {
                    text = QPATHTOF(startup_logo.paa);
                    x = "0.33375 * safezoneW";
                    y = "0.29 * safezoneH";
                    w = "0.3325 * safezoneW";
                    h = "0.39375 * safezoneH";
                    onLoad = "";
                };
            };
        };
    };
};
class RscDisplayMain : RscStandardDisplay {
    class Controls {
        class Logo : RscPictureKeepAspect {
            text = QPATHTOF(startup_logo.paa);
            tooltip = "Сборник модификаций для игр c RHS.";
            x = "0.4425 -  5 *  (pixelW * pixelGrid * 2)";
            y = "safezoneY + (3 - 0.9 *  5) *  (pixelH * pixelGrid * 2)";
            w = "3 *  5 *  (pixelW * pixelGrid * 2)";
            h = "2 *  5 *  (pixelH * pixelGrid * 2)";
            onButtonClick = CSTRING(URL);
            onKillFocus = "";
            onSetFocus = "";
            onLoad = "";
        };
        class LogoApex : Logo {
            show = 0;
            text = QPATHTOF(startup_logo.paa);
            onLoad = "";
        };
    };
};
class RscBackgroundLogo : RscPictureKeepAspect {
    text = QPATHTOF(startup_logo.paa);
    x = "0.33375 * safezoneW";
    y = "0.29 * safezoneH";
    w = "0.3325 * safezoneW";
    h = "0.39375 * safezoneH";
};
class RscDisplayNotFreeze : RscStandardDisplay {
    class controls {
        class LoadingStart : RscControlsGroup {
            class controls {
                class Logo : RscPictureKeepAspect {
                    text = QPATHTOF(startup_logo.paa);
                    x = "0.33375 * safezoneW";
                    y = "0.29 * safezoneH";
                    w = "0.3325 * safezoneW";
                    h = "0.39375 * safezoneH";
                    onLoad = "";
                };
            };
        };
    };
};
class RscDisplayLoadMission : RscStandardDisplay {
    class controls {
        class LoadingStart : RscControlsGroup {
            class controls {
                class Logo : RscPictureKeepAspect {
                    text = QPATHTOF(startup_logo.paa);
                    x = "0.33375 * safezoneW";
                    y = "0.29 * safezoneH";
                    w = "0.3325 * safezoneW";
                    h = "0.39375 * safezoneH";
                    onLoad = "";
                };
            };
        };
    };
};
class RscTitles {
    class SplashArma3 {
        class Picture : RscPictureKeepAspect {
            text = QPATHTOF(startup_logo.paa);
            x = "0.33375 * safezoneW";
            y = "0.29 * safezoneH";
            w = "0.3325 * safezoneW";
            h = "0.39375 * safezoneH";
        };
    };
};
