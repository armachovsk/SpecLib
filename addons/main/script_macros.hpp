#include "\z\ace\addons\main\script_macros.hpp"

#define SPECLIB_SETTINGS_CAT "SpecLib"
#define SPECLIB_SETTINGS_SCAT_MISC "Misc"
#define SPECLIB_SETTINGS_SCAT_CONTAM "Contamination"

#define PRIVATE                                                                \
    scope = 0;                                                                 \
    scopeArsenal = 0;                                                          \
    scopeCurator = 0
#define PROTECTED                                                              \
    scope = 1;                                                                 \
    scopeArsenal = 1;                                                          \
    scopeCurator = 1
#define PUBLIC                                                                 \
    scope = 2;                                                                 \
    scopeArsenal = 2;                                                          \
    scopeCurator = 2

#define mag_xx(a, b)                                                           \
    class _xx_##a {                                                            \
        magazine = a;                                                          \
        count = b;                                                             \
    }

#define weap_xx(a, b)                                                          \
    class _xx_##a {                                                            \
        weapon = a;                                                            \
        count = b;                                                             \
    }

#define item_xx(a, b)                                                          \
    class _xx_##a {                                                            \
        name = a;                                                              \
        count = b;                                                             \
    }

#define backpack_xx(a, b)                                                      \
    class _xx_##a {                                                            \
        backpack = a;                                                          \
        count = b;                                                             \
    }

#define mag_2(a) a, a
#define mag_3(a) a, a, a
#define mag_4(a) a, a, a, a
#define mag_5(a) a, a, a, a, a
#define mag_6(a) a, a, a, a, a, a
#define mag_7(a) a, a, a, a, a, a, a
#define mag_8(a) a, a, a, a, a, a, a, a
#define mag_9(a) a, a, a, a, a, a, a, a, a
#define mag_10(a) a, a, a, a, a, a, a, a, a, a
#define mag_11(a) a, a, a, a, a, a, a, a, a, a, a
#define mag_12(a) a, a, a, a, a, a, a, a, a, a, a, a
#define mag_13(a) a, a, a, a, a, a, a, a, a, a, a, a, a

#define PREVIEW(CLASSNAME)                                                     \
    editorPreview = \z\speclib\addons\COMPONENT\preview\##CLASSNAME##.jpg
