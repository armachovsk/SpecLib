params ["_chance", "_howMuch"];

private _isAlive = {
    params ["_unit"];
    (alive _unit) and (not (_unit in allDead)) and (not (_unit in allDeadMen))
};

{
    private _alive = [_x] call _isAlive;
	// 90 percent chance of adding toolkit to vehicles
	if((itemCargo _x find "ToolKit" == -1 && !_alive) && _chance) then {
		_x addItemCargoGlobal ["ToolKit", _howMuch]
	};
} forEach vehicles;

true
