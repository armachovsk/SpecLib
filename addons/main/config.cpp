#include "script_component.hpp"

class CfgPatches {
    class ADDON {
        name = COMPONENT_NAME;
        units[] = {};
        weapons[] = {};
        requiredVersion = REQUIRED_VERSION;
        requiredAddons[] = {"cba_main", "ace_overheating"};
        author = CSTRING(SpecLibTeam);
        url = CSTRING(URL);
        VERSION_CONFIG;
    };
    class speclib_reveal {
        name = "SpecLib - Module Reveal (Compatibility)";
        units[] = {};
        weapons[] = {};
        requiredVersion = REQUIRED_VERSION;
        requiredAddons[] = {"cba_main", "A3_Ui_F", "ace_overheating"};
        author = CSTRING(SpecLibTeam);
        url = CSTRING(URL);
        VERSION_CONFIG;
    };
};

class CfgMods {
    class PREFIX {
        dir = "@z";
        name = "Special Operations Modpack Library";
        picture = QPATHOF(mod.paa);
        hidePicture = "true";
        hideName = "true";
        actionName = "Website";
        action = CSTRING(URL);
        description =
            "Issue Tracker: https://gitlab.com/Armachovsk/SpecLib/issues";
    };
};

#include "CfgEventHandlers.hpp"
#include "CfgFactionClasses.hpp"
#include "CfgWeapons.hpp"
#include "CfgVehicles.hpp"
#include "CfgStartupLogo.hpp"
