class CfgVehicles {
    class Man;

    class CAManBase: Man {
        class ACE_SelfActions {
            class ACE_Equipment {
                class ace_overheating_UnJam {
                    displayName = "Clear jam (dzn)";
                    condition = "ace_overheating_enabled && {[_player] call ace_overheating_fnc_canUnjam}";
                    exceptions[] = {"isNotInside", "isNotSwimming", "isNotSitting"};
                    statement = "[] spawn dzn_EJAM_fnc_uiShowUnjamMenu";
                    showDisabled = 0;
                    icon = "z\ace\addons\common\UI\repack_ca.paa";
                };
            };
        };
    };
};
