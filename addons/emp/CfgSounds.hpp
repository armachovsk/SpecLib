class CfgSounds
{
	class murmur
	{
		name = "murmur"; // Name for mission editor
		sound[] = {"\z\speclib\addons\emp\sounds\murmur.ogg", 0.8, 1.0};
		titles[] = {0, ""};
	};
	class geiger
	{
		name = "geiger"; // Name for mission editor
		sound[] = {"\z\speclib\addons\emp\sounds\geiger.ogg", .7, 1.0};
		titles[] = {0, ""};
	};
	class spark1
	{
		name = "spark1";
		sound[] = {"\z\speclib\addons\emp\sounds\spark1.ogg", db+5, 1};
		titles[] = {};
	};
	class spark11
	{
		name = "spark11";
		sound[] = {"\z\speclib\addons\emp\sounds\spark11.ogg", db+5, 1};
		titles[] = {};
	};
	class spark2
	{
		name = "spark2";
		sound[] = {"\z\speclib\addons\emp\sounds\spark2.ogg", db+5, 1};
		titles[] = {};
	};
	class spark22
	{
		name = "spark22";
		sound[] = {"\z\speclib\addons\emp\sounds\spark22.ogg", db+5, 1};
		titles[] = {};
	};
	class spark3
	{
		name = "spark3";
		sound[] = {"\z\speclib\addons\emp\sounds\spark3.ogg", db+5, 1};
		titles[] = {};
	};
	class spark4
	{
		name = "spark4";
		sound[] = {"\z\speclib\addons\emp\sounds\spark4.ogg", db+5, 1};
		titles[] = {};
	};
	class spark5
	{
		name = "spark5";
		sound[] = {"\z\speclib\addons\emp\sounds\spark5.ogg", db+5, 1};
		titles[] = {};
	};
	class earthquake_02
	{
		name = "earthquake_02";
		sound[] = {"\z\speclib\addons\emp\sounds\earthquake_02.ogg",0.7, 1};
		titles[] = {1, ""};
	};
	class tiuit
	{
		name = "tiuit"; // Name for mission editor
		sound[] = {"\z\speclib\addons\emp\sounds\tiuit.ogg", 0.2, 1.0};
		titles[] = {0, ""};
	};
};
