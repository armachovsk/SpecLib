#include "script_component.hpp"

// Argument 0 is module logic.
_logic = param [0,objNull,[objNull]];
// Argument 1 is list of affected units (affected by value selected in the 'class Units' argument))
_units = param [1,[],[[]]];
// True when the module was activated, false when it's deactivated (i.e., synced triggers are no longer active)
_activated = param [2,true,[true]];

if (_units isEqualTo []) exitWith {
    [localize "STR_speclib_emp_objectName"] call BIS_fnc_error;
    false
};

private _empRange = _logic getVariable ["speclib_emp_empRange", 500];
private _visibleEmp = _logic getVariable ["speclib_emp_visibleEmp", true];
private _affectPerception = _logic getVariable ["speclib_emp_affectPerception", true];
private _damageUnit = _logic getVariable ["speclib_emp_damageUnit", 0.1];

[_units select 0, _empRange, _visibleEmp, _affectPerception, _damageUnit] spawn speclib_emp_fnc_empStarter;

true
